 package jsp.weixin.menu.util;

/** 
 * 按钮的基类 
 *  
 *@date 2015.04.24
 */  
public class Button {  
    private String name;  
  
    public String getName() {  
        return name;  
    }  
  
    public void setName(String name) {  
        this.name = name;  
    }  
}  
