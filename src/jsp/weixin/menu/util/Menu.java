package jsp.weixin.menu.util;

/** 
 * 菜单 
 *  
 *@date 2015.04.24
 */  
public class Menu {  
    private Button[] button;  
  
    public Button[] getButton() {  
        return button;  
    }  
  
    public void setButton(Button[] button) {  
        this.button = button;  
    }  
}  
