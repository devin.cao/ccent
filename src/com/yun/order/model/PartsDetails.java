package com.yun.order.model;

import java.lang.reflect.Field;

public class PartsDetails {
	
	private String pade_name;
	private String pade_price;
	private String pade_unit;
	private String pade_partsorderid;
	private String pade_producttype;
	private String pade_productsid;
	private String pade_products;
	private String pade_amount;
	private String pade_specialremark;
	private String pade_quantity;
	private String pade_status;
	private String pade_PartsDetailsID;
	private String pade_shipments;//发货数量

	//总页数
		private Integer total;
		

		public String getPade_PartsDetailsID() {
			return pade_PartsDetailsID;
		}
		public void setPade_PartsDetailsID(String pade_PartsDetailsID) {
			this.pade_PartsDetailsID = pade_PartsDetailsID;
		}
		public Integer getTotal() {
			return total;
		}
		public void setTotal(Integer total) {
			this.total = total;
		}
	public String getPade_name() {
		return pade_name;
	}
	public void setPade_name(String pade_name) {
		this.pade_name = pade_name;
	}
	public String getPade_price() {
		return pade_price;
	}
	public void setPade_price(String pade_price) {
		this.pade_price = pade_price;
	}
	public String getPade_unit() {
		return pade_unit;
	}
	public void setPade_unit(String pade_unit) {
		this.pade_unit = pade_unit;
	}
	public String getPade_partsorderid() {
		return pade_partsorderid;
	}
	public void setPade_partsorderid(String pade_partsorderid) {
		this.pade_partsorderid = pade_partsorderid;
	}
	public String getPade_producttype() {
		return pade_producttype;
	}
	public void setPade_producttype(String pade_producttype) {
		this.pade_producttype = pade_producttype;
	}
	public String getPade_productsid() {
		return pade_productsid;
	}
	public void setPade_productsid(String pade_productsid) {
		this.pade_productsid = pade_productsid;
	}
	public String getPade_amount() {
		return pade_amount;
	}
	public void setPade_amount(String pade_amount) {
		this.pade_amount = pade_amount;
	}
	public String getPade_specialremark() {
		return pade_specialremark;
	}
	public void setPade_specialremark(String pade_specialremark) {
		this.pade_specialremark = pade_specialremark;
	}
	public String getPade_quantity() {
		return pade_quantity;
	}
	public void setPade_quantity(String pade_quantity) {
		this.pade_quantity = pade_quantity;
	}

	
	public String getPade_status() {
		return pade_status;
	}
	public void setPade_status(String pade_status) {
		this.pade_status = pade_status;
	}
	public String getPade_products() {
		return pade_products;
	}
	public void setPade_products(String pade_products) {
		this.pade_products = pade_products;
	}

	public String getPade_shipments() {
		return pade_shipments;
	}

	public void setPade_shipments(String pade_shipments) {
		this.pade_shipments = pade_shipments;
	}

	public static void main(String[] args) {
		  Class<?> c = PartsDetails.class;
	        try {
	            Object object = c.newInstance();
	            Field[] fields = c.getDeclaredFields();
	            System.out.println("Heros所有属性：");
	            StringBuffer bf=new StringBuffer();
	           
	            for (Field f : fields) {
	            	 bf.append("'"+f.getName()+"':'${ item."+f.getName()+"}',");
	            }
	           
	            System.out.println("{"+bf.substring(0,bf.length()-1)+"}");
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	}
}
