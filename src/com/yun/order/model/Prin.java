package com.yun.order.model;

public class Prin {
	private Integer StatusCode;
	private String Message;

	private String prin_name;
	//机头编号
	private String prin_headsn;
	//机身编号
	private String prin_bodysn;
	
	//产品类型
	private String prin_producttype;
	//产品型号
	private String prin_productmodel;
	//生产日期
	private String prin_manufacturedate;
	//出货日期
	private String prin_shipmentdate;
	//用户姓名
	private String prin_consumer;
	//安装日期
	private String prin_installationdate;
	//购买日期
	private String prin_purchasedate;
	//代理商名称
	private String prin_company;
	
	public String getPrin_name() {
		return prin_name;
	}
	public void setPrin_name(String prin_name) {
		this.prin_name = prin_name;
	}
	public String getPrin_headsn() {
		return prin_headsn;
	}
	public void setPrin_headsn(String prin_headsn) {
		this.prin_headsn = prin_headsn;
	}
	public String getPrin_producttype() {
		return prin_producttype;
	}
	public void setPrin_producttype(String prin_producttype) {
		this.prin_producttype = prin_producttype;
	}
	public String getPrin_productmodel() {
		return prin_productmodel;
	}
	public void setPrin_productmodel(String prin_productmodel) {
		this.prin_productmodel = prin_productmodel;
	}
	public String getPrin_manufacturedate() {
		return prin_manufacturedate;
	}
	public void setPrin_manufacturedate(String prin_manufacturedate) {
		this.prin_manufacturedate = prin_manufacturedate;
	}
	public String getPrin_shipmentdate() {
		return prin_shipmentdate;
	}
	public void setPrin_shipmentdate(String prin_shipmentdate) {
		this.prin_shipmentdate = prin_shipmentdate;
	}
	public String getPrin_consumer() {
		return prin_consumer;
	}
	public void setPrin_consumer(String prin_consumer) {
		this.prin_consumer = prin_consumer;
	}
	public String getPrin_installationdate() {
		return prin_installationdate;
	}
	public void setPrin_installationdate(String prin_installationdate) {
		this.prin_installationdate = prin_installationdate;
	}
	public String getPrin_purchasedate() {
		return prin_purchasedate;
	}
	public void setPrin_purchasedate(String prin_purchasedate) {
		this.prin_purchasedate = prin_purchasedate;
	}
	public String getPrin_company() {
		return prin_company;
	}
	public void setPrin_company(String prin_company) {
		this.prin_company = prin_company;
	}
	public String getPrin_bodysn() {
		return prin_bodysn;
	}
	public void setPrin_bodysn(String prin_bodysn) {
		this.prin_bodysn = prin_bodysn;
	}
	public Integer getStatusCode() {
		return StatusCode;
	}
	public void setStatusCode(Integer statusCode) {
		StatusCode = statusCode;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	
	
}
