package com.yun.order.model;

import java.lang.reflect.Field;
import java.util.List;

public class PartOrder {

	private String paor_PartsOrderID;
	private String paor_name;
	private String paor_company;
	private String  paor_tel;
	private String paor_deliveringway;
	private String paor_express;
	private String paor_province;
	private String paor_city;
	private String paor_county;
	private String paor_address;
	
	private String paor_orderer;
	private String paor_ordererphone;
	private String paor_orderdate;
	private String paor_recipientsphone;
	private String paor_recipients;
	//总页数
	private Integer total;
	//状态
	private Integer paor_status;
	private List<PartsDetails> partsdetails;
	
	
	public String getPaor_recipientsphone() {
		return paor_recipientsphone;
	}
	public void setPaor_recipientsphone(String paor_recipientsphone) {
		this.paor_recipientsphone = paor_recipientsphone;
	}
	public String getPaor_recipients() {
		return paor_recipients;
	}
	public void setPaor_recipients(String paor_recipients) {
		this.paor_recipients = paor_recipients;
	}
	public String getPaor_PartsOrderID() {
		return paor_PartsOrderID;
	}
	public void setPaor_PartsOrderID(String paor_PartsOrderID) {
		this.paor_PartsOrderID = paor_PartsOrderID;
	}
	public String getPaor_name() {
		return paor_name;
	}
	public void setPaor_name(String paor_name) {
		this.paor_name = paor_name;
	}
	public String getPaor_company() {
		return paor_company;
	}
	public void setPaor_company(String paor_company) {
		this.paor_company = paor_company;
	}
	public String getPaor_tel() {
		return paor_tel;
	}
	public void setPaor_tel(String paor_tel) {
		this.paor_tel = paor_tel;
	}
	public String getPaor_deliveringway() {
		return paor_deliveringway;
	}
	public void setPaor_deliveringway(String paor_deliveringway) {
		this.paor_deliveringway = paor_deliveringway;
	}
	public String getPaor_express() {
		return paor_express;
	}
	public void setPaor_express(String paor_express) {
		this.paor_express = paor_express;
	}
	public String getPaor_province() {
		return paor_province;
	}
	public void setPaor_province(String paor_province) {
		this.paor_province = paor_province;
	}
	public String getPaor_city() {
		return paor_city;
	}
	public void setPaor_city(String paor_city) {
		this.paor_city = paor_city;
	}
	public String getPaor_county() {
		return paor_county;
	}
	public void setPaor_county(String paor_county) {
		this.paor_county = paor_county;
	}
	public String getPaor_address() {
		return paor_address;
	}
	public void setPaor_address(String paor_address) {
		this.paor_address = paor_address;
	}
	public String getPaor_orderer() {
		return paor_orderer;
	}
	public void setPaor_orderer(String paor_orderer) {
		this.paor_orderer = paor_orderer;
	}
	public String getPaor_orderdate() {
		return paor_orderdate;
	}
	public void setPaor_orderdate(String paor_orderdate) {
		this.paor_orderdate = paor_orderdate;
	}
	public List<PartsDetails> getPartsdetails() {
		return partsdetails;
	}
	public void setPartsdetails(List<PartsDetails> partsdetails) {
		this.partsdetails = partsdetails;
	}
	
	
	public String getPaor_ordererphone() {
		return paor_ordererphone;
	}
	public void setPaor_ordererphone(String paor_ordererphone) {
		this.paor_ordererphone = paor_ordererphone;
	}
	public Integer getPaor_status() {
		return paor_status;
	}
	public void setPaor_status(Integer paor_status) {
		this.paor_status = paor_status;
	}
	
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public static void main(String[] args) {
		  Class<?> c = PartOrder.class;
	        try {
	            Object object = c.newInstance();
	            Field[] fields = c.getDeclaredFields();
	            System.out.println("Heros所有属性：");
	            StringBuffer bf=new StringBuffer();
	           
	            for (Field f : fields) {
	            	 bf.append("'"+f.getName()+"':'${ item."+f.getName()+"}',");
	            }
	           
	            System.out.println("{"+bf.substring(0,bf.length()-1)+"}");
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	}
}
