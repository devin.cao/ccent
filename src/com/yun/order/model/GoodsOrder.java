package com.yun.order.model;

import java.lang.reflect.Field;
import java.util.List;

public class GoodsOrder {

	
	private String goor_GoodsOrderID;
	//订单编号
	private String goor_name;
	//客户姓名
	private String goor_companyid;
	
	
	//
	private String goor_company;
	private String goor_deliveringway;
	//客户地址
	private String goor_address;
	//订货日期
	private String goor_orderdate;
	//货款总额
	private String goor_totalamount;
	private String goor_totalamount_CID;
	//预定定金
	private String goor_earnest;
	private String goor_earnest_CID;
	//专卖店地址
	private String goor_storeaddress;
	//专卖店电话
	private String goor_storetel;
	//订单人
	private String goor_orderer;
	private String goor_ordererphone;
	//状态
	
	private Integer goor_status;
      
	private List<PartsDetails> goodsdetails;
	
	//总页数
		private Integer total;
	private String  goor_tel;
	private String  goor_province;
	private String  goor_city;
	private String  goor_county;
	private String  goor_recipientsphone;
	private String  goor_recipients;

		public Integer getTotal() {
			return total;
		}
		public void setTotal(Integer total) {
			this.total = total;
		}

	public String getGoor_GoodsOrderID() {
		return goor_GoodsOrderID;
	}




	public void setGoor_GoodsOrderID(String goor_GoodsOrderID) {
		this.goor_GoodsOrderID = goor_GoodsOrderID;
	}




	public String getGoor_ordererphone() {
		return goor_ordererphone;
	}
	public void setGoor_ordererphone(String goor_ordererphone) {
		this.goor_ordererphone = goor_ordererphone;
	}
	public String getGoor_name() {
		return goor_name;
	}




	public void setGoor_name(String goor_name) {
		this.goor_name = goor_name;
	}




	public String getGoor_companyid() {
		return goor_companyid;
	}




	public void setGoor_companyid(String goor_companyid) {
		this.goor_companyid = goor_companyid;
	}




	public String getGoor_company() {
		return goor_company;
	}




	public void setGoor_company(String goor_company) {
		this.goor_company = goor_company;
	}




	public String getGoor_address() {
		return goor_address;
	}




	public void setGoor_address(String goor_address) {
		this.goor_address = goor_address;
	}




	public String getGoor_orderdate() {
		return goor_orderdate;
	}




	public void setGoor_orderdate(String goor_orderdate) {
		this.goor_orderdate = goor_orderdate;
	}




	public String getGoor_totalamount() {
		return goor_totalamount;
	}




	public void setGoor_totalamount(String goor_totalamount) {
		this.goor_totalamount = goor_totalamount;
	}




	public String getGoor_totalamount_CID() {
		return goor_totalamount_CID;
	}




	public void setGoor_totalamount_CID(String goor_totalamount_CID) {
		this.goor_totalamount_CID = goor_totalamount_CID;
	}




	public String getGoor_earnest() {
		return goor_earnest;
	}




	public void setGoor_earnest(String goor_earnest) {
		this.goor_earnest = goor_earnest;
	}




	public String getGoor_earnest_CID() {
		return goor_earnest_CID;
	}




	public void setGoor_earnest_CID(String goor_earnest_CID) {
		this.goor_earnest_CID = goor_earnest_CID;
	}




	public String getGoor_storeaddress() {
		return goor_storeaddress;
	}




	public void setGoor_storeaddress(String goor_storeaddress) {
		this.goor_storeaddress = goor_storeaddress;
	}




	public String getGoor_storetel() {
		return goor_storetel;
	}




	public void setGoor_storetel(String goor_storetel) {
		this.goor_storetel = goor_storetel;
	}




	public String getGoor_orderer() {
		return goor_orderer;
	}




	public void setGoor_orderer(String  goor_orderer) {
		this.goor_orderer = goor_orderer;
	}




	public Integer getGoor_status() {
		return goor_status;
	}




	public void setGoor_status(Integer goor_status) {
		this.goor_status = goor_status;
	}




	public List<PartsDetails> getGoodsdetails() {
		return goodsdetails;
	}




	public void setGoodsdetails(List<PartsDetails> goodsdetails) {
		this.goodsdetails = goodsdetails;
	}




	public String getGoor_deliveringway() {
		return goor_deliveringway;
	}




	public void setGoor_deliveringway(String goor_deliveringway) {
		this.goor_deliveringway = goor_deliveringway;
	}


	public String getgoor_tel() {
		return goor_tel;
	}

	public void setgoor_tel(String goor_tel) {
		this.goor_tel = goor_tel;
	}

	public String getGoor_province() {
		return goor_province;
	}

	public void setGoor_province(String goor_province) {
		this.goor_province = goor_province;
	}

	public String getGoor_city() {
		return goor_city;
	}

	public void setGoor_city(String goor_city) {
		this.goor_city = goor_city;
	}

	public String getGoor_county() {
		return goor_county;
	}

	public void setGoor_county(String goor_county) {
		this.goor_county = goor_county;
	}

	public String getGoor_recipientsphone() {
		return goor_recipientsphone;
	}

	public void setGoor_recipientsphone(String goor_recipientsphone) {
		this.goor_recipientsphone = goor_recipientsphone;
	}

	public String getGoor_recipients() {
		return goor_recipients;
	}

	public void setGoor_recipients(String goor_recipients) {
		this.goor_recipients = goor_recipients;
	}

	public static void main(String[] args) {
		  Class<?> c = GoodsOrder.class;
	        try {
	            Object object = c.newInstance();
	            Field[] fields = c.getDeclaredFields();
	            System.out.println("Heros所有属性：");
	            StringBuffer bf=new StringBuffer();
	           
	            for (Field f : fields) {
	            	 bf.append("'"+f.getName()+"':'${ item."+f.getName()+"}',");
	            }
	           
	            System.out.println("{"+bf.substring(0,bf.length()-1)+"}");
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	}
}
