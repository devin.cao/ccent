package com.yun.order.model;

import java.lang.reflect.Field;

public class WorkOrder {

	//
	private String woor_WorkOrderID;
	private String woor_company;
	//代理商
	private String woor_companyid;
	
	private String woor_consumer;
	//来电客户
	private String woor_consumerid;
	//问题描述
	private String woor_description;
	//处理人
	private String woor_handler;
	//工单号
	private String woor_name;
	private String woor_product;
	//产品型号
	private String woor_productid;
	//备注
	private String woor_remark;
	//解决方案
	private String woor_solution;
	//工单状态
	private String woor_status;
	//标题
	private String woor_title;
	//工单类型
	private String woor_type;
	//工单日期
	private String woor_createddate;
	
	
	//总页数
		private Integer total;
		

		public Integer getTotal() {
			return total;
		}
		public void setTotal(Integer total) {
			this.total = total;
		}
	
	public WorkOrder() {
		
	}
	
	
	public String getWoor_createddate() {
		return woor_createddate;
	}


	public void setWoor_createddate(String woor_createddate) {
		this.woor_createddate = woor_createddate;
	}


	public String getWoor_WorkOrderID() {
		return woor_WorkOrderID;
	}
	public void setWoor_WorkOrderID(String woor_WorkOrderID) {
		this.woor_WorkOrderID = woor_WorkOrderID;
	}
	public String getWoor_company() {
		return woor_company;
	}
	public void setWoor_company(String woor_company) {
		this.woor_company = woor_company;
	}
	public String getWoor_companyid() {
		return woor_companyid;
	}
	public void setWoor_companyid(String woor_companyid) {
		this.woor_companyid = woor_companyid;
	}
	public String getWoor_consumer() {
		return woor_consumer;
	}
	public void setWoor_consumer(String woor_consumer) {
		this.woor_consumer = woor_consumer;
	}
	public String getWoor_consumerid() {
		return woor_consumerid;
	}
	public void setWoor_consumerid(String woor_consumerid) {
		this.woor_consumerid = woor_consumerid;
	}
	public String getWoor_description() {
		return woor_description;
	}
	public void setWoor_description(String woor_description) {
		this.woor_description = woor_description;
	}
	public String getWoor_handler() {
		return woor_handler;
	}
	public void setWoor_handler(String woor_handler) {
		this.woor_handler = woor_handler;
	}
	public String getWoor_name() {
		return woor_name;
	}
	public void setWoor_name(String woor_name) {
		this.woor_name = woor_name;
	}
	public String getWoor_product() {
		return woor_product;
	}
	public void setWoor_product(String woor_product) {
		this.woor_product = woor_product;
	}
	public String getWoor_productid() {
		return woor_productid;
	}
	public void setWoor_productid(String woor_productid) {
		this.woor_productid = woor_productid;
	}
	public String getWoor_remark() {
		return woor_remark;
	}
	public void setWoor_remark(String woor_remark) {
		this.woor_remark = woor_remark;
	}
	public String getWoor_solution() {
		return woor_solution;
	}
	public void setWoor_solution(String woor_solution) {
		this.woor_solution = woor_solution;
	}
	public String getWoor_status() {
		return woor_status;
	}
	public void setWoor_status(String woor_status) {
		this.woor_status = woor_status;
	}
	public String getWoor_title() {
		return woor_title;
	}
	public void setWoor_title(String woor_title) {
		this.woor_title = woor_title;
	}
	public String getWoor_type() {
		return woor_type;
	}
	public void setWoor_type(String woor_type) {
		this.woor_type = woor_type;
	}
	
	public static void main(String[] args) {
		  Class<?> c = WorkOrder.class;
	        try {
	            Object object = c.newInstance();
	            Field[] fields = c.getDeclaredFields();
	            System.out.println("Heros所有属性：");
	            StringBuffer bf=new StringBuffer();
	           
	            for (Field f : fields) {
	            	 bf.append("'"+f.getName()+"':'${ item."+f.getName()+"}',");
	            }
	           
	            System.out.println("{"+bf.substring(0,bf.length()-1)+"}");
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	}
	
}
