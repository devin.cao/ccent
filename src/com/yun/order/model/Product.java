package com.yun.order.model;

public class Product {
	private Integer prod_productid;
	private	String prod_name;
	private	String prod_code;
	private	String prod_model;
	private	String prod_unit;
	private	String prod_price;
	private	String prod_remarks;
	private	String prod_property;
	//总页数
	private Integer total;
		
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getProd_productid() {
		return prod_productid;
	}
	public void setProd_productid(Integer prod_productid) {
		this.prod_productid = prod_productid;
	}
	public String getProd_name() {
		return prod_name;
	}
	public void setProd_name(String prod_name) {
		this.prod_name = prod_name;
	}
	public String getProd_code() {
		return prod_code;
	}
	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}
	public String getProd_model() {
		return prod_model;
	}
	public void setProd_model(String prod_model) {
		this.prod_model = prod_model;
	}
	public String getProd_unit() {
		return prod_unit;
	}
	public void setProd_unit(String prod_unit) {
		this.prod_unit = prod_unit;
	}
	public String getProd_price() {
		return prod_price;
	}
	public void setProd_price(String prod_price) {
		this.prod_price = prod_price;
	}
	public String getProd_remarks() {
		return prod_remarks;
	}
	public void setProd_remarks(String prod_remarks) {
		this.prod_remarks = prod_remarks;
	}
	public String getProd_property() {
		return prod_property;
	}
	public void setProd_property(String prod_property) {
		this.prod_property = prod_property;
	}

}
