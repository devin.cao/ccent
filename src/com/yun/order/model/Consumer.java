package com.yun.order.model;

import net.sf.json.JSONArray;

import java.lang.reflect.Field;
import java.util.List;

public class Consumer {
	//用户名称
	private String cons_name;
	private String cons_Status;
	private String cons_phonenumber;
	//用户来源
	private String cons_source;
	//用户生日
	private String cons_birthday;
	private String cons_gender;
	//详细地址
	private String cons_address;
	//产品选项
	private String cons_productoption;
	//	产品类别
	private String cons_productcategory;
	private String cons_province;
	private String cons_city;
	private String cons_county;
	//购买地点
	private String cons_purchaseplace;
	//购买日期
	private String cons_purchasedate;
	//安装日期
	private String cons_installationdate;
	//安装人员姓名
	private String cons_Installationname;
	//安装人员电话
	private String cons_Installationtel;
	//所在代理商名称
	private String cons_company;
	//机头编号
	private String cons_headsn;
	//机身编号
	private String cons_bodysn;
	private String cons_producttype;
	//生产日期
	private String cons_manufacturedate;
	//设备号
	private String cons_screenwificode;
	//总页数
	private Integer total;
	//多个产品
	private JSONArray consumer_body;
	private String consumer_body_string;


	

	public String getCons_gender() {
		return cons_gender;
	}
	public void setCons_gender(String cons_gender) {
		this.cons_gender = cons_gender;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getCons_name() {
		return cons_name;
	}
	public void setCons_name(String cons_name) {
		this.cons_name = cons_name;
	}
	public String getCons_Status() {
		return cons_Status;
	}
	public void setCons_Status(String cons_Status) {
		this.cons_Status = cons_Status;
	}
	public String getCons_phonenumber() {
		return cons_phonenumber;
	}
	public void setCons_phonenumber(String cons_phonenumber) {
		this.cons_phonenumber = cons_phonenumber;
	}
	public String getCons_source() {
		return cons_source;
	}
	public void setCons_source(String cons_source) {
		this.cons_source = cons_source;
	}
	public String getCons_birthday() {
		return cons_birthday;
	}
	public void setCons_birthday(String cons_birthday) {
		this.cons_birthday = cons_birthday;
	}
	public String getCons_address() {
		return cons_address;
	}
	public void setCons_address(String cons_address) {
		this.cons_address = cons_address;
	}
	public String getCons_productoption() {
		return cons_productoption;
	}
	public void setCons_productoption(String cons_productoption) {
		this.cons_productoption = cons_productoption;
	}
	public String getCons_province() {
		return cons_province;
	}
	public void setCons_province(String cons_province) {
		this.cons_province = cons_province;
	}
	public String getCons_city() {
		return cons_city;
	}
	public void setCons_city(String cons_city) {
		this.cons_city = cons_city;
	}
	public String getCons_county() {
		return cons_county;
	}
	public void setCons_county(String cons_county) {
		this.cons_county = cons_county;
	}
	public String getCons_purchaseplace() {
		return cons_purchaseplace;
	}
	public void setCons_purchaseplace(String cons_purchaseplace) {
		this.cons_purchaseplace = cons_purchaseplace;
	}
	public String getCons_purchasedate() {
		return cons_purchasedate;
	}
	public void setCons_purchasedate(String cons_purchasedate) {
		this.cons_purchasedate = cons_purchasedate;
	}
	public String getCons_installationdate() {
		return cons_installationdate;
	}
	public void setCons_installationdate(String cons_installationdate) {
		this.cons_installationdate = cons_installationdate;
	}
	public String getCons_Installationname() {
		return cons_Installationname;
	}
	public void setCons_Installationname(String cons_Installationname) {
		this.cons_Installationname = cons_Installationname;
	}
	public String getCons_Installationtel() {
		return cons_Installationtel;
	}
	public void setCons_Installationtel(String cons_Installationtel) {
		this.cons_Installationtel = cons_Installationtel;
	}
	public String getCons_company() {
		return cons_company;
	}
	public void setCons_company(String cons_company) {
		this.cons_company = cons_company;
	}
	public String getCons_headsn() {
		return cons_headsn;
	}
	public void setCons_headsn(String cons_headsn) {
		this.cons_headsn = cons_headsn;
	}
	public String getCons_bodysn() {
		return cons_bodysn;
	}
	public void setCons_bodysn(String cons_bodysn) {
		this.cons_bodysn = cons_bodysn;
	}
	public String getCons_producttype() {
		return cons_producttype;
	}
	public void setCons_producttype(String cons_producttype) {
		this.cons_producttype = cons_producttype;
	}
	public String getCons_manufacturedate() {
		return cons_manufacturedate;
	}
	public void setCons_manufacturedate(String cons_manufacturedate) {
		this.cons_manufacturedate = cons_manufacturedate;
	}
	
	public String getCons_screenwificode() {
		return cons_screenwificode;
	}
	public void setCons_screenwificode(String cons_screenwificode) {
		this.cons_screenwificode = cons_screenwificode;
	}
	
	public String getCons_productcategory() {
		return cons_productcategory;
	}
	public void setCons_productcategory(String cons_productcategory) {
		this.cons_productcategory = cons_productcategory;
	}

	public JSONArray getConsumer_body() {
		return consumer_body;
	}

	public void setConsumer_body(JSONArray consumer_body) {
		this.consumer_body = consumer_body;
	}

	public String getConsumer_body_string() {
		return consumer_body_string;
	}

	public void setConsumer_body_string(String consumer_body_string) {
		this.consumer_body_string = consumer_body_string;
	}

	public static void main(String[] args) {
		  Class<?> c = Consumer.class;
	        try {
	            Object object = c.newInstance();
	            Field[] fields = c.getDeclaredFields();
	            System.out.println("Heros所有属性：");
	            StringBuffer bf=new StringBuffer();
	           
	            for (Field f : fields) {
	            	 bf.append("'"+f.getName()+"':'${ item."+f.getName()+"}',");
	            }
	           
	            System.out.println("{"+bf.substring(0,bf.length()-1)+"}");
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	}
	
	
}
