package com.yun.order.model;

import java.lang.reflect.Field;

public class GoodsDetails {

	private String gode_GoodsDetailsID;
	//序号
	private String gode_name;
	//产品的id
	private String gode_productid;
	private String gode_productname;
	private String gode_color;
	private String gode_price;
	private String gode_quantity;
	private String gode_remarks;
	private String gode_goodsorderid;
	private String gode_status;
	private Integer gode_shipments;//发货数量
	//总页数
		private Integer total;
		

		public Integer getTotal() {
			return total;
		}
		public void setTotal(Integer total) {
			this.total = total;
		}
		
	public String getGode_productname() {
			return gode_productname;
		}
		public void setGode_productname(String gode_productname) {
			this.gode_productname = gode_productname;
		}
	public String getGode_GoodsDetailsID() {
		return gode_GoodsDetailsID;
	}

	public void setGode_GoodsDetailsID(String gode_GoodsDetailsID) {
		this.gode_GoodsDetailsID = gode_GoodsDetailsID;
	}

	public String getGode_name() {
		return gode_name;
	}

	public void setGode_name(String gode_name) {
		this.gode_name = gode_name;
	}

	public String getGode_productid() {
		return gode_productid;
	}

	public void setGode_productid(String gode_productid) {
		this.gode_productid = gode_productid;
	}

	public String getGode_color() {
		return gode_color;
	}

	public void setGode_color(String gode_color) {
		this.gode_color = gode_color;
	}

	public String getGode_status() {
		return gode_status;
	}
	public void setGode_status(String gode_status) {
		this.gode_status = gode_status;
	}
	public String getGode_price() {
		return gode_price;
	}

	public void setGode_price(String gode_price) {
		this.gode_price = gode_price;
	}

	public String getGode_quantity() {
		return gode_quantity;
	}

	public void setGode_quantity(String gode_quantity) {
		this.gode_quantity = gode_quantity;
	}

	public String getGode_remarks() {
		return gode_remarks;
	}

	public void setGode_remarks(String gode_remarks) {
		this.gode_remarks = gode_remarks;
	}

	public String getGode_goodsorderid() {
		return gode_goodsorderid;
	}

	public void setGode_goodsorderid(String gode_goodsorderid) {
		this.gode_goodsorderid = gode_goodsorderid;
	}

	public Integer getGode_shipments() {
		return gode_shipments;
	}

	public void setGode_shipments(Integer gode_shipments) {
		this.gode_shipments = gode_shipments;
	}

	public static void main(String[] args) {
		  Class<?> c = GoodsDetails.class;
	        try {
	            Object object = c.newInstance();
	            Field[] fields = c.getDeclaredFields();
	            System.out.println("Heros所有属性：");
	            StringBuffer bf=new StringBuffer();
	           
	            for (Field f : fields) {
	            	 bf.append("'"+f.getName()+"':'${ item."+f.getName()+"}',");
	            }
	           
	            System.out.println("{"+bf.substring(0,bf.length()-1)+"}");
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	}

}
