package com.yun.order.model;

public class Company {
	

	private String comp_code;
	private String comp_name;
	//全名:
	private String comp_fullname;
    //省:
	private String comp_province;
    //城市
	private String  comp_city;
    //区/县:
	private String  comp_county;
    //联系人:
	private String comp_contacts;
    //电话:
	private String comp_tel;
    //移动电话:
	private String comp_phone;
    //地址:
	private String comp_address;
    //传真:
	private String comp_fax;
    //客户分类:
	private String comp_category;
    //备注:
	private String comp_remarks;
	//
	private String credit;
	//收件人手机
	private String comp_recipientsphone;
	//收件人姓名
	private String comp_recipients;
	//回收款
	private String deposit;
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getComp_name() {
		return comp_name;
	}
	public void setComp_name(String comp_name) {
		this.comp_name = comp_name;
	}
	public String getComp_fullname() {
		return comp_fullname;
	}
	public void setComp_fullname(String comp_fullname) {
		this.comp_fullname = comp_fullname;
	}
	public String getComp_province() {
		return comp_province;
	}
	public void setComp_province(String comp_province) {
		this.comp_province = comp_province;
	}
	
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getComp_city() {
		return comp_city;
	}
	public void setComp_city(String comp_city) {
		this.comp_city = comp_city;
	}
	public String getComp_county() {
		return comp_county;
	}
	public void setComp_county(String comp_county) {
		this.comp_county = comp_county;
	}
	public String getComp_contacts() {
		return comp_contacts;
	}
	public void setComp_contacts(String comp_contacts) {
		this.comp_contacts = comp_contacts;
	}
	public String getComp_tel() {
		return comp_tel;
	}
	public void setComp_tel(String comp_tel) {
		this.comp_tel = comp_tel;
	}
	public String getComp_phone() {
		return comp_phone;
	}
	public void setComp_phone(String comp_phone) {
		this.comp_phone = comp_phone;
	}
	public String getComp_address() {
		return comp_address;
	}
	public void setComp_address(String comp_address) {
		this.comp_address = comp_address;
	}
	public String getComp_fax() {
		return comp_fax;
	}
	public void setComp_fax(String comp_fax) {
		this.comp_fax = comp_fax;
	}
	public String getComp_category() {
		return comp_category;
	}
	public void setComp_category(String comp_category) {
		this.comp_category = comp_category;
	}
	public String getComp_remarks() {
		return comp_remarks;
	}
	public void setComp_remarks(String comp_remarks) {
		this.comp_remarks = comp_remarks;
	}
	public String getComp_recipientsphone() {
		return comp_recipientsphone;
	}
	public void setComp_recipientsphone(String comp_recipientsphone) {
		this.comp_recipientsphone = comp_recipientsphone;
	}
	public String getComp_recipients() {
		return comp_recipients;
	}
	public void setComp_recipients(String comp_recipients) {
		this.comp_recipients = comp_recipients;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}
}
