package com.yun.web.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.yun.core.utils.PermissionEnum;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yun.core.BaseController;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.MD5Util;
import com.yun.core.utils.HttpTookit;

@Controller
@RequestMapping("/user/")
public class UserServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(UserServerAPI.class);

	/**
	 * 跳转到登录界面
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login")
	public String login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String burl=CoreUtils.trim(request.getParameter("burl"));
		//其他参数拼接
		@SuppressWarnings("unchecked")
		Map<String,Object> map=request.getParameterMap();
		StringBuffer bf=new StringBuffer();
		for(String dataKey : map.keySet())   
		{   
		    log.info(dataKey ); 
		    if(!dataKey.equals("burl")){
		    	 String [] param = (String[])map.get(dataKey);  
				 String value=param[0];
				 bf.append("&"+dataKey+"="+CoreUtils.trim(value));
		    }
		   
		}
		String str=bf.toString();
		log.info("登录接收到数据str:"+str);
		if(str.length()>0){
			if(burl.contains("?")){
				burl=burl+str;
			}else{
				burl=burl+"?"+str.substring(1);
			}
		}
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setAttribute("backurl", burl);
		return "user/login";
	}

	/**
	 * 登录
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dologin")
	public void dologin(HttpServletRequest request, HttpServletResponse response){

		String url1 = Constants.baseurl + Constants.getAgentAccountUrl;
		Map<String, String> map = new HashMap<String, String>();
		String openid=CoreUtils.trim(request.getParameter("openid"));
		String agac_name=CoreUtils.trim(request.getParameter("agac_name"));
		String agac_password=CoreUtils.trim(request.getParameter("agac_password"));
		String burl=CoreUtils.trim(request.getParameter("backurl"));
		//agac_password=MD5Util.GetMD5Code(agac_password);
		if(openid.equals("")){
			openid="123";
		}
		map.put("openid",openid);
		map.put("agac_name",agac_name);
		map.put("agac_password",agac_password);
		map.put("method", "login");
		map.put("key", Constants.httpkey);
		String jsondata = HttpTookit.doGet(url1, map);
		HttpSession backurlSession= request.getSession();
		HttpSession sessionuser= request.getSession();
		if(jsondata.indexOf("\"StatusCode\":1")>0){
			log.info("登录成功");
			//request.getSession().setAttribute("sessionuser", agac_name);
			PermissionEnum[] array = PermissionEnum.getArray();
			Long menu = null;
			for (PermissionEnum permissionEnum : array) {
				if(burl.equals(permissionEnum.getDesc())){
					menu = Long.valueOf(permissionEnum.getId());
				}
			}
			if (menu!=null){

				String url = Constants.baseurl+"/api/Company/CheckPermission";
				log.info(url);
				HashMap map1 = new HashMap();
				map1.put("menu", String.valueOf(menu));
				map1.put("Usr", agac_name);
				String e = HttpTookit.doGet(url, map1);
				HttpSession session= request.getSession();
				if(e.indexOf("\"StatusCode\":1")>0){
					this.print(response, e);
					session.setMaxInactiveInterval(8*60*60);
					session.setAttribute("isPremission", "isPremission");
				}else {
					session.removeAttribute("isPremission");
					sessionuser.removeAttribute("sessionuser");
				}
			}
			backurlSession.setMaxInactiveInterval(8*60*60);
			backurlSession.setAttribute("backurl", burl);

			sessionuser.setMaxInactiveInterval(8*60*60);
			sessionuser.setAttribute("sessionuser", agac_name);
		}else {
			backurlSession.removeAttribute("backurl");
			sessionuser.removeAttribute("sessionuser");
		}
		log.info(jsondata);
		print(response, jsondata);
		HttpSession session= request.getSession();
		session.setMaxInactiveInterval(8*60*60);
		session.setAttribute("backurl", burl);
	}

	/**
	 * 跳转到注册界面
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toreg")
	public String toreg(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		return "user/reg";
	}

	/**
	 * 注册
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doreg")
	public void doreg(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String agac_name = CoreUtils.trim(request.getParameter("agac_name"));
		String agac_password = CoreUtils.trim(request.getParameter("agac_password"));
		String agac_orderpassword = CoreUtils.trim(request.getParameter("agac_orderpassword"));
		String agac_accountname = CoreUtils.trim(request.getParameter("agac_accountname"));
		String agac_mobilephone1 = CoreUtils.trim(request.getParameter("agac_mobilephone1"));
		String agac_mobilephone2 = CoreUtils.trim(request.getParameter("agac_mobilephone2"));
		String agac_address = CoreUtils.trim(request.getParameter("agac_address"));
		String agac_exclusiveshop = CoreUtils.trim(request.getParameter("agac_exclusiveshop"));
		String agac_remark = CoreUtils.trim(request.getParameter("agac_remark"));
		String agac_province = CoreUtils.trim(request.getParameter("agac_province"));
		String agac_city = CoreUtils.trim(request.getParameter("agac_city"));
		String agac_county = CoreUtils.trim(request.getParameter("agac_county"));

		//agac_password=MD5Util.GetMD5Code(agac_password);
		//agac_orderpassword=MD5Util.GetMD5Code(agac_orderpassword);
		String url = Constants.baseurl + Constants.addAgentAccountUrl;
		Map<String, String> map = new HashMap<String, String>();
		map.put("agac_name", agac_name);
		map.put("agac_password", agac_password);
		map.put("agac_orderpassword", agac_orderpassword);
		map.put("agac_accountname", agac_accountname);
		map.put("agac_mobilephone1", agac_mobilephone1);
		map.put("agac_mobilephone2", agac_mobilephone2);
		map.put("agac_address", agac_address);
		map.put("agac_exclusiveshop", agac_exclusiveshop);
		map.put("agac_remark", agac_remark);
		map.put("agac_province", agac_province);
		map.put("agac_city", agac_city);
		map.put("agac_county", agac_county);
		map.put("key", Constants.httpkey);
		// map.put("method", "login");
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**'
	 * 跳转到找回密码界面
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getpwd")
	public String forgetpwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		return "user/forget_pwd";
	}
	/**
	 * 找回密码
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */

	@RequestMapping({"/dogetpwd"})
	public void doforgetpwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String url = Constants.baseurl+"/api/AgentAccount/VerifyMsg";
		log.info("当前调用接口为：" + url);
		HashMap map = new HashMap();
		String agac_accountname = CoreUtils.trim(request.getParameter("agac_name"));
		String agac_mobilephone1 = CoreUtils.trim(request.getParameter("agac_mobilephone1"));
		String cons_phonecode = CoreUtils.trim(request.getParameter("cons_phonecode"));
		String key = "8803ea17cdb4c868";
		map.put("key", key);
		map.put("name", agac_accountname);
		map.put("phone", agac_mobilephone1);
		map.put("code", cons_phonecode);
		log.info("key" + key + "code:" + cons_phonecode + ",name:" + agac_accountname + ",phone:" + agac_mobilephone1);

		try {
			String e = HttpTookit.doGet(url, map);
			log.info(e);
			this.print(response, e);
		} catch (Exception var10) {
			this.print(response, errorjson);
		}

	}
	/**'
	 * 去设置密码界面
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/tosetpwd")
	public String tosetpwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String agac_name = CoreUtils.trim(request.getParameter("agac_name"));
		String agac_accountname = CoreUtils.trim(request.getParameter("agac_accountname"));
		String agac_mobilephone1 = CoreUtils.trim(request.getParameter("agac_mobilephone1"));
		request.setAttribute("agac_name", agac_name);
		request.setAttribute("agac_accountname", agac_accountname);
		request.setAttribute("agac_mobilephone1", agac_mobilephone1);
		return "user/reset_pwd";
	}
	/**'
	 * 设置密码界面
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */

	@RequestMapping({"/dosetpwd"})
	public void dosetpwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String newpwd = CoreUtils.trim(request.getParameter("newpwd"));
		String newpwd_order = CoreUtils.trim(request.getParameter("newpwd_order"));
		String usr = CoreUtils.trim(request.getParameter("usr"));
		log.info("usr:" + usr + " newpwd:" + newpwd);
		log.info("usr:" + usr + " newpwd_order:" + newpwd_order);
		//newpwd = MD5Util.GetMD5Code(newpwd);
		//newpwd_order = MD5Util.GetMD5Code(newpwd_order);
		String url = Constants.baseurl+"/api/AgentAccount/UpdatePassword";
		log.info("url:" + url);
		HashMap map = new HashMap();
		map.put("usr", usr);
		map.put("newpwd", newpwd);
		map.put("newodpwd", newpwd_order);
		map.put("key", "8803ea17cdb4c868");

		try {
			String e = HttpTookit.doGet(url, map);
			log.info(e);
			this.print(response, e);
		} catch (Exception var9) {
			this.print(response, errorjson);
		}

	}
	/**'
	 *  下单密码验证界面
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/odcheck")
	public String odcheck(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "user/orderpwdcheck";
	}
	/**'
	 *  oder pwd
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/orderpwd")
	public void orderpwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String pwd = CoreUtils.trim(request.getParameter("pwd"));
		String usr =getSessionUser(request);
		//pwd=MD5Util.GetMD5Code(pwd);
		String url = Constants.baseurl + Constants.checkOrderPasswordUrl;
		Map<String, String> map = new HashMap<String, String>();
		map.put("usr", usr);
		map.put("odpwd",  pwd);
		map.put("key", Constants.httpkey);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			print(response, errorjson);
		}
		
	}
	
	/*
	 * 获取省份数据
	 * */
	@RequestMapping(value = "/province") 
	public  void getProvince(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.province;
		Map<String, String> map = new HashMap<String, String>();
		String jsondata="";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);  
			print(response, jsondata); 
		} catch (Exception e) {
			e.printStackTrace();
			 print(response, errorjson);
		} 
	}
	
	/**
	 *获取城市
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/city")
	public void city(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.city;
		log.info("############城市");
		Map<String, String> map = new HashMap<String, String>();

		String pid = CoreUtils.trim(request.getParameter("id")); 
		log.info("省份代号："+pid);
		map.put("provinceID", pid); 
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsondata="";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		} 
	}
	
	
	/**
	 *获取区域
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/county")
	public void county(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.county;
		log.info("获取区域数据");
		Map<String, String> map = new HashMap<String, String>();

		String pid = CoreUtils.trim(request.getParameter("id")); 
		map.put("cityID", pid); 
		log.info("区县ID:"+pid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsondata="";
		try {
			 jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		} 
	}

	@RequestMapping({"/getvalcode"})
	public void getValCode(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl+"/api/AgentAccount/SendMessage";
		log.info(url);
		HashMap map = new HashMap();
		String phone = CoreUtils.trim(request.getParameter("phone"));
		String name = CoreUtils.trim(request.getParameter("agac_name"));
		map.put("phone", phone);
		map.put("name", name);
		map.put("key", "8803ea17cdb4c868");

		try {
			log.info(map);
			String e = HttpTookit.doGet(url, map);
			log.info(e);
			this.print(response, e);
		} catch (Exception var9) {
			var9.printStackTrace();
			this.print(response, errorjson);
		}
	}

}
