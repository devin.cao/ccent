package com.yun.web.controllers;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yun.wx.WXConstants;

import jsp.weixin.ParamesAPI.util.WeixinUtil;
import jsp.weixin.encryption.util.AesException;
import jsp.weixin.encryption.util.WXBizMsgCrypt;
import jsp.weixin.msg.Resp.TextMessage;
import jsp.weixin.msg.Util.MessageUtil;
import jsp.weixin.oauth2.util.GOauth2Core;

@Controller
@RequestMapping("/wx/")
public class WxServerAPI {
	private static final Logger log = Logger.getLogger(WxServerAPI.class);

	@RequestMapping(value = "/auth", method = { RequestMethod.GET,RequestMethod.POST })
	public void auth(HttpServletRequest request, HttpServletResponse response)throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String code = request.getParameter("code");
		if (!"authdeny".equals(code)) {
			String access_token = WeixinUtil.getAccessToken(WXConstants.corpId,
					WXConstants.secret).getToken();
			// agentid 跳转链接时所在的企业应用ID
			// 管理员须拥有agent的使用权限；agentid必须和跳转链接时所在的企业应用ID相同
			String openID = GOauth2Core.GetOpenid(access_token, code, "3");
			request.setAttribute("openID", openID);
		} else {
			out.print("授权获取失败，至于为什么，自己找原因。。。");
		}
		request.getRequestDispatcher("index.jsp").forward(request, response);
	} 		
	

	@RequestMapping(value = "/index", method = { RequestMethod.GET,RequestMethod.POST })
	public String index(HttpServletRequest request, HttpServletResponse response)throws Exception {
		/**
		 * 功能：验证开发者 微信服务器Get方式发送请求,开发者通过微信服务器发过来的参数来处理后与signature对比，一样则通过验证
		 */
		String method = request.getMethod().toUpperCase().trim();
		 log.info("method=" + method);  
		if (method.equals("GET")) {
			// 微信加密签名
			String msg_signature = request.getParameter("msg_signature");
			// 时间戳
			String timestamp = request.getParameter("timestamp");
			// 随机数
			String nonce = request.getParameter("nonce");
			// 随机字符串
			String echostr = request.getParameter("echostr");
			// 打印请求地址
			log.info("request=" + request.getRequestURL());
			// 流
			log.info(echostr);
			PrintWriter out = response.getWriter();
			// 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
			String result = null;
			try {
				WXBizMsgCrypt wxcpt = new WXBizMsgCrypt(WXConstants.token,
						WXConstants.encodingAESKey, WXConstants.corpId);
				// 验证URL函数
				result = wxcpt.VerifyURL(msg_signature, timestamp, nonce, echostr);
			} catch (AesException e) {
				e.printStackTrace();
			}
			if (result == null) {
				// result为空，赋予token
				result = WXConstants.token;
			}
			// 拼接请求参数
			String str = msg_signature + " " + timestamp + " " + nonce + " "
					+ echostr;
			// 打印参数+地址+result
			log.info("Exception:" + result + " "
					+ request.getRequestURL() + " " + "FourParames:" + str);
			out.print(result);
			out.close();
			out = null;
		} else if (method.equals("POST")) {
			// 将请求、响应的编码均设置为UTF-8（防止中文乱码）
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");

			// 微信加密签名
			String msg_signature = request.getParameter("msg_signature");
			// 时间戳
			String timestamp = request.getParameter("timestamp");
			// 随机数
			String nonce = request.getParameter("nonce");
			String access_token = WeixinUtil.getAccessToken(WXConstants.corpId,
					WXConstants.secret).getToken();
			HttpSession session = request.getSession();
			session.setAttribute("noncestr", nonce);
			session.setAttribute("timestamp", timestamp);
			session.setAttribute("access_token", access_token);
			
			// 从请求中读取整个post数据
			InputStream inputStream = request.getInputStream();
			// commons.io.jar 方法
			String Post = IOUtils.toString(inputStream, "UTF-8");
			// Post打印结果
			log.info(Post);
		
			
//			String Msg = "";
//			WXBizMsgCrypt wxcpt = null;
//			try {
//				wxcpt = new WXBizMsgCrypt(ParamesAPI.token,
//						ParamesAPI.encodingAESKey, ParamesAPI.corpId);
//				// 解密消息
//				Msg = wxcpt.DecryptMsg(msg_signature, timestamp, nonce, Post);
//			} catch (AesException e) {
//				e.printStackTrace();
//			}
//			// Msg打印结果
//			log.info("Msg打印结果：" + Msg);
//
//			// 调用核心业务类接收消息、处理消息
//			String respMessage =processRequest(Msg);
//			String contextPath = request.getContextPath();
//			
			// respMessage打印结果
			
//			String encryptMsg = "";
//			try {
//				// 加密回复消息
//				encryptMsg = wxcpt.EncryptMsg(respMessage, timestamp, nonce);
//			} catch (AesException e) {
//				e.printStackTrace();
//			}
			
			// 响应消息
//			PrintWriter out = response.getWriter();
//			out.print(encryptMsg);
//			out.close();
		}
		return null;
	} 		
	/**
	 * 处理微信发来的请求
	 * 
	 * @param request
	 * @return xml
	 */
	public  String processRequest(String request) {
		// xml格式的消息数据
		String respXml = null;
		// 默认返回的文本消息内容
		String respContent = "未知的消息类型！";
		try {
			// 调用parseXml方法解析请求消息
			Map<String, String> requestMap = MessageUtil.parseXml(request);
			// 发送方帐号
			String fromUserName = requestMap.get("FromUserName");
			// 开发者微信号
			String toUserName = requestMap.get("ToUserName");
			// 消息类型
			String msgType = requestMap.get("MsgType");

			// 回复文本消息
			TextMessage textMessage = new TextMessage();
			textMessage.setToUserName(fromUserName);
			textMessage.setFromUserName(toUserName);
			textMessage.setCreateTime(new Date().getTime());
			textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);

			// 文本消息
			if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
				respContent = "您发送的是文本消息！";
			}
			// 图片消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {
				respContent = "您发送的是图片消息！";
			}
			// 语音消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {
				respContent = "您发送的是语音消息！";
			}
			// 视频消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VIDEO)) {
				respContent = "您发送的是视频消息！";
			}
			// 地理位置消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
				respContent = "您发送的是地理位置消息！";
			}
			// 链接消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {
				respContent = "您发送的是链接消息！";
			}
			// 事件推送
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {
				// 事件类型
				String eventType = requestMap.get("Event");
				// 关注
				if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
					respContent = "谢谢您的关注！";
				}
				// 取消关注
				else if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {
					// TODO 取消订阅后用户不会再收到公众账号发送的消息，因此不需要回复
				}
				// 扫描带参数二维码
				else if (eventType.equals(MessageUtil.EVENT_TYPE_SCAN)) {
					// TODO 处理扫描带参数二维码事件
					
				}
				// 上报地理位置
				else if (eventType
						.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
					// TODO 处理上报地理位置事件
				}
				// 自定义菜单
				else if (eventType.toUpperCase().equals(MessageUtil.EVENT_TYPE_CLICK)) {
					// TODO 处理菜单点击事件
					// 事件KEY值，与创建自定义菜单时指定的KEY值对应
					String eventKey = requestMap.get("EventKey");
					if(eventKey.equals("partodlist")){
						//配件订单
					}else if(eventKey.equals("productodlist")){
						//成品订单
					}else if(eventKey.equals("deviceuser")){
						//终端用户
					}else if(eventKey.equals("workodlist")){
						//工单
					}else if(eventKey.equals("productscan")){
						//产品扫码
					}
					log.info("EventKey=" + eventKey);
					respContent = "点击的菜单KEY:" + eventKey;
					
				}
			}
			// 设置文本消息的内容
			textMessage.setContent(respContent);
			// 将文本消息对象转换成xml
			respXml = MessageUtil.textMessageToXml(textMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return respXml;
	}
	
	
	
}
