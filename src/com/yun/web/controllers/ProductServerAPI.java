package com.yun.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yun.core.utils.PermissionCheck;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yun.core.BaseController;
import com.yun.core.PageUtils;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.PropertyStrategyWrapper;
import com.yun.core.utils.HttpTookit;
import com.yun.order.model.Prin;
import com.yun.order.model.Product;
import com.yun.wx.JsapiTicketUtil;
import com.yun.wx.Sign;
import com.yun.wx.WXConstants;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;

@Controller
@RequestMapping("/product/")
public class ProductServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(ProductServerAPI.class);
	/**
	 * 获取产品明细
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/scan")
	public String wxsign(Model model,String barcode,HttpServletRequest request, HttpServletResponse response) throws Exception {
	 	String jsapi_ticket =JsapiTicketUtil.getJSApiTicket();
		String sessionuser = getSessionUser(request);
		// 注意 URL 一定要动态获取，不能 hardcode
		String url = "http://crm.entive.com/ccent/product/scan";//url是你请求的一个action或者controller地址，并且方法直接跳转到使用jsapi的jsp界面
		String flag = CoreUtils.trim(request.getParameter("flag"));
		if (flag.equals("")) {
			flag = (String) request.getSession().getAttribute("orderpwd");
			if (StringUtils.isEmpty(flag)){
				model.addAttribute("utype", "7");
				return "user/orderpwdcheck";
			}
		}
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		request.getSession().setAttribute("orderpwd", flag);
		Map<String, String> ret = Sign.sign(jsapi_ticket, url);
		for (Map.Entry entry : ret.entrySet()) {
			model.addAttribute((String)entry.getKey(), entry.getValue());
		}
		model.addAttribute("appId", WXConstants.corpId);//应用ID
		model.addAttribute("flag", flag);
		return "product/product_scan";
	}
	
	/**
	 * 获取产品明细
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get")
	public String get(Model model,String barcode,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getProductInfoUrl;
		Map<String, String> map = new HashMap<String, String>();
		//barcode="43566111";
		if (barcode.contains("%")){
			barcode = barcode.replace("%", "\\");
		}
		map.put("barcode", barcode);
		map.put("key", Constants.httpkey);
		String sessionuser=getSessionUser(request);
		map.put("usr",sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:"+jsondata);
			JSONObject json = JSONObject.fromObject(jsondata);//userStr是json字符串
			Prin product= (Prin)JSONObject.toBean(json, Prin.class);
			model.addAttribute("entity", product);
		} catch (Exception e) {
			
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "product/product_info";
	}
	
	/**
	 * 获取产品 列表
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public String list(Model model,String barcode,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getproductUrl;
		Map<String, String> map = new HashMap<String, String>();
		String prp= CoreUtils.trim(request.getParameter("prp"));
		String pageIndex= CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize= CoreUtils.trim(request.getParameter("pageSize"));
		String prod_code= CoreUtils.trim(request.getParameter("prod_code"));
		String prod_name= CoreUtils.trim(request.getParameter("prod_name"));
		
		if(pageIndex.equals("")){
			 pageIndex ="1";
		}
        if(pageSize.equals("")){
    	    pageSize ="5";
		}
        map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("prod_property", prp);
		map.put("prod_code", prod_code);
		map.put("prod_name", prod_name);
		map.put("key", Constants.httpkey);
		String sessionuser=getSessionUser(request);
		map.put("usr",sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
            log.info("rs:"+jsondata);
			List<Product> list=new ArrayList<Product>();
			if(jsondata.startsWith("[")&&jsondata.endsWith("]")){
				     JSONArray json = JSONArray.fromObject(jsondata);//userStr是json字符串
					 JsonConfig config = new JsonConfig();  
					 Map<String, Object> classMap = new HashMap<String, Object>();  
					 config.setClassMap(classMap);  
					 config.setRootClass(Product.class);  
					 config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT)); 
					 list= (List<Product>)JSONArray.toCollection(json,config);
			}
			PageUtils pageUtils=new PageUtils();
			if(list!=null&&list.size()>0){
				int total=list.get(0).getTotal();
				 pageUtils=new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("prod_code", prod_code); 
			model.addAttribute("prod_name", prod_name); 
			model.addAttribute("prp", prp); 
			model.addAttribute("pageUtils", pageUtils); 
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);	
			model.addAttribute("pageSize", pageSize);	
		} catch (Exception e) {
			
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "product/product_list";
	}
	/**
	 * 获取产品price
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getPrice")
	public void getPrice(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String url = Constants.baseurl + Constants.getPriceUrl;
		Map<String, String> map = new HashMap<String, String>();
		String productid= CoreUtils.trim(request.getParameter("productid"));
		map.put("productid", productid);
		String sessionuser=getSessionUser(request);
		map.put("account",sessionuser);
		map.put("key", Constants.httpkey);
		map.put("usr",sessionuser);
		try {
			String rs = HttpTookit.doGet(url, map);
			String jsondata="";
            if(jsondata.startsWith("{")&&jsondata.endsWith("}")){
            	
            }else{
            	jsondata="{\"StatusCode\":1,\"Message\":\"查询成功\",\"Price\":"+rs+"}";
            }
            log.info("jsondata:"+jsondata);
            print(response, jsondata);
		} catch (Exception e) {
			print(response, errorjson);
		}
	}

	/**
	 * 获取产品price
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/price")
	public String Price(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String flag = CoreUtils.trim(request.getParameter("flag"));
		String sessionuser=getSessionUser(request);
        if (flag.equals("")) {
            flag = (String) request.getSession().getAttribute("orderpwd");
            if (StringUtils.isEmpty(flag)){
                model.addAttribute("utype", "3");
                return "user/orderpwdcheck";
            }
        }
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.getPriceAndProductUrl;
		Map<String, String> map = new HashMap<String, String>();
		String pageIndex= CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize= CoreUtils.trim(request.getParameter("pageSize"));
		String prod_code= CoreUtils.trim(request.getParameter("prod_code"));
		String prod_name= CoreUtils.trim(request.getParameter("prod_name"));
		if(pageIndex.equals("")){
			pageIndex ="1";
		}
		if(pageSize.equals("")){
			pageSize ="5";
		}
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);

		map.put("usr",sessionuser);
		if (prod_code.contains("%")){
			prod_code = prod_code.replace("%", "\\");
		}
		map.put("prod_code", prod_code);
		map.put("prod_name", prod_name);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:"+jsondata);
			List<Product> list=new ArrayList<Product>();
			if(jsondata.startsWith("[")&&jsondata.endsWith("]")){
				JSONArray json = JSONArray.fromObject(jsondata);//userStr是json字符串
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				config.setClassMap(classMap);
				config.setRootClass(Product.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list= (List<Product>)JSONArray.toCollection(json,config);
			}
			PageUtils pageUtils=new PageUtils();
			if(list!=null&&list.size()>0){
				int total=list.get(0).getTotal();
				pageUtils=new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("pageUtils", pageUtils);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("prod_code", prod_code);
			model.addAttribute("prod_name", prod_name);
			model.addAttribute("flag", flag);
		} catch (Exception e) {

		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "price/price_product_list";

	}
}
