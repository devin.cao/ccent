package com.yun.web.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yun.wx.WXConstants;

import net.sf.json.JSONObject;
import jsp.weixin.ParamesAPI.util.WeixinUtil;
import jsp.weixin.encryption.util.SHA1Util;
@Controller
@RequestMapping("/api/ticket/")
public class GetTicket extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@RequestMapping(value = "/index", method = { RequestMethod.GET,RequestMethod.POST })
	public void test(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		String wxurl =request.getParameter("wxurl");
		System.out.println("url==="+wxurl);
		String access_token = WeixinUtil.getAccessToken(WXConstants.corpId,
				WXConstants.secret).getToken();
		String jspapi_ticket = WeixinUtil.getjsapi_ticket(access_token)
				.getTicket();
		PrintWriter out = response.getWriter();		
		Map<String,String>	signature = SHA1Util.sign(jspapi_ticket,wxurl);	
		JSONObject jo=JSONObject.fromObject(signature);//转化Map对象  
		out.print(jo);
		out.close();
	
	}

	public static void main(String[] args) {
		String access_token = WeixinUtil.getAccessToken(WXConstants.corpId,
				WXConstants.secret).getToken();
		String jspapi_ticket = WeixinUtil.getjsapi_ticket(access_token)
				.getTicket();
		try {
			String url="http://yuhai422.51vip.biz:28071/ccent/frontedjs.html";
			Map<String, String>	signature = SHA1Util.sign(jspapi_ticket,url); 
			System.out.println(signature);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
