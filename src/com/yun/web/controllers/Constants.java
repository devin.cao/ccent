package com.yun.web.controllers;

public class Constants {

	public static final String baseurl="http://118.178.182.63:88/ccentive/";
	

	public static final String getAgentAccountUrl="api/AgentAccount/GetAgentAccount";
	public static final String updatePasswordUrl="api/AgentAccount/UpdatePassword";
	
	public static final String checkOrderPasswordUrl="api/AgentAccount/CheckOrderPassword";
	
	public static final String addAgentAccountUrl="api/AgentAccount/AddAgentAccount";
	
	public  static  final String getproductUrl= "api/product/getproduct";
	public  static final String getPriceUrl="api/product/GetProductPrice";
	public  static final String getPriceAndProductUrl="api/product/GetAllProduct";
	//
	public  static  final String getConsumerUrl= "api/Consumer/GetConsumer";
	public  static  final String addConsumerUrl= "api/Consumer/AddConsumer";
	public  static  final String sendMessageUrl= "api/Consumer/SendMessage";
	public  static  final String AddMultiConsumer= "api/Consumer/AddMultiConsumer";
	public  static  final String getAllConsumerUrl= "api/Consumer/GetAllConsumer";

	public  static  final String getProductInfoUrl= "api/ProductInfo/GetProductInfo";
	public  static  final String getpartsorderUrl= "api/partsorder/getpartsorder";
	
	//配件订单明细
	
	public  static  final String getPartsDetailsUrl= "api/partsorder/GetPartsDetails";
	//9.新增配件订单明细
	public  static  final String addPartsDetailsUrl= "api/PartsOrder/AddPartsDetails";
	
	public  static  final String AddPartsOrderUrl= "api/PartsOrder/AddPartsOrder";
	public  static  final String submitPartOrderUrl= "api/partsorder/SubmitOrder";
	public  static  final String delPartOrderUrl= "api/PartsOrder/DelOrder";
	public  static  final String delPartOrderDetailsUrl= "api/PartsOrder/DelOrderDetails";
	
	public  static  final String AddPartsDetailsUrl= "api/PartsOrder/AddPartsDetails";
	public  static  final String getGoodsOrderUrl= "api/GoodsOrder/GetGoodsOrder";
	
	//16.查询成品订单明细
	public  static  final String getGoodsDetailsUrl= "api/GoodsOrder/GetGoodsDetails";
	//11.新增成品订单明细
	public  static  final String addGoodsDetailsUrl= "api/GoodsOrder/AddGoodsDetails";
	
	public  static  final String AddGoodsOrderUrl= "api/GoodsOrder/AddGoodsOrder";
	//提交成品订单
	public  static  final String submitGoodsOrderUrl= "api/GoodsOrder/SubmitOrder";
	public  static  final String delGoodsOrderUrl= "api/GoodsOrder/DelOrder";
	public  static  final String delGoodsOrderDetailsUrl= "api/GoodsOrder/DelOrderDetails";
	
	public  static  final String AddGoodsDetailsUrl= "api/GoodsOrder/AddGoodsDetails";
	
	//查询工单
	public  static  final String getworkorderUrl= "api/workorder/getworkorder";
	//修改工单
	public  static  final String modifyWorkOrderUrl= "api/workorder/ModifyWorkOrder";
	
	//27.根据ID查成品订单
	public  static  final String getGoodsOrderByIDUrl= "api/GoodsOrder/GetGoodsOrderByID";
	
	//28.根据ID查配件订单
	public  static  final String getPartsOrderByIDUrl= "api/partsorder/GetPartsOrderByID";
	public  static  final String getCompanyUrl= "api/AgentAccount/GetCompany";
	//成品订单数量修改
	public  static  final String goodsOrderMDOrderDetailsUrl= "api/GoodsOrder/MDOrderDetails";
	//配件订单数量修改
	public  static  final String partsOrderMDOrderDetailsUrl= "api/PartsOrder/MDOrderDetails";
		
	//获取省份数据
	public static final String province="api/BaseData/GetProvince";
	//获取城市数据
	public static final String city="api/BaseData/GetCity";
	//获取区域数据
	public static final String county="api/BaseData/GetCounty";
	//订单明细查询
	public static final String orderReport="/api/Company/GetOrderReport";
	//回款查询
	public static final String receiveReport="/api/Company/GetReceiveReport";
	//指标查询
	public static final String getTargetList="/api/Company/GetEmpCompany";
    //指标详情查询
	public static final String getTargetDetail="/api/Company/GetTarget";
    //新增指标详情
	public static final String addTargetDetail="/api/Company/AddTarget";



	public static final String httpkey="8803ea17cdb4c868";

	
	public static final String httptimeout_error="{\"StatusCode\":-1,\"Message\":\"请求超时\"}";
	public static final String httpservice_error="{\"StatusCode\":-1,\"Message\":\"接口异常\"}";
	
	public static final String nodatalisttrips="无数据";
	public static final  String PAGESIZE="10";
	
	public static final String getAgentSendMessage="api/AgentAccount/SendMessage";
	public static final String getVerfy="api/AgentAccount/VerifyMsg";
	// 成品、物料类型
	public static final String type_goods="1";//成品
	public static final String type_part ="2";//配件
	public static final String type_bom ="3";//物料
	public static final String type_traditional ="4";//传统机
}
