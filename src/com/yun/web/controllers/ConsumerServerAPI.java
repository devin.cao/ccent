package com.yun.web.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yun.core.utils.PermissionCheck;
import net.sf.json.JSON;
import net.sf.json.util.CycleDetectionStrategy;
import org.apache.commons.httpclient.HttpClient;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yun.core.BaseController;
import com.yun.core.PageUtils;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.PropertyStrategyWrapper;
import com.yun.core.utils.HttpTookit;
import com.yun.order.model.Consumer;
import com.yun.wx.JsapiTicketUtil;
import com.yun.wx.Sign;
import com.yun.wx.WXConstants;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;

@Controller
@RequestMapping("/consumer/")
public class ConsumerServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(ConsumerServerAPI.class);
	private String s;

	/**
	 * 查安装工单
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getList")
	public String list(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String sessionuser=getSessionUser(request);
		log.info("session:"+sessionuser);
		String url = Constants.baseurl + Constants.getConsumerUrl;
		Map<String, String> map = new HashMap<String, String>();
		String cons_name= CoreUtils.trim(request.getParameter("cons_name"));
		String cons_phonenumber= CoreUtils.trim(request.getParameter("cons_phonenumber"));
		
		String pageIndex= CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize= CoreUtils.trim(request.getParameter("pageSize"));
		String q= CoreUtils.trim(request.getParameter("q"));
		if(pageIndex.equals("")){
			 pageIndex ="1";
		}
        if(pageSize.equals("")){
    	    pageSize ="10";
		}
       
		map.put("cons_name", cons_name);
		map.put("cons_phonenumber", cons_phonenumber);
		
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);
		map.put("usr",sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:"+jsondata);
			List<Consumer> list=new ArrayList<Consumer>();
			if(jsondata.startsWith("[")&&jsondata.endsWith("]")){
				 JSONArray json = JSONArray.fromObject(jsondata);//userStr是json字符串
				 JsonConfig config = new JsonConfig();  
				 Map<String, Object> classMap = new HashMap<String, Object>();  
				 config.setClassMap(classMap);  
				 config.setRootClass(Consumer.class);  
				 config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT)); 
				 list= (List<Consumer>)JSONArray.toCollection(json,config);
			}
			PageUtils pageUtils=new PageUtils();
			if(list!=null&&list.size()>0){
				int total=list.get(0).getTotal();
				 pageUtils=new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("q", q); 
			model.addAttribute("pageUtils", pageUtils); 
			model.addAttribute("cons_name", cons_name);
			model.addAttribute("cons_phonenumber", cons_phonenumber);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);	
			model.addAttribute("pageSize", pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "consumer/consumer_list";
	}
   
	/**
	 * 查安装工单
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get")
	public String get(Model model,String info,HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println(info);
		String s = info.toString().replaceAll("'", "\"").replaceAll("(\\r\\n|\\r|\\n|\\n\\r)","");
		System.out.println("info:"+info);
		System.out.println("s:"+s);
		org.json.JSONObject jsonObject = new org.json.JSONObject(s);
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes(false);
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		config.setExcludes(new String[]{//只要设置这个数组，指定过滤哪些字段。

		});
		JSONObject json = JSONObject.fromObject(jsonObject.toString(),config);//userStr是json字符串
		Consumer consumer= (Consumer)JSONObject.toBean(json, Consumer.class);
		model.addAttribute("item", consumer);
		return "consumer/consumer_info";
	}
	/**
	 * 查安装工单
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toEdit")
	public String edit(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String jsapi_ticket =JsapiTicketUtil.getJSApiTicket();
		   String url = "http://crm.entive.com:8080/ccent/consumer/toEdit";
		 //String url = "http://localhost:8080/consumer/toEdit";
	        Map<String, String> ret = Sign.sign(jsapi_ticket, url);
	        for (Map.Entry entry : ret.entrySet()) {
	            model.addAttribute((String)entry.getKey(), entry.getValue());
	        }
	        model.addAttribute("appId", WXConstants.corpId);//应用ID
	    	model.addAttribute("cons_installationdate", CoreUtils.DateToStr(new Date()));
		return "consumer/consumer_edit";
	}
	/**
	 * 添加端用户
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doEdit")
	public void doEdit(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.addConsumerUrl;
		Map<String, String> map = new HashMap<String, String>();
		/*Enumeration enu=request.getParameterNames();
		while(enu.hasMoreElements()){
			String paraName=(String)enu.nextElement();
			System.out.println("云行到了这里"+paraName+": "+request.getParameter(paraName));
		}*/
		String cons_name=CoreUtils.trim(request.getParameter("cons_name"));
		String cons_phonenumber=CoreUtils.trim(request.getParameter("cons_phonenumber"));
		String cons_phonecode=CoreUtils.trim(request.getParameter("cons_phonecode"));
		
		String cons_source=CoreUtils.trim(request.getParameter("cons_source"));
		String cons_birthday=CoreUtils.trim(request.getParameter("cons_birthday"));
		String cons_gender=CoreUtils.trim(request.getParameter("cons_gender"));
		
		String cons_address=CoreUtils.trim(request.getParameter("cons_address"));
		String cons_productoption=CoreUtils.trim(request.getParameter("cons_productoption"));
		String cons_province=CoreUtils.trim(request.getParameter("cons_province"));
		String cons_city=CoreUtils.trim(request.getParameter("cons_city"));
		String cons_county=CoreUtils.trim(request.getParameter("cons_county"));
		String cons_purchaseplace=CoreUtils.trim(request.getParameter("cons_purchaseplace"));
		String cons_purchasedate=CoreUtils.trim(request.getParameter("cons_purchasedate"));
		String cons_installationdate=CoreUtils.trim(request.getParameter("cons_installationdate"));
		String cons_Installationname=CoreUtils.trim(request.getParameter("cons_Installationname"));
		String cons_Installationtel=CoreUtils.trim(request.getParameter("cons_Installationtel"));
		String cons_company=CoreUtils.trim(request.getParameter("cons_company"));
		String cons_headsn=CoreUtils.trim(request.getParameter("cons_headsn"));
		String cons_bodysn=CoreUtils.trim(request.getParameter("cons_bodysn"));
		String cons_screenwificode=CoreUtils.trim(request.getParameter("cons_screenwificode"));
		String cons_productcategory=CoreUtils.trim(request.getParameter("cons_productcategory"));
		
		String cons_producttype=CoreUtils.trim(request.getParameter("cons_producttype"));
		String cons_manufacturedate=CoreUtils.trim(request.getParameter("cons_manufacturedate"));
		cons_company=(String) request.getSession().getAttribute("sessionuser");//当前登录用户
		map.put("cons_name", cons_name);
		map.put("cons_phonenumber", cons_phonenumber);
		map.put("cons_authcode", cons_phonecode);//2017/4/6新增
		
		map.put("cons_source", cons_source);
		map.put("cons_birthday", cons_birthday);
		map.put("cons_gender", cons_gender);
		map.put("cons_address", cons_address);
		map.put("cons_productoption", cons_productoption);
		map.put("cons_province", cons_province);
		map.put("cons_city", cons_city);
		map.put("cons_county", cons_county);
		map.put("cons_purchaseplace", cons_purchaseplace);
		map.put("cons_purchasedate", cons_purchasedate);
		map.put("cons_installationdate", cons_installationdate);
		map.put("cons_Installationname", cons_Installationname);
		map.put("cons_Installationtel", cons_Installationtel);
		map.put("cons_company", cons_company);
		map.put("cons_headsn", cons_headsn);
		map.put("cons_bodysn", cons_bodysn);
		map.put("cons_screenwificode", cons_screenwificode);
		map.put("cons_producttype", cons_producttype);
		map.put("cons_manufacturedate", cons_manufacturedate);
		map.put("cons_productcategory", cons_productcategory);
		map.put("key", Constants.httpkey);
		String sessionuser=getSessionUser(request);
		map.put("usr",sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			e.printStackTrace();
			print(response, errorjson);
		}
		
	}
	/**
	 * 获取验证码
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCode")
	public void getCode(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.sendMessageUrl;
		Map<String, String> map = new HashMap<String, String>();
		String phone=CoreUtils.trim(request.getParameter("phone"));
		map.put("phone", phone);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			e.printStackTrace();
			print(response, errorjson);
		}

	}

	/*
	 * 获取省份数据
	 * */
	@RequestMapping(value = "/province")
	public void getProvince(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.province;
		Map<String, String> map = new HashMap<String, String>();
		String jsondata = "";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			e.printStackTrace();
			print(response, errorjson);
		}
	}

	/**
	 * 获取城市
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/city")
	public void city(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.city;
		log.info("############城市");
		Map<String, String> map = new HashMap<String, String>();

		String pid = CoreUtils.trim(request.getParameter("id"));
		log.info("省份代号：" + pid);
		map.put("provinceID", pid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsondata = "";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}
	}


	/**
	 * 获取区域
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/county")
	public void county(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.county;
		log.info("获取区域数据");
		Map<String, String> map = new HashMap<String, String>();

		String pid = CoreUtils.trim(request.getParameter("id"));
		map.put("cityID", pid);
		log.info("区县ID:" + pid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsondata = "";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}
	}

	/**
	 * 添加端用户
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doEditMultiple")
	public void doEditMultiple(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.AddMultiConsumer;
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper json = new ObjectMapper();
		Enumeration enu = request.getParameterNames();
		while (enu.hasMoreElements()) {
			String paraName = (String) enu.nextElement();
			log.info("云行到了这里" + paraName + ": " + request.getParameter(paraName));
		}
		String cons_name = CoreUtils.trim(request.getParameter("cons_name"));
		String cons_phonenumber = CoreUtils.trim(request.getParameter("cons_phonenumber"));
		String cons_phonecode = CoreUtils.trim(request.getParameter("cons_phonecode"));

		String cons_source = CoreUtils.trim(request.getParameter("cons_source"));
		String cons_birthday = CoreUtils.trim(request.getParameter("cons_birthday"));
		String cons_gender = CoreUtils.trim(request.getParameter("cons_gender"));

		String cons_address = CoreUtils.trim(request.getParameter("cons_address"));
		//String cons_productoption=CoreUtils.trim(request.getParameter("cons_productoption"));
		String cons_province = CoreUtils.trim(request.getParameter("cons_province"));
		String cons_city = CoreUtils.trim(request.getParameter("cons_city"));
		String cons_county = CoreUtils.trim(request.getParameter("cons_county"));
		String cons_purchaseplace = CoreUtils.trim(request.getParameter("cons_purchaseplace"));
		String cons_purchasedate = CoreUtils.trim(request.getParameter("cons_purchasedate"));
		String cons_installationdate = CoreUtils.trim(request.getParameter("cons_installationdate"));
		String cons_Installationname = CoreUtils.trim(request.getParameter("cons_Installationname"));
		String cons_Installationtel = CoreUtils.trim(request.getParameter("cons_Installationtel"));
		String cons_company = CoreUtils.trim(request.getParameter("cons_company"));
		//String cons_headsn=CoreUtils.trim(request.getParameter("cons_headsn"));
		//String cons_bodysn=CoreUtils.trim(request.getParameter("cons_bodysn"));
		//String cons_screenwificode=CoreUtils.trim(request.getParameter("cons_screenwificode"));
		//String cons_productcategory=CoreUtils.trim(request.getParameter("cons_productcategory"));

		//String cons_producttype=CoreUtils.trim(request.getParameter("cons_producttype"));
		//String cons_manufacturedate=CoreUtils.trim(request.getParameter("cons_manufacturedate"));
		String ConsumerBody = CoreUtils.trim(request.getParameter("prod"));
		cons_company = (String) request.getSession().getAttribute("sessionuser");//当前登录用户
		map.put("cons_name", cons_name);
		map.put("cons_phonenumber", cons_phonenumber);
		map.put("cons_authcode", cons_phonecode);//2017/4/6新增

		map.put("cons_source", cons_source);
		map.put("cons_birthday", cons_birthday);
		map.put("cons_gender", cons_gender);
		map.put("cons_address", cons_address);
		//map.put("cons_productoption", cons_productoption);
		map.put("cons_province", cons_province);
		map.put("cons_city", cons_city);
		map.put("cons_county", cons_county);
		map.put("cons_purchaseplace", cons_purchaseplace);
		map.put("cons_purchasedate", cons_purchasedate);
		map.put("cons_installationdate", cons_installationdate);
		map.put("cons_Installationname", cons_Installationname);
		map.put("cons_Installationtel", cons_Installationtel);
		map.put("cons_company", cons_company);
		map.put("ConsumerBody", ConsumerBody);
		//map.put("cons_headsn", cons_headsn);
		//map.put("cons_bodysn", cons_bodysn);
		//map.put("cons_screenwificode", cons_screenwificode);
		//map.put("cons_producttype", cons_producttype);
		//map.put("cons_manufacturedate", cons_manufacturedate);
		//map.put("cons_productcategory", cons_productcategory);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsonString = json.writeValueAsString(map);
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		jsonObject.put("ConsumerBody", ConsumerBody);
		jsonString = jsonObject.toString();
		System.out.println("运行到了这里" + jsonString);
		try {
			//String jsondata = HttpTookit.doPost(url, map);
			String jsondata = HttpTookit.doPostJson(url, jsonString);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			e.printStackTrace();
			print(response, errorjson);
		}
	}

	/**
	 * 查包含多个产品的安装工单
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public String getListlist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String sessionuser = getSessionUser(request);
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		log.info("session:" + sessionuser);
		String url = Constants.baseurl + Constants.getAllConsumerUrl;
		Map<String, String> map = new HashMap<String, String>();
		String cons_name = CoreUtils.trim(request.getParameter("cons_name"));
		String cons_phonenumber = CoreUtils.trim(request.getParameter("cons_phonenumber"));

		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		String q = CoreUtils.trim(request.getParameter("q"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "10";
		}

		map.put("cons_name", cons_name);
		map.put("cons_phonenumber", cons_phonenumber);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);
		map.put("usr", sessionuser);
		JSONArray jsonArray = null;
		List<Consumer> result = new ArrayList<Consumer>();
		try {

			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:" + jsondata);

			JSONArray json = JSONArray.fromObject(jsondata);//userStr是json字符串
			for (Object o : json) {
				jsonArray = new JSONArray();
				List<Consumer> list = new ArrayList<Consumer>();
				JSONObject jsonObject = (JSONObject) o;
				jsonArray.add(jsonObject);
				JSONArray consumerBody = jsonObject.getJSONArray("ConsumerBody");
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				//classMap.put("ConsumerBody",Consumer.class);
				config.setClassMap(classMap);
				config.setRootClass(Consumer.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list = (List<Consumer>) JSONArray.toCollection(jsonArray, config);
				list.get(0).setConsumer_body_string(consumerBody.toString().replaceAll("\"", "&quot;"));//双引号转义
				result.add(list.get(0));
			}

			PageUtils pageUtils = new PageUtils();
			if (result != null && result.size() > 0) {
				int total = result.get(0).getTotal();
				pageUtils = new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("q", q);
			model.addAttribute("pageUtils", pageUtils);
			model.addAttribute("cons_name", cons_name);
			model.addAttribute("cons_phonenumber", cons_phonenumber);
			model.addAttribute("entityList", result);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "consumer/consumer_list";
	}



	/**
	 * 查包含多个产品的安装工单（订阅号）
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/listForSubscriptions")
	public String getListlistForSubscriptions(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String sessionuser = getSessionUser(request);
		log.info("session:" + sessionuser);
		String url = Constants.baseurl + Constants.getAllConsumerUrl;
		Map<String, String> map = new HashMap<String, String>();
		String cons_name = CoreUtils.trim(request.getParameter("cons_name"));
		String cons_phonenumber = CoreUtils.trim(request.getParameter("cons_phonenumber"));

		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		String q = CoreUtils.trim(request.getParameter("q"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "10";
		}

		map.put("cons_name", cons_name);
		map.put("cons_phonenumber", cons_phonenumber);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);
		map.put("usr", sessionuser);
		JSONArray jsonArray = null;
		List<Consumer> result = new ArrayList<Consumer>();
		try {

			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:" + jsondata);

			JSONArray json = JSONArray.fromObject(jsondata);//userStr是json字符串
			for (Object o : json) {
				jsonArray = new JSONArray();
				List<Consumer> list = new ArrayList<Consumer>();
				JSONObject jsonObject = (JSONObject) o;
				jsonArray.add(jsonObject);
				JSONArray consumerBody = jsonObject.getJSONArray("ConsumerBody");
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				//classMap.put("ConsumerBody",Consumer.class);
				config.setClassMap(classMap);
				config.setRootClass(Consumer.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list = (List<Consumer>) JSONArray.toCollection(jsonArray, config);
				list.get(0).setConsumer_body_string(consumerBody.toString().replaceAll("\"", "&quot;"));//双引号转义
				result.add(list.get(0));
			}

			PageUtils pageUtils = new PageUtils();
			if (result != null && result.size() > 0) {
				int total = result.get(0).getTotal();
				pageUtils = new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("q", q);
			model.addAttribute("pageUtils", pageUtils);
			model.addAttribute("cons_name", cons_name);
			model.addAttribute("cons_phonenumber", cons_phonenumber);
			model.addAttribute("entityList", result);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "consumer/consumer_list_subscriptions";
	}

	/**
	 * 查安装工单（订阅号）
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toEditForSubscriptions")
	public String editForSubscriptions(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String jsapi_ticket =JsapiTicketUtil.getJSApiTicketForSubscriptions();
		String url = "http://crm.entive.com:8080/ccent/consumer/toEditForSubscriptions";
		//String url = "http://localhost:8080/consumer/toEdit";
		Map<String, String> ret = Sign.signForSubscriptions(jsapi_ticket, url);
		for (Map.Entry entry : ret.entrySet()) {
			model.addAttribute((String)entry.getKey(), entry.getValue());
		}
		model.addAttribute("appId", WXConstants.APPID);//应用ID
		model.addAttribute("cons_installationdate", CoreUtils.DateToStr(new Date()));
		return "consumer/consumer_edit";
	}

	public static void main(String[] args) throws JSONException {

		String info = "{'cons_name':'范水发','cons_Status':'','cons_phonenumber':'18960610168','cons_source':'1','cons_birthday':'1959-09-25T00:00:00','cons_address':'新城花园5#908','cons_productoption':'1','cons_province':'福建省','cons_city':'南平市','cons_county':'延平区','cons_purchaseplace':'华泰楼','cons_purchasedate':'2017-10-05T00:00:00','cons_installationdate':'2018-01-05T00:00:00','cons_Installationname':'李晨','cons_Installationtel':'18039763260','cons_company':'福建南平市康金寿','cons_headsn':'','cons_bodysn':'','cons_producttype':'','cons_manufacturedate':'','cons_productcategory':'','cons_screenwificode':'','cons_gender':'1','consumer_body':[{\"cons_productcategory\":\"集成灶\",\"cons_productoption\":\"1\",\"cons_bodysn\":\"GC20171023099/01.05.23.00020\",\"cons_headsn\":\"T20171216117/01.05.23.00001\",\"cons_screenwificode\":\"\",\"cons_producttype\":\"JJZT-S6\",\"cons_manufacturedate\":null,\"cons_purchaseplace\":null,\"cons_purchasedate\":null}]}";

		String s = info.toString().replaceAll("'", "\"");
		System.out.println(s);
		org.json.JSONObject jsonObject = new org.json.JSONObject(s);
		JsonConfig config = new JsonConfig();
		config.setIgnoreDefaultExcludes(false);
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		config.setExcludes(new String[]{//只要设置这个数组，指定过滤哪些字段。

		});
		JSONObject json = JSONObject.fromObject(jsonObject.toString(),config);//userStr是json字符串
		Consumer consumer= (Consumer)JSONObject.toBean(json, Consumer.class);
		System.out.println(consumer.toString());
	}
}
