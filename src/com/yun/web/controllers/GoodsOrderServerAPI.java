package com.yun.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yun.core.utils.PermissionCheck;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yun.core.BaseController;
import com.yun.core.PageUtils;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.PropertyStrategyWrapper;
import com.yun.core.utils.HttpTookit;
import com.yun.order.model.Company;
import com.yun.order.model.GoodsDetails;
import com.yun.order.model.GoodsOrder;
import com.yun.order.model.PartOrder;
import com.yun.order.model.PartsDetails;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;

@Controller
@RequestMapping("/goodsorder/")
public class GoodsOrderServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(GoodsOrderServerAPI.class);

	/**
	 * 配件订单列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public String list(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String flag = CoreUtils.trim(request.getParameter("flag"));
		String sessionuser = getSessionUser(request);
		if (flag.equals("")) {
			flag = (String) request.getSession().getAttribute("orderpwd");
			if (StringUtils.isEmpty(flag)){
				model.addAttribute("utype", "2");
				return "user/orderpwdcheck";
			}
		}
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
        request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.getGoodsOrderUrl;
		Map<String, String> map = new HashMap<String, String>();
		String goor_name = CoreUtils.trim(request.getParameter("goor_name"));
		String BeginDate = CoreUtils.trim(request.getParameter("BeginDate"));
		String EndDate = CoreUtils.trim(request.getParameter("EndDate"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		String q = CoreUtils.trim(request.getParameter("q"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "10";
		}
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("goor_name", goor_name);
		map.put("BeginDate", BeginDate);
		map.put("EndDate", EndDate);
		map.put("key", Constants.httpkey);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:" + jsondata);
			List<GoodsOrder> list = new ArrayList<GoodsOrder>();
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				JSONArray json = JSONArray.fromObject(jsondata);// userStr是json字符串
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				config.setClassMap(classMap);
				config.setRootClass(GoodsOrder.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list = (List<GoodsOrder>) JSONArray.toCollection(json, config);
			}
			
			PageUtils pageUtils = new PageUtils();
			if (list != null && list.size() > 0) {
				int total = list.get(0).getTotal();
				
				pageUtils = new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("pageUtils", pageUtils);
			model.addAttribute("q", q);
			model.addAttribute("goor_name", goor_name);
			model.addAttribute("BeginDate", BeginDate);
			model.addAttribute("EndDate", EndDate);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "goodsorder/goodsorder_list";
	}

	/**
	 * 查询成品订单明细
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detaillist")
	public String detaillist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getGoodsDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();

		String gode_goodsorderid = CoreUtils.trim(request.getParameter("gode_goodsorderid"));
		String goor_status = CoreUtils.trim(request.getParameter("goor_status"));

		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
	
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "10";
		}
		map.put("gode_goodsorderid", gode_goodsorderid);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:" + jsondata);
			List<GoodsDetails> list = new ArrayList<GoodsDetails>();
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				JSONArray json = JSONArray.fromObject(jsondata);// userStr是json字符串
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				config.setClassMap(classMap);
				config.setRootClass(GoodsDetails.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list = (List<GoodsDetails>) JSONArray.toCollection(json, config);
			}
			String gode_status="1";
			PageUtils pageUtils = new PageUtils();
			if (list != null && list.size() > 0) {
				int total = list.get(0).getTotal();
				gode_status=list.get(0).getGode_status();
				pageUtils = new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("pageUtils", pageUtils);
			model.addAttribute("gode_status", gode_status);
			
			model.addAttribute("gode_goodsorderid", gode_goodsorderid);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);

		} catch (Exception e) {

		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "goodsorder/goodsinfo_list";
	}

	/**
	 * 新增成品订单明细界面
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detailAdd")
	public String detail_toadd(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String gode_goodsorderid = CoreUtils.trim(request.getParameter("gode_goodsorderid"));
		model.addAttribute("gode_goodsorderid", gode_goodsorderid);
		return "goodsorder/goodsinfo_edit";
	}

	/**
	 * 新增成品订单明细
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detailDoAdd")
	public void detailDoAdd(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String url = Constants.baseurl + Constants.addGoodsDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();
		// String gode_name= CoreUtils.trim(request.getParameter("gode_name"));
		String gode_productid = CoreUtils.trim(request.getParameter("gode_productid"));
		String gode_color = CoreUtils.trim(request.getParameter("gode_color"));
		String gode_price = CoreUtils.trim(request.getParameter("gode_price"));
		String gode_quantity = CoreUtils.trim(request.getParameter("gode_quantity"));
		String gode_remarks = CoreUtils.trim(request.getParameter("gode_remarks"));
		String gode_goodsorderid = CoreUtils.trim(request.getParameter("gode_goodsorderid"));
		String gode_amount = CoreUtils.trim(request.getParameter("gode_amount"));
		
		
		// map.put("gode_name", gode_name);
		map.put("gode_productid", gode_productid);
		map.put("gode_color", gode_color);
		map.put("gode_price", gode_price);
		map.put("gode_quantity", gode_quantity);
		map.put("gode_remarks", gode_remarks);
		map.put("gode_goodsorderid", gode_goodsorderid);
		map.put("gode_amount", gode_amount);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}
	}

	/**
	 * 查终端用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get")
	public String get(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		GoodsOrder item = null;
		String id = CoreUtils.trim(request.getParameter("id"));
		// if(info==null||info.equals("")){
		String url = Constants.baseurl + Constants.getGoodsOrderByIDUrl;
		try {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", id);
			map.put("key", Constants.httpkey);
			String sessionuser = getSessionUser(request);
			map.put("usr", sessionuser);
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);

			JsonConfig config = new JsonConfig();
			Map<String, Object> classMap = new HashMap<String, Object>();
			config.setClassMap(classMap);
			config.setRootClass(GoodsOrder.class);
			config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
			JSONObject json = JSONObject.fromObject(jsondata);// userStr是json字符串
			item = (GoodsOrder) JSONObject.toBean(json, config);

		} catch (Exception e) {
			e.printStackTrace();
			// print(response, errorjson);
		}
		// }else{
		// org.json.JSONObject jsonObject = new org.json.JSONObject(info);
		// JSONObject json =
		// JSONObject.fromObject(jsonObject.toString());//userStr是json字符串
		// item= (GoodsOrder)JSONObject.toBean(json, GoodsOrder.class);
		// }

		model.addAttribute("item", item);
		return "goodsorder/goodsorder_info";
	}

	/**
	 * 成品订单添加界面
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toEdit")
	public String edit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getCompanyUrl;
		Company item=new Company();
		try {
			Map<String, String> map = new HashMap<String, String>();
			map.put("key", Constants.httpkey);
			String sessionuser = getSessionUser(request);
			map.put("usr", sessionuser);
			String jsondata = HttpTookit.doGet(url, map);
			
			log.info(jsondata);
			JsonConfig config = new JsonConfig();
			Map<String, Object> classMap = new HashMap<String, Object>();
			config.setClassMap(classMap);
			config.setRootClass(Company.class);
			config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
			JSONObject json = JSONObject.fromObject(jsondata);// userStr是json字符串
			item = (Company) JSONObject.toBean(json, config);

		} catch (Exception e) {
			e.printStackTrace();
			// print(response, errorjson);
		}
		model.addAttribute("item", item);
		model.addAttribute("goor_orderdate", CoreUtils.DateToStr(new Date()));
		return "goodsorder/goodsorder_edit";
	}

	/**
	 * 添加成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doEdit")
	public void doEdit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.AddGoodsOrderUrl;
		Map<String, String> map = new HashMap<String, String>();
		String sessionuser = getSessionUser(request);
		String goor_companyid = sessionuser;
		String goor_address = CoreUtils.trim(request.getParameter("goor_address"));
		String goor_orderdate = CoreUtils.trim(request.getParameter("goor_orderdate"));
		String goor_totalamount = CoreUtils.trim(request.getParameter("goor_totalamount"));
		String goor_earnest = CoreUtils.trim(request.getParameter("goor_earnest"));
		String goor_storeaddress = CoreUtils.trim(request.getParameter("goor_storeaddress"));
		String goor_storetel = CoreUtils.trim(request.getParameter("goor_storetel"));

		goor_totalamount = "0";
		map.put("goor_companyid", goor_companyid);
		map.put("goor_address", goor_address);
		map.put("goor_orderdate", goor_orderdate);
		map.put("goor_totalamount", goor_totalamount);
		map.put("goor_earnest", goor_earnest);
		map.put("goor_storeaddress", goor_storeaddress);
		map.put("goor_storetel", goor_storetel);
		map.put("goor_orderer", "");

		map.put("key", Constants.httpkey);
	
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 * 查终成品订单明细
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getinfo")
	public String getinfo(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String gode_goodsorderid = CoreUtils.trim(request.getParameter("gode_goodsorderid"));
		model.addAttribute("gode_goodsorderid", gode_goodsorderid);
		org.json.JSONObject jsonObject = new org.json.JSONObject(info);
		JSONObject json = JSONObject.fromObject(jsonObject.toString());// userStr是json字符串
		GoodsDetails consumer = (GoodsDetails) JSONObject.toBean(json, GoodsDetails.class);
		model.addAttribute("item", consumer);
		
		return "goodsorder/goodsinfo_info";
	}
	/**
	 * 提交成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/submit")
	public void submit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.submitGoodsOrderUrl;
		Map<String, String> map = new HashMap<String, String>();

		String orderid = CoreUtils.trim(request.getParameter("orderid"));
		map.put("orderid", orderid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 *delete成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public void delete(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.delGoodsOrderUrl;
		Map<String, String> map = new HashMap<String, String>();

		String orderid = CoreUtils.trim(request.getParameter("orderid"));
		map.put("orderid", orderid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 *delete成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detailDelete")
	public void detailDelete(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.delGoodsOrderDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();

		String id = CoreUtils.trim(request.getParameter("id"));
		map.put("id", id);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 *修改成品订单数量
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/changeNum")
	public void changeNum(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.goodsOrderMDOrderDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();

		String id = CoreUtils.trim(request.getParameter("id"));
		String quantity = CoreUtils.trim(request.getParameter("quantity"));
		String amount = CoreUtils.trim(request.getParameter("amount"));
		map.put("id", id);
		map.put("quantity", quantity);
		map.put("amount", amount);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}


	/**
	 * 代理商额度查询
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getCredit")
	public String getCredit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String sessionuser = getSessionUser(request);
		String url = Constants.baseurl + Constants.getCompanyUrl;
		Company item=new Company();
		String flag = CoreUtils.trim(request.getParameter("flag"));
		if (flag.equals("")) {
			flag = (String) request.getSession().getAttribute("orderpwd");
			if (StringUtils.isEmpty(flag)){
				model.addAttribute("utype", "4");
				return "user/orderpwdcheck";
			}
		}
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		request.getSession().setAttribute("orderpwd", flag);
		try {
			Map<String, String> map = new HashMap<String, String>();
			map.put("key", Constants.httpkey);

			map.put("usr", sessionuser);
			String jsondata = HttpTookit.doGet(url, map);

			log.info(jsondata);
			JsonConfig config = new JsonConfig();
			Map<String, Object> classMap = new HashMap<String, Object>();
			config.setClassMap(classMap);
			config.setRootClass(Company.class);
			config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
			JSONObject json = JSONObject.fromObject(jsondata);// userStr是json字符串
			item = (Company) JSONObject.toBean(json, config);

		} catch (Exception e) {
			e.printStackTrace();
			// print(response, errorjson);
		}
		model.addAttribute("item", item);
		model.addAttribute("goor_orderdate", CoreUtils.DateToStr(new Date()));
		model.addAttribute("flag", flag);
		//model.addAttribute("flag",flag);
		return "price/getCredit";
	}
}
