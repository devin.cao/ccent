package com.yun.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yun.core.utils.PermissionCheck;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yun.core.BaseController;
import com.yun.core.PageUtils;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.PropertyStrategyWrapper;
import com.yun.core.utils.HttpTookit;
import com.yun.order.model.WorkOrder;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;

@Controller
@RequestMapping("/wo/")
public class WOServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(WOServerAPI.class);

	/**
	 * 查终工单
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public String list(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getworkorderUrl;
		String sessionuser=getSessionUser(request);
		Map<String, String> map = new HashMap<String, String>();
		String woor_name=CoreUtils.trim(request.getParameter("woor_name"));
		String BeginDate= CoreUtils.trim(request.getParameter("BeginDate"));
		String EndDate= CoreUtils.trim(request.getParameter("EndDate"));
		String pageIndex= CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize= CoreUtils.trim(request.getParameter("pageSize"));
		String q= CoreUtils.trim(request.getParameter("q"));
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		if(pageIndex.equals("")){
			 pageIndex ="1";
		}
        if(pageSize.equals("")){
    	    pageSize ="10";
		}
        map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
        map.put("woor_name", woor_name);
		map.put("BeginDate", BeginDate);
		map.put("EndDate", EndDate);
		map.put("key", Constants.httpkey);

		map.put("usr",sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			 List<WorkOrder> list=new ArrayList<WorkOrder>();
			if(jsondata.startsWith("[")&&jsondata.endsWith("]")){
				 JSONArray json = JSONArray.fromObject(jsondata);//userStr是json字符串
				 JsonConfig config = new JsonConfig();  
				 Map<String, Object> classMap = new HashMap<String, Object>();  
				 config.setClassMap(classMap);  
				 config.setRootClass(WorkOrder.class);  
				 config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT)); 
				 list= (List<WorkOrder>)JSONArray.toCollection(json,config);
			}
			PageUtils pageUtils=new PageUtils();
			if(list!=null&&list.size()>0){
				int total=list.get(0).getTotal();
				 pageUtils=new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("q", q); 
			model.addAttribute("pageUtils", pageUtils); 
		    model.addAttribute("woor_name", woor_name);
			model.addAttribute("BeginDate", BeginDate);
			model.addAttribute("EndDate", EndDate);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);	
			model.addAttribute("pageSize", pageSize);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "wo/wo_list";
	}
   
	/**
	 * 查终端用户
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get")
	public String get(Model model,String info,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String edit=CoreUtils.trim(request.getParameter("edit"));
		
		org.json.JSONObject jsonObject = new org.json.JSONObject(info);
		JSONObject json = JSONObject.fromObject(jsonObject.toString());//userStr是json字符串
		WorkOrder wo= (WorkOrder)JSONObject.toBean(json, WorkOrder.class);
		model.addAttribute("item", wo);
        if(edit.equals("1")){
        	return "wo/wo_edit";
		}else{
			return "wo/wo_info";
		}
		
	}
	/**
	 * 查终端用户
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toEdit")
	public String edit(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		return "wo/wo_edit";
	}
	/**
	 * 工单修改
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doEdit")
	public void doEdit(Model model,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.modifyWorkOrderUrl;
		Map<String, String> map = new HashMap<String, String>();
		String woor_WorkOrderID=CoreUtils.trim(request.getParameter("woor_WorkOrderID"));
		String woor_solution=CoreUtils.trim(request.getParameter("woor_solution"));
		String woor_remark=CoreUtils.trim(request.getParameter("woor_remark"));
		String woor_status=CoreUtils.trim(request.getParameter("woor_status"));
		
		map.put("woor_WorkOrderID", woor_WorkOrderID);
		map.put("woor_solution", woor_solution);
		map.put("woor_status", woor_status);
		map.put("woor_remark", woor_remark);
		
		map.put("key", Constants.httpkey);
		String sessionuser=getSessionUser(request);
		map.put("usr",sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);
		} catch (Exception e) {
			e.printStackTrace();
			print(response, errorjson);
		}
		
	}
}
