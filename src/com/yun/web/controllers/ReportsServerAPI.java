package com.yun.web.controllers;

import com.yun.core.BaseController;
import com.yun.core.PageUtils;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.PropertyStrategyWrapper;
import com.yun.core.utils.HttpTookit;
import com.yun.core.utils.PermissionCheck;
import com.yun.order.model.Company;
import com.yun.order.model.PartOrder;
import com.yun.order.model.PartsDetails;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Controller
@RequestMapping("/reports/")
public class ReportsServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(ReportsServerAPI.class);

	/**
	 * 订单明细列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/orderlist")
	public String orderlist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String flag = CoreUtils.trim(request.getParameter("flag"));
		String sessionuser = getSessionUser(request);
		JSONArray json = null;
		if (flag.equals("")) {
            flag = (String) request.getSession().getAttribute("orderpwd");
            if (StringUtils.isEmpty(flag)){
                model.addAttribute("utype", "8");
                return "user/orderpwdcheck";
            }
        }
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
        request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.orderReport;
		Map<String, String> map = new HashMap<String, String>();
		String BeginDate = CoreUtils.trim(request.getParameter("start_date"));
		String EndDate = CoreUtils.trim(request.getParameter("end_date"));
		String compCode = CoreUtils.trim(request.getParameter("compCode"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "9999";
		}
		if (BeginDate.equals("")){
			BeginDate = DateUtil.formatDate(new Date(),"yyyy-MM-dd");
		}
		if (EndDate.equals("")){
			EndDate = DateUtil.formatDate(new Date(),"yyyy-MM-dd");
		}

		map.put("BeginDate", BeginDate);
		map.put("EndDate", EndDate);
		map.put("CompCode", compCode);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);

		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info("rs:" + jsondata);
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				json = JSONArray.fromObject(jsondata);// userStr是json字符串
			}
			model.addAttribute("BeginDate", BeginDate);
			model.addAttribute("EndDate", EndDate);
			model.addAttribute("compCode", compCode);
			model.addAttribute("entityList", json);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "reports/orderReport";
	}
	/**
	 * 订单明细列表
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/receivelist")
	public String receivelist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String flag = CoreUtils.trim(request.getParameter("flag"));
		String sessionuser = getSessionUser(request);
		JSONArray json = null;
		if (flag.equals("")) {
			flag = (String) request.getSession().getAttribute("orderpwd");
			if (StringUtils.isEmpty(flag)){
				model.addAttribute("utype", "9");
				return "user/orderpwdcheck";
			}
		}
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.receiveReport;
		Map<String, String> map = new HashMap<String, String>();
		String BeginDate = CoreUtils.trim(request.getParameter("start_date"));
		String EndDate = CoreUtils.trim(request.getParameter("end_date"));
		String compCode = CoreUtils.trim(request.getParameter("compCode"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "999999";
		}
		if (BeginDate.equals("")){
			BeginDate = DateUtil.formatDate(new Date(),"yyyy-MM-dd");
		}
		if (EndDate.equals("")){
			EndDate = DateUtil.formatDate(new Date(),"yyyy-MM-dd");
		}

		map.put("BeginDate", BeginDate);
		map.put("EndDate", EndDate);
		map.put("CompCode", compCode);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info("rs:" + jsondata);
            BigDecimal receiveAmtTotal = BigDecimal.ZERO.setScale(2, RoundingMode.UP);
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				json = JSONArray.fromObject(jsondata);// userStr是json字符串
                for (Object o : json) {
                    JSONObject jsonObject = (JSONObject) o;
                    receiveAmtTotal = receiveAmtTotal.add(new BigDecimal(String.valueOf(jsonObject.get("ReceiveAmt"))));
                }
			}

            model.addAttribute("BeginDate", BeginDate);
			model.addAttribute("EndDate", EndDate);
			model.addAttribute("compCode", compCode);
			model.addAttribute("entityList", json);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("flag", flag);
			model.addAttribute("receiveAmtTotal", receiveAmtTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "reports/receiveReport";
	}


}
