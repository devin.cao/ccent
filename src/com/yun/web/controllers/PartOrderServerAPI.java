package com.yun.web.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yun.core.utils.PermissionCheck;
import com.yun.core.utils.PermissionEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yun.core.BaseController;
import com.yun.core.PageUtils;
import com.yun.core.common.CoreUtils;
import com.yun.core.common.PropertyStrategyWrapper;
import com.yun.core.utils.HttpTookit;
import com.yun.order.model.Company;
import com.yun.order.model.PartOrder;
import com.yun.order.model.PartsDetails;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;

@Controller
@RequestMapping("/partorder/")
public class PartOrderServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(PartOrderServerAPI.class);

	/**
	 * 配件订单列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list")
	public String list(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String flag = CoreUtils.trim(request.getParameter("flag"));
		String sessionuser = getSessionUser(request);
        if (flag.equals("")) {
            flag = (String) request.getSession().getAttribute("orderpwd");
            if (StringUtils.isEmpty(flag)){
                model.addAttribute("utype", "1");
                return "user/orderpwdcheck";
            }
        }
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
        request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.getpartsorderUrl;
		Map<String, String> map = new HashMap<String, String>();
		String paor_name = CoreUtils.trim(request.getParameter("paor_name"));
		String BeginDate = CoreUtils.trim(request.getParameter("BeginDate"));
		String EndDate = CoreUtils.trim(request.getParameter("EndDate"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		String q = CoreUtils.trim(request.getParameter("q"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = Constants.PAGESIZE;
		}

		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("paor_name", paor_name);
		map.put("BeginDate", BeginDate);
		map.put("EndDate", EndDate);

		map.put("key", Constants.httpkey);

		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:" + jsondata);
			List<PartOrder> list = new ArrayList<PartOrder>();
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				JSONArray json = JSONArray.fromObject(jsondata);// userStr是json字符串
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				config.setClassMap(classMap);
				config.setRootClass(PartOrder.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list = (List<PartOrder>) JSONArray.toCollection(json, config);
			}
			PageUtils pageUtils = new PageUtils();
			if (list != null && list.size() > 0) {
				int total = list.get(0).getTotal();
				pageUtils = new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("q", q);
			model.addAttribute("pageUtils", pageUtils);
			model.addAttribute("paor_name", paor_name);
			model.addAttribute("BeginDate", BeginDate);
			model.addAttribute("EndDate", EndDate);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "partorder/partorder_list";
	}

	/**
	 * 配件明细列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detaillist")
	public String detaillist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getPartsDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();
		String pade_partsorderid = CoreUtils.trim(request.getParameter("pade_partsorderid"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		String paor_status = CoreUtils.trim(request.getParameter("paor_status"));
		
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "10";
		}
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("pade_partsorderid", pade_partsorderid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info("rs:" + jsondata);
			List<PartsDetails> list = new ArrayList<PartsDetails>();
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				JSONArray json = JSONArray.fromObject(jsondata);// userStr是json字符串
				JsonConfig config = new JsonConfig();
				Map<String, Object> classMap = new HashMap<String, Object>();
				config.setClassMap(classMap);
				config.setRootClass(PartsDetails.class);
				config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
				list = (List<PartsDetails>) JSONArray.toCollection(json, config);

			}
			String pade_status="1";
			PageUtils pageUtils = new PageUtils();
			if (list != null && list.size() > 0) {
				int total = list.get(0).getTotal();
				pade_status= list.get(0).getPade_status();
				pageUtils = new PageUtils(Integer.parseInt(pageIndex), Integer.parseInt(pageSize), total);
			}
			model.addAttribute("pageUtils", pageUtils);
		
			model.addAttribute("pade_status", pade_status);
			model.addAttribute("entityList", list);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("pade_partsorderid", pade_partsorderid);
			model.addAttribute("paor_status", paor_status);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "partorder/partinfo_list";
	}

	/**
	 * 新增配件明细界面
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detailAdd")
	public String detail_toadd(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String pade_partsorderid = CoreUtils.trim(request.getParameter("pade_partsorderid"));
		String paor_status = CoreUtils.trim(request.getParameter("paor_status"));
		model.addAttribute("pade_partsorderid", pade_partsorderid);
		model.addAttribute("paor_status", paor_status);
		return "partorder/partinfo_edit";
	}

	/**
	 * 新增配件明细
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detailDoAdd")
	public void detailDoAdd(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String url = Constants.baseurl + Constants.addPartsDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();
		String pade_partsorderid = CoreUtils.trim(request.getParameter("pade_partsorderid"));
		String pade_name = CoreUtils.trim(request.getParameter("pade_name"));
		String pade_producttype = CoreUtils.trim(request.getParameter("pade_producttype"));
		String pade_productsid = CoreUtils.trim(request.getParameter("pade_productsid"));
		String pade_price = CoreUtils.trim(request.getParameter("pade_price"));
		String pade_unit = CoreUtils.trim(request.getParameter("pade_unit"));
		String pade_quantity = CoreUtils.trim(request.getParameter("pade_quantity"));
		String pade_amount = CoreUtils.trim(request.getParameter("pade_amount"));
		String pade_specialremark = CoreUtils.trim(request.getParameter("pade_specialremark"));
		map.put("pade_partsorderid", pade_partsorderid);
		map.put("pade_name", pade_name);
		map.put("pade_producttype", pade_producttype);
		map.put("pade_productsid", pade_productsid);
		map.put("pade_price", pade_price);
		map.put("pade_unit", pade_unit);
		map.put("pade_quantity", pade_quantity);
		map.put("pade_amount", pade_amount);
		map.put("pade_specialremark", pade_specialremark);

		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}
	}

	/**
	 * 查终端用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get")
	public String get(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PartOrder item = null;
		String id = CoreUtils.trim(request.getParameter("id"));
		// if(info==null||info.equals("")){
		String url = Constants.baseurl + Constants.getPartsOrderByIDUrl;
		try {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", id);
			map.put("key", Constants.httpkey);
			String sessionuser = getSessionUser(request);
			map.put("usr", sessionuser);
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			JsonConfig config = new JsonConfig();
			Map<String, Object> classMap = new HashMap<String, Object>();
			config.setClassMap(classMap);
			config.setRootClass(PartOrder.class);
			config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
			JSONObject json = JSONObject.fromObject(jsondata);// userStr是json字符串
			item = (PartOrder) JSONObject.toBean(json, config);

		} catch (Exception e) {
			e.printStackTrace();
			// print(response, errorjson);
		}
		// }else{
		// org.json.JSONObject jsonObject = new org.json.JSONObject(info);
		// JSONObject json =
		// JSONObject.fromObject(jsonObject.toString());//userStr是json字符串
		// item= (PartOrder)JSONObject.toBean(json, PartOrder.class);
		// }

		model.addAttribute("item", item);
		return "partorder/partorder_info";
	}

	/**
	 * 查终配件订单明细
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getinfo")
	public String getinfo(Model model, String info, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String pade_partsorderid = CoreUtils.trim(request.getParameter("pade_partsorderid"));
		String paor_status = CoreUtils.trim(request.getParameter("paor_status"));
		String pade_specialremark=CoreUtils.trim(request.getParameter("pade_specialremark"));
		model.addAttribute("pade_partsorderid", pade_partsorderid);
		model.addAttribute("paor_status", paor_status);
		model.addAttribute("pade_specialremark", pade_specialremark);
		org.json.JSONObject jsonObject = new org.json.JSONObject(info);
		JSONObject json = JSONObject.fromObject(jsonObject.toString());// userStr是json字符串
		PartsDetails consumer = (PartsDetails) JSONObject.toBean(json, PartsDetails.class);
		model.addAttribute("item", consumer);
		
		return "partorder/partinfo_info";
	}

	/**
	 * 查终端用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/toEdit")
	public String edit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.getCompanyUrl;
		Company item=new Company();
		try {
			Map<String, String> map = new HashMap<String, String>();
			map.put("key", Constants.httpkey);
			String sessionuser = getSessionUser(request);
			map.put("usr", sessionuser);
			String jsondata = HttpTookit.doGet(url, map);
			
			log.info(jsondata);
			JsonConfig config = new JsonConfig();
			Map<String, Object> classMap = new HashMap<String, Object>();
			config.setClassMap(classMap);
			config.setRootClass(Company.class);
			config.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
			JSONObject json = JSONObject.fromObject(jsondata);// userStr是json字符串
			item = (Company) JSONObject.toBean(json, config);

		} catch (Exception e) {
			e.printStackTrace();
			// print(response, errorjson);
		}
		model.addAttribute("item", item);
		return "partorder/partorder_edit";
	}

	/**
	 * 添加端用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/doEdit")
	public void doEdit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.AddPartsOrderUrl;
		Map<String, String> map = new HashMap<String, String>();

		// String paor_name=CoreUtils.trim(request.getParameter("paor_name"));
		String pade_partsorderid = CoreUtils.trim(request.getParameter("pade_partsorderid"));
		String sessionuser = getSessionUser(request);
		String paor_company =sessionuser;
		String paor_tel = CoreUtils.trim(request.getParameter("paor_tel"));
		String paor_deliveringway = CoreUtils.trim(request.getParameter("paor_deliveringway"));
		String paor_express = CoreUtils.trim(request.getParameter("paor_express"));
		String paor_province = CoreUtils.trim(request.getParameter("paor_province"));
		String paor_city = CoreUtils.trim(request.getParameter("paor_city"));
		String paor_county = CoreUtils.trim(request.getParameter("paor_county"));
		String paor_address = CoreUtils.trim(request.getParameter("paor_address"));
		String paor_orderer = CoreUtils.trim(request.getParameter("paor_orderer"));
		String paor_recipientsphone = CoreUtils.trim(request.getParameter("paor_recipientsphone"));
		String paor_recipients = CoreUtils.trim(request.getParameter("paor_recipients"));
		
		String paor_orderdate = CoreUtils.DateToStr(new Date());
		// map.put("paor_name", paor_name);
		map.put("pade_partsorderid", pade_partsorderid);
		map.put("paor_company", paor_company);
		map.put("paor_tel", paor_tel);
		map.put("paor_deliveringway", paor_deliveringway);
		map.put("paor_express", paor_express);
		map.put("paor_province", paor_province);
		map.put("paor_city", paor_city);
		map.put("paor_county", paor_county);
		map.put("paor_address", paor_address);
		map.put("paor_orderer", paor_orderer);
		map.put("paor_recipientsphone", paor_recipientsphone);
		map.put("paor_recipients", paor_recipients);
		map.put("paor_orderdate", paor_orderdate);
		map.put("key", Constants.httpkey);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}
	}

	/**
	 * 提交成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/submit")
	public void submit(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.submitPartOrderUrl;
		Map<String, String> map = new HashMap<String, String>();

		String orderid = CoreUtils.trim(request.getParameter("orderid"));
		map.put("orderid", orderid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 *delete成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete")
	public void delete(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.delPartOrderUrl;
		Map<String, String> map = new HashMap<String, String>();

		String orderid = CoreUtils.trim(request.getParameter("orderid"));
		map.put("orderid", orderid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 *delete成品订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/detailDelete")
	public void detailDelete(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.delPartOrderDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();

		String id = CoreUtils.trim(request.getParameter("id"));
		map.put("id", id);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	/**
	 *修改配件订单数量
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/changeNum")
	public void changeNum(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.partsOrderMDOrderDetailsUrl;
		Map<String, String> map = new HashMap<String, String>();

		String id = CoreUtils.trim(request.getParameter("id"));
		String quantity = CoreUtils.trim(request.getParameter("quantity"));
		String amount = CoreUtils.trim(request.getParameter("amount"));
		map.put("id", id);
		map.put("quantity", quantity);
		map.put("amount", amount);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		}

	}
	
	
	/*
	 * 获取省份数据
	 * */
	@RequestMapping(value = "/province") 
	public  void getProvince(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.province;
		Map<String, String> map = new HashMap<String, String>();
		String jsondata="";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);  
			print(response, jsondata); 
		} catch (Exception e) {
			e.printStackTrace();
			 print(response, errorjson);
		} 
	}
	
	/**
	 *获取城市
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/city")
	public void city(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.city;
		log.info("############城市");
		Map<String, String> map = new HashMap<String, String>();

		String pid = CoreUtils.trim(request.getParameter("id")); 
		log.info("省份代号："+pid);
		map.put("provinceID", pid); 
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsondata="";
		try {
			jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		} 
	}
	
	
	/**
	 *获取区域
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/county")
	public void county(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String url = Constants.baseurl + Constants.county;
		log.info("获取区域数据");
		Map<String, String> map = new HashMap<String, String>();

		String pid = CoreUtils.trim(request.getParameter("id")); 
		map.put("cityID", pid); 
		log.info("区县ID:"+pid);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		String jsondata="";
		try {
			 jsondata = HttpTookit.doGet(url, map);
			log.info(jsondata);
			print(response, jsondata);

		} catch (Exception e) {
			print(response, errorjson);
		} 
	} 
}
