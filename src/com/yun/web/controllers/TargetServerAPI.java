package com.yun.web.controllers;

import com.yun.core.BaseController;
import com.yun.core.common.CoreUtils;
import com.yun.core.utils.HttpTookit;
import com.yun.core.utils.PermissionCheck;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/target/")
public class TargetServerAPI extends BaseController {
	private static final Logger log = Logger.getLogger(TargetServerAPI.class);

	/**
	 * 业务指标公式列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/targetList")
	public String orderlist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("utf-8");
		String flag = CoreUtils.trim(request.getParameter("flag"));
		String sessionuser = getSessionUser(request);
		JSONArray json = null;
		if (flag.equals("")) {
            flag = (String) request.getSession().getAttribute("orderpwd");
            if (StringUtils.isEmpty(flag)){
                model.addAttribute("utype", "10");
                return "user/orderpwdcheck";
            }
        }
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		if (!PermissionCheck.check(sessionuser,requestUrl,request)){
			return "user/checkPermission";
		}
		request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.getTargetList;
		Map<String, String> map = new HashMap<String, String>();
		String compName = CoreUtils.trim(request.getParameter("compName"));
		String compCode = CoreUtils.trim(request.getParameter("compCode"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "99999";
		}

		map.put("comp_code", compCode);
		map.put("comp_name", compName);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);

		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info("rs:" + jsondata);
			BigDecimal receiveAmtTotal = BigDecimal.ZERO.setScale(2, RoundingMode.UP);
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
				json = JSONArray.fromObject(jsondata);// userStr是json字符串
			}

			model.addAttribute("compCode", compCode);
			model.addAttribute("compName", compName);
			model.addAttribute("entityList", json);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("flag", flag);
			model.addAttribute("receiveAmtTotal", receiveAmtTotal);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "target/target_show";
	}
	/**
	 * 业务指标详情
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/targetDetail")
	public String receivelist(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setCharacterEncoding("utf-8");
	    String flag = CoreUtils.trim(request.getParameter("flag"));
		JSONArray json = null;
		if (flag.equals("")) {
			flag = (String) request.getSession().getAttribute("orderpwd");
			if (StringUtils.isEmpty(flag)){
				model.addAttribute("utype", "11");
				return "user/orderpwdcheck";
			}
		}
		request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.getTargetDetail;
		Map<String, String> map = new HashMap<String, String>();
		String compCompanyid = CoreUtils.trim(request.getParameter("Comp_companyid"));
		String compCode = CoreUtils.trim(request.getParameter("compCode"));
		String compName = CoreUtils.trim(request.getParameter("compName"));
		String pageIndex = CoreUtils.trim(request.getParameter("pageIndex"));
		String pageSize = CoreUtils.trim(request.getParameter("pageSize"));
		if (pageIndex.equals("")) {
			pageIndex = "1";
		}
		if (pageSize.equals("")) {
			pageSize = "99999";
		}
		if (StringUtils.isNotEmpty(compName)){
            compName = URLDecoder.decode(compName,"utf-8");
        }

		map.put("targ_companyid", compCompanyid);
		map.put("pageIndex", pageIndex);
		map.put("pageSize", pageSize);
		map.put("key", Constants.httpkey);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);
		try {
			String jsondata = HttpTookit.doPost(url, map);
			log.info("rs:" + jsondata);
			if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
                DecimalFormat df = new DecimalFormat("#.0000");
				json = JSONArray.fromObject(jsondata);// userStr是json字符串
                for (Object o : json) {
                    JSONObject jsonObject = (JSONObject) o;
                    jsonObject.put("targ_targetamt",df.format( Double.parseDouble(jsonObject.get("targ_targetamt").toString())));
					String targ_season = jsonObject.get("targ_season").toString();
					jsonObject.put("targ_season",targ_season.equals("1")?"第一季度":targ_season.equals("2")?"第二季度":targ_season.equals("3")?"第三季度":"第四季度");
                }
            }
            model.addAttribute("compCompanyid", compCompanyid);
            model.addAttribute("compCode", compCode);
            model.addAttribute("compName", compName);
			model.addAttribute("entityList", json);
			model.addAttribute("pageIndex", pageIndex);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("nodatalisttrips", Constants.nodatalisttrips);
		return "target/target_detail";
	}

	/**
	 * 保存业务指标
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveTarget")
	public String saveTarget(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String result = null;
		request.setCharacterEncoding("utf-8");
		String flag = CoreUtils.trim(request.getParameter("flag"));
		JSONArray json = null;
		if (flag.equals("")) {
			flag = (String) request.getSession().getAttribute("orderpwd");
			if (StringUtils.isEmpty(flag)){
				model.addAttribute("utype", "9");
				return "user/orderpwdcheck";
			}
		}
		request.getSession().setAttribute("orderpwd", flag);
		String url = Constants.baseurl + Constants.addTargetDetail;
		Map<String, String> map = new HashMap<String, String>();
		String targ_companyid = CoreUtils.trim(request.getParameter("Comp_companyid"));
		String compCode = CoreUtils.trim(request.getParameter("compCode"));
		String compName = CoreUtils.trim(request.getParameter("compName"));
		String targ_year = CoreUtils.trim(request.getParameter("years"));
		String firstJidu = CoreUtils.trim(request.getParameter("firstJidu"));
		String secondJidu = CoreUtils.trim(request.getParameter("secondJidu"));
		String thirdJidu = CoreUtils.trim(request.getParameter("thirdJidu"));
		String fourJidu = CoreUtils.trim(request.getParameter("fourJidu"));
		//String targ_targetamt = CoreUtils.trim(request.getParameter("mbNum"));
		if (StringUtils.isNotEmpty(compName)){
			compName = URLDecoder.decode(compName,"utf-8");
		}
		map.put("targ_companyid", targ_companyid);
		map.put("key", Constants.httpkey);
		map.put("targ_year", targ_year);
		String sessionuser = getSessionUser(request);
		map.put("usr", sessionuser);

		try {
			for (int i = 1; i < 5; i++) {
				map.put("targ_season", String.valueOf(i));
				map.put("targ_targetamt", i==1?firstJidu:i==2?secondJidu:i==3?thirdJidu:fourJidu);
				String jsondata = HttpTookit.doPost(url, map);
				log.info("rs:" + jsondata);
				if (jsondata.startsWith("[") && jsondata.endsWith("]")) {
					json = JSONArray.fromObject(jsondata);// userStr是json字符串
				}else  if (jsondata!=""){
					JSONObject jsonObject = JSONObject.fromObject(jsondata);
					result = jsonObject.toString();
				}
			}
			model.addAttribute("compCode", compCode);
			model.addAttribute("Comp_companyid", targ_companyid);
			model.addAttribute("compName", URLEncoder.encode(compName,"utf-8"));
			model.addAttribute("entityList", json);
			model.addAttribute("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/target/targetDetail";
	}
}
