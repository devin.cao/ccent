package com.yun.wx;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
 
import net.sf.json.JSONObject;
/***
 * @author V型知识库  www.vxzsk.com
 *
 */
public class JsapiTicketUtil {
     
    /***
     * 模拟get请求
     * @param url
     * @param charset
     * @param timeout
     * @return
     */
     public static String sendGet(String url, String charset, int timeout)
      {
        String result = "";
        try
        {
          URL u = new URL(url);
          try
          {
            URLConnection conn = u.openConnection();
            conn.connect();
            conn.setConnectTimeout(timeout);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
            String line="";
            while ((line = in.readLine()) != null)
            {
             
              result = result + line;
            }
            in.close();
          } catch (IOException e) {
            return result;
          }
        }
        catch (MalformedURLException e)
        {
          return result;
        }
       
        return result;
      }
     /***
      * 获取acess_token 
      * 来源www.vxzsk.com
      * @return
      */
     public static String getAccessToken(){
            String appid=WXConstants.corpId;//应用ID
            String appSecret=WXConstants.secret;//(应用密钥)
            String url ="https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid="+appid+"&corpsecret="+appSecret;
         System.out.println("运行到了这里"+appid );
         System.out.println("运行到了这里"+appSecret );
         System.out.println("运行到了这里"+url );
            String backData=JsapiTicketUtil.sendGet(url, "utf-8", 10000);
            System.out.println("运行到了这里"+backData);
            String accessToken = (String) JSONObject.fromObject(backData).get("access_token");  
            return accessToken;
     }
     
     /***
      * 获取jsapiTicket
      * 来源 www.vxzsk.com
      * @return
      */
    public static String getJSApiTicket(){ 
        //获取token
        String acess_token= JsapiTicketUtil.getAccessToken();
        System.out.println(acess_token);
        String urlStr = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token="+acess_token;
        String backData=JsapiTicketUtil.sendGet(urlStr, "utf-8", 10000);  
        String ticket = (String) JSONObject.fromObject(backData).get("ticket");  
        return  ticket;  
           
    }

    /***
     * 获取jsapiTicket(订阅号)
     * 来源 www.vxzsk.com
     * @return
     */
    public static String getJSApiTicketForSubscriptions(){
        //获取token
        String acess_token= JsapiTicketUtil.getAccessTokenForSubscriptions();
        System.out.println(acess_token);
        String urlStr = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token="+acess_token;
        String backData=JsapiTicketUtil.sendGet(urlStr, "utf-8", 10000);
        String ticket = (String) JSONObject.fromObject(backData).get("ticket");
        return  ticket;

    }

    /***
     * 获取acess_token
     * 来源www.vxzsk.com
     * @return
     */
    public static String getAccessTokenForSubscriptions(){
        String appid=WXConstants.APPID;//应用ID
        String appSecret=WXConstants.APPSECRET;//(应用密钥)
        String url ="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appid+"&secret="+appSecret;
        String backData=JsapiTicketUtil.sendGet(url, "utf-8", 10000);
        System.out.println(backData);
        String accessToken = (String) JSONObject.fromObject(backData).get("access_token");
        return accessToken;
    }

    public static void main(String[] args) {
        String jsapiTicket = JsapiTicketUtil.getJSApiTicket();
        System.out.println("调用微信jsapi的凭证票为："+jsapiTicket);
 
    }
 
}