package com.yun.core.utils;

/**
 * Created by cmt on 2018/5/24.
 */
public enum  PermissionEnum {

    PARTORDER(1, "/partorder/list"),
    BOMORDER(2, "/bomOrder/list"),
    GOODSORDER_LIST(3,"/goodsorder/list"),
    TRADITIONALORDER(4, "/traditional/list"),
    CONSUMER(6, "/consumer/list"),
    WO(7, "/wo/list"),
    TARGET(8, "/target/targetList"),
    PRODUCT_SCAN(11, "/product/scan"),
    PRODUCT_PRICE(12, "/product/price"),
    GOODSORDER_GETCREDIT(13, "/goodsorder/getCredit"),
    REPORTS_ORDERLIST(14, "/reports/orderlist"),
    REPORTS_RECEIVELIST(15, "/reports/receivelist");

    private Integer id;
    private String desc;

    private PermissionEnum(Integer id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public Integer getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public static PermissionEnum[] getArray() {
        return PermissionEnum.values();
    }

    public static String getDesc(Integer id){
        if(id != null){
            PermissionEnum[] values = PermissionEnum.values();
            for(PermissionEnum logNote : values){
                if(logNote.getId().equals(id)){
                    return logNote.getDesc();
                }
            }
        }
        return "";
    }
}
