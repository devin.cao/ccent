package com.yun.core.utils;

import com.yun.core.common.CoreUtils;
import com.yun.web.controllers.Constants;
import net.sf.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cmt on 2018/5/22.
 */
public class PermissionCheck  {
    public static Boolean check(String userName,String requestUrl,HttpServletRequest request){
        String menu= null;
        PermissionEnum[] array = PermissionEnum.getArray();
        for (PermissionEnum permissionEnum : array) {
            if (requestUrl.equals(permissionEnum.getDesc())){
                menu = String.valueOf(permissionEnum.getId());
            }
        }
        if(menu!=null){
            String url = Constants.baseurl+"/api/Company/CheckPermission";
            HashMap map1 = new HashMap();
            map1.put("menu", menu);
            map1.put("Usr", userName);
            String e = HttpTookit.doGet(url, map1);
            if(e.indexOf("\"StatusCode\":1")>0){
                return true;
            }else {
                JSONObject json = JSONObject.fromObject(e);
                request.getSession().setAttribute("Message",json.get("Message"));
                return false;
            }
        }else {
            return true;
        }
    }
}
