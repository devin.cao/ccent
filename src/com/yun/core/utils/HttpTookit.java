package com.yun.core.utils;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.yun.web.controllers.Constants;
import com.yun.web.controllers.GoodsOrderServerAPI;
import org.json.JSONObject;

/**
 * 基于 httpclient 4.3.1版本的 http工具类
 * @author mcSui
 *
 */
public class HttpTookit {
	private static final Logger log = Logger.getLogger(HttpTookit.class);
    private static final CloseableHttpClient httpClient;
    public static final String CHARSET = "UTF-8";

    static {
        RequestConfig config = RequestConfig.custom().setConnectTimeout(10000).setSocketTimeout(100000).build();
        httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
    }

    public static String doGet(String url, Map<String, String> params){
        return doGet(url, params,CHARSET);
    }
    public static String doPost(String url, Map<String, String> params){
        return doPost(url, params,CHARSET);
    }
    /**
     * HTTP Get 获取内容
     * @param url  请求的url地址 ?之前的地址
     * @param params	请求的参数
     * @param charset	编码格式
     * @return	页面内容
     */
    public static String doGet(String url,Map<String,String> params,String charset){
    	if(StringUtils.isBlank(url)){
    		return null;
    	}
    	String res="";
    	CloseableHttpResponse response=null;
    	try {
    		if(params != null && !params.isEmpty()){
    			List<NameValuePair> pairs = new ArrayList<NameValuePair>(params.size());
    			for(Map.Entry<String,String> entry : params.entrySet()){
    				String value = entry.getValue();
    				if(value != null){
    					pairs.add(new BasicNameValuePair(entry.getKey(),value));
    				}
    			}
    			url += "?" + EntityUtils.toString(new UrlEncodedFormEntity(pairs, charset));
    		}
    		HttpGet httpGet = new HttpGet(url);
    		response = httpClient.execute(httpGet);
    		int statusCode = response.getStatusLine().getStatusCode();
    		if (statusCode != 200) {
//    			httpGet.abort();
//    			throw new RuntimeException("HttpClient,error status code :" + statusCode);
    		}else{
    			HttpEntity entity = response.getEntity();
        		String result = null;
        		if (entity != null){
        			result = EntityUtils.toString(entity, "utf-8");
        		}
        		EntityUtils.consume(entity);
        		res= result;
    		}
    	log.info("rs:"+res);
    		if(res==null||res.equals("")){
    			res= Constants.httpservice_error;
    		}
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		log.error("httperror", e);
    		if(e instanceof ConnectException){
    			res= Constants.httptimeout_error;
    		}else{
    			res= Constants.httpservice_error;
    		}
    	}finally{
    		if(response!=null){
    			try {
					response.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
    	return res;
    }
    
    /**
     * HTTP Post 获取内容
     * @param url  请求的url地址 ?之前的地址
     * @param params	请求的参数
     * @param charset	编码格式
     * @return	页面内容
     */
    public static String doPost(String url,Map<String,String> params,String charset){
    	if(StringUtils.isBlank(url)){
    		return null;
    	}
    	CloseableHttpResponse response=null;
    	String res="";
    	try {
    		List<NameValuePair> pairs = null;
    		if(params != null && !params.isEmpty()){
    			pairs = new ArrayList<NameValuePair>(params.size());
    			for(Map.Entry<String,String> entry : params.entrySet()){
    				String value = entry.getValue();
    				if(value != null){
    					pairs.add(new BasicNameValuePair(entry.getKey(),value));
    				}
    			}
    		}
    		HttpPost httpPost = new HttpPost(url);
    		if(pairs != null && pairs.size() > 0){
    			httpPost.setEntity(new UrlEncodedFormEntity(pairs,CHARSET));

    		}
    		response = httpClient.execute(httpPost);
    		int statusCode = response.getStatusLine().getStatusCode();
    		if (statusCode != 200) {
    			httpPost.abort();
    			throw new RuntimeException("HttpClient,error status code :" + statusCode);
    		}
    		HttpEntity entity = response.getEntity();
    		String result = null;
    		if (entity != null){
    			result = EntityUtils.toString(entity, "utf-8");
    		}
    		EntityUtils.consume(entity);
    		res= result;
    	} catch (Exception e) {
    		//e.printStackTrace();
    		log.error("httperror", e);
    		if(e instanceof ConnectException){
    			res= Constants.httptimeout_error;
    		}else{
    			res= Constants.httpservice_error;
    		}
    	}finally{
    		if(response!=null){
    			try {
					response.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
    	return res;
    }

	/**
	 * 请求参数为json
	 * @param url
	 * @param json
	 * @return
	 * @throws Exception
     */
	public static String doPostJson(String url, String json) throws Exception {
		String result = null;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		StringEntity stringEntity = new StringEntity(json,"UTF-8");
		//拼接参数
		httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
		httpPost.setEntity(stringEntity);
		CloseableHttpResponse response  = null;
		try {
			response = httpClient.execute(httpPost);
			System.out.println("========HttpResponseProxy：========"+response.getStatusLine());
			HttpEntity entity = response.getEntity();
			if(entity != null){
				result = EntityUtils.toString(entity, "UTF-8");
				System.out.println("========接口返回=======" +result);
			}
			EntityUtils.consume(entity);
			//httpClient.getConnectionManager().shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(response != null){
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(httpClient != null){
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public static void main(String []args){
    	String url1=Constants.baseurl+Constants.getAgentAccountUrl;
    	Map map=new HashMap();
    	map.put("agac_name", "123");
    	map.put("agac_password", "123");
    	map.put("method", "login");
    	String getData = doGet(url1,map);
    	System.out.println(getData);
    }
    
}