package com.yun.core;

public class PageUtils {

	private Integer page;
	private Integer pagesize;
	private Integer total;
	private Integer totalpage;
	//显示上一页
	private Integer showLastPage;
	//显示下一页
	private Integer showNextPage;
	//
	
	
	
	public PageUtils() {
		super();
	}
	
	public PageUtils(Integer page,Integer pagesize, Integer total) {
		super();
		this.page = page;
		this.pagesize = pagesize;
		this.total = total;
		showLastPage=0;
		showNextPage=0;
		if(total%pagesize==0){
			totalpage=total/pagesize;
		}else{
			totalpage=total/pagesize+1;
		}
		if(totalpage<=1){
			showLastPage=0;
			showNextPage=0;
		}else {
			//总页数大于1
			if(page==1){
				showLastPage=0;
				showNextPage=1;
			}else if(page<totalpage){
				showLastPage=1;
				showNextPage=1;
			}else{
				showLastPage=1;
				showNextPage=0;
			}
		}
		if(page>totalpage){
			page=totalpage;
		}
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getShowNextPage() {
		return showNextPage;
	}
	public void setShowNextPage(Integer showNextPage) {
		this.showNextPage = showNextPage;
	}
	public Integer getShowLastPage() {
		return showLastPage;
	}
	public void setShowLastPage(Integer showLastPage) {
		this.showLastPage = showLastPage;
	}
	public Integer getPagesize() {
		return pagesize;
	}
	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}

	public Integer getTotalpage() {
		return totalpage;
	}

	public void setTotalpage(Integer totalpage) {
		this.totalpage = totalpage;
	}
	
}
