package com.yun.core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseController {
	public static String errorjson="{\"StatusCode\":0,\"Message\":\"接口异常\"}"; 
   
	/**
	 * session user
	 * @param request
	 * @return
	 */
	public String getSessionUser(HttpServletRequest request){
		String sessionuser=(String) request.getSession().getAttribute("sessionuser");
		if(sessionuser==null){
			return "";
		}
		return sessionuser;
	}
	public void print(HttpServletResponse response,String jsonstr){
		 try {
			 //JSONObject obj=JSONObject.fromObject(jsonstr);
		        //设置页面不缓存
		        response.setContentType("application/json");
		        response.setHeader("Pragma", "No-cache");
		        response.setHeader("Cache-Control", "no-cache");
		        response.setCharacterEncoding("UTF-8");
		        PrintWriter out= null;
		        out = response.getWriter();
		        out.print(jsonstr);
		        out.flush();
		        out.close();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		     
	}
	public boolean checklogin(HttpServletRequest request){
		String sessionuser=(String) request.getSession().getAttribute("sessionuser");
		if(sessionuser==null||sessionuser.equals("")){
			return false;
		}else{
			return true;
		}
	}
	
}
