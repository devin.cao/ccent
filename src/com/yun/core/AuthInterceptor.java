package com.yun.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.yun.core.common.CoreUtils;

public class AuthInterceptor implements HandlerInterceptor {
	private static final Logger log = Logger.getLogger(AuthInterceptor.class);
	private static String tologinURL = "/login.jsp";
	private static String errorURL = "/error.jsp";
	private static String loginURL = "/admin/home/login";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception{
		String requestUrl = CoreUtils.trim(request.getRequestURI().replace(request.getContextPath(), ""));
		checkRequestHost(request);
		if (requestUrl.indexOf("/wx/") >= 0 || requestUrl.contains("/premission/") || requestUrl.contains("/user/")||requestUrl.contains("/product/list")) {
			return true;
		} else {						
			String xreqestw = request.getHeader("x-requested-with");
			String contextPath = request.getContextPath();
			String url = request.getServletPath().toString();
			HttpSession session = request.getSession();
			String sessionUser = (String) session.getAttribute("sessionuser");
			if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
				// 请求的路径
				log.info("sessionUser："+sessionUser);
				if (sessionUser == null && !url.contains(tologinURL) && !url.contains(loginURL)) {
					log.info("Interceptor：跳转到login页面！");
					if (xreqestw == null) {
						response.sendRedirect(contextPath + tologinURL);
						return true;
					} else {
						String json="{\"StatusCode\":0,\"Message\":\"接口异常\",\"backurl\":\""+requestUrl+"\"}";
						showJSON(response,json);
					}
					return false;
				}else {
					return true;
				}
			} else{
				if (sessionUser == null && !url.contains(tologinURL) && !url.contains(loginURL)) {
					if(requestUrl.endsWith("/get")){
						Map<String,Object> map=request.getParameterMap();
						StringBuffer bf=new StringBuffer();
						for(String dataKey : map.keySet())   
						{   
						    System.out.println(dataKey ); 
						    String [] param = (String[])map.get(dataKey);  
						    String value=param[0];
						    bf.append("&"+dataKey+"="+CoreUtils.trim(value));
						}
						String str=bf.toString();
						if(bf.length()>0){
							requestUrl=requestUrl+"?"+str.substring(1);
						}
					}
					request.setAttribute("backurl", requestUrl);
					response.sendRedirect(contextPath+"/user/login?burl="+requestUrl);
					return false;
				}
				else{
					return true;
				}
			}
		}

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	public void showJSON(HttpServletResponse response, String jsonStr) {
		PrintWriter out = null;
		try {
			response.setContentType("application/json; charset=UTF-8");
			out = response.getWriter();
			out.print(jsonStr);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				// 释放资源
				out.flush();
				out.close();
			}
		}
	}

	public String toURL(HttpServletRequest request, String url) {
		return request.getContextPath() + url;
	}

	public void responseJson(String jsonstr, HttpServletResponse response) throws IOException {
		response.setContentType("application/json;charset=UTF-8");
		this.responPw(jsonstr, response);
	}

	private void responPw(String jsonstr, HttpServletResponse response) throws IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter pw = response.getWriter();
		pw.write(jsonstr);
		pw.flush();
	}

	private void checkRequestHost(HttpServletRequest request) {
		String userAgent = request.getHeader("User-Agent");
		String ip=getIpAddr(request);
//		log.info("ip=" +ip+",useragent="+userAgent);
	}
    public String getIpAddr(HttpServletRequest request) { 
        String ip = request.getHeader("x-forwarded-for"); 
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("Proxy-Client-IP"); 
        } 
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getHeader("WL-Proxy-Client-IP"); 
        } 
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
            ip = request.getRemoteAddr(); 
        } 
        return ip; 
    } 
  
}