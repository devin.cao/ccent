<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html class="ui-page-login">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>报修工单详细信息</title>
<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
<style>
.area {
	margin: 20px auto 0px auto;
}

.mui-input-group {
	margin-top: 10px;
}

.mui-input-group:first-child {
	margin-top: 20px;
}

.mui-input-group label {
	width: 27%;
}

#vcode {
	width: 50%;
	float: left;
}

#code {
	width: 23%;
	float: right;
}

.mui-input-row label ~input, .mui-input-row label ~select,
	.mui-input-row label ~textarea {
	width: 73%;
}

.mui-checkbox input[type=checkbox], .mui-radio input[type=radio] {
	top: 6px;
}

.mui-content-padded {
	margin-top: 25px;
}

.mui-btn {
	padding: 10px;
}

.link-area {
	display: block;
	margin-top: 25px;
	text-align: center;
}

.spliter {
	color: #bbb;
	padding: 0px 8px;
}

.oauth-area {
	position: absolute;
	bottom: 20px;
	left: 0px;
	text-align: center;
	width: 100%;
	padding: 0px;
	margin: 0px;
}

.oauth-area .oauth-btn {
	display: inline-block;
	width: 50px;
	height: 50px;
	background-size: 30px 30px;
	background-position: center center;
	background-repeat: no-repeat;
	margin: 0px 20px;
	/*-webkit-filter: grayscale(100%); */
	border: solid 1px #ddd;
	border-radius: 25px;
}

.oauth-area .oauth-btn:active {
	border: solid 1px #aaa;
}

.oauth-area .oauth-btn.disabled {
	background-color: #ddd;
}
</style>

</head>

<body>
	<header class="mui-bar mui-bar-nav">
		
		<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-left">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		<h1 class="mui-title"></h1>
		<a class="mui-btn mui-btn-blue mui-btn-link mui-pull-right" id="editButton">修改</a>
	</header>
	<div class="mui-content">
		<ul class="mui-table-view">
			<li class="mui-table-view-cell">
			<form method="post" id="formcell" action="<c:url value='/wo/get?edit=1' />">
			<input id="info" type="hidden" name="info" value="{'woor_WorkOrderID':'${ item.woor_WorkOrderID}','woor_company':'${ item.woor_company}','woor_companyid':'${ item.woor_companyid}','woor_consumer':'${ item.woor_consumer}','woor_consumerid':'${ item.woor_consumerid}','woor_description':'${ item.woor_description}','woor_handler':'${ item.woor_handler}','woor_name':'${ item.woor_name}','woor_product':'${ item.woor_product}','woor_productid':'${ item.woor_productid}','woor_remark':'${ item.woor_remark}','woor_solution':'${ item.woor_solution}','woor_status':'${ item.woor_status}','woor_title':'${ item.woor_title}','woor_type':'${ item.woor_type}'}">
			<input id="woor_WorkOrderID" type="hidden" name="woor_WorkOrderID" value="${item.woor_WorkOrderID}">
			</form>
					<label>工单编号:</label>&nbsp;&nbsp;&nbsp;<label>${ item.woor_name }</label>
				</li>
				
				<li class="mui-table-view-cell">
					<label>工单类型:</label>&nbsp;&nbsp;&nbsp;<label>
					<c:if test="${ item.woor_type eq 1}">故障</c:if>
					<c:if test="${ item.woor_type eq 2}">咨询</c:if>
					</label>
				</li>
				<li class="mui-table-view-cell">
					<label>问题描述:</label>&nbsp;&nbsp;&nbsp;<label>${ item.woor_description }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>解决方案:</label>&nbsp;&nbsp;&nbsp;<label>${ item.woor_solution }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>产品型号:</label>&nbsp;&nbsp;<label>${ item.woor_product }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>来电客户:</label>&nbsp;&nbsp;&nbsp;<label>${ item.woor_consumer }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>代理商:</label>&nbsp;&nbsp;&nbsp;<label>${ item.woor_company }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>状态:</label>&nbsp;&nbsp;&nbsp;<label>
					        <c:if test="${ item.woor_status eq 1}">新建</c:if>
							<c:if test="${ item.woor_status eq 2}">提交</c:if>
							<c:if test="${ item.woor_status eq 3}">处理中</c:if>
							<c:if test="${ item.woor_status eq 4}">处理完成</c:if>
					</label>
				</li>
				<li class="mui-table-view-cell">
					<label>处理人:</label>&nbsp;&nbsp;&nbsp;<label>${ item.woor_handler }</label>
				</li>
				
		</ul>
	</div>
	<script src="<c:url value='/js/mui.min.js' />"></script>
	<script src="<c:url value='/js/mui.enterfocus.js' />"></script>
	<script src="<c:url value='/js/app.js' />"></script>
	<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/wo/list'/>";
				}
				var editButton = doc.getElementById('editButton');
				editButton.addEventListener('tap', function(event) {
					var formid="formcell";
					//alert(formid);
					doc.getElementById(formid).submit()
				}, false);
				
			}(mui, document));
		</script>
</body>

</html>