<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>修改报修工单信息</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			.mui-input-group label {
				width: 34%;
			}
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 66%;
			}
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title"></h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		 <section id="preview"></section>
			<form class="mui-input-group" id="consumerform" name="getpwdform" method="post" action="<c:url value='/wo/doEdit'/>">
				<div class="mui-input-row">
				<input id='woor_WorkOrderID' name="woor_WorkOrderID" type="hidden" class="mui-input-clear mui-input" value="${item.woor_WorkOrderID}">
					<label>工单编号:</label>
					<label>
					${ item.woor_name}
					</label>
				</div>             	
               
				<div class="mui-input-row">
					<label>工单类型:</label>
					<label>
					<c:if test="${ item.woor_type eq 1}">故障</c:if>
					<c:if test="${ item.woor_type eq 2}">咨询</c:if>
					</label>
				</div>
				<div class="mui-input-row">
					<label>问题描述 :</label>
					<label>
					${ item.woor_description}
					</label>
				</div>
				<div class="mui-input-row">
					<label>解决方案:</label>
					<input id='woor_solution' name="woor_solution" value="${item.woor_solution}"  type="text" class="mui-input-clear mui-input" placeholder="请输入解决方案">
				</div>
				<div class="mui-input-row">
					<label>产品型号:</label>
					
					<input id='woor_productid' name="woor_productid" readonly="readonly"  value="${item.woor_product}" type="text" class="mui-input" placeholder="请输入产品型号">
				</div>
				<div class="mui-input-row">
					<label>来电客户 :</label>
					<label>${ item.woor_consumer}</label>
				</div>
				<div class="mui-input-row">
					<label>代理商:</label> <label>${item.woor_company}</label>
					
				</div>
				<div class="mui-input-row">
					<label>状态 :</label>
					<select name="woor_status" id="woor_status">
					 <option value="3">处理中</option>
					  <option value="4">处理完成</option>
					</select>
				</div>
				<div class="mui-input-row">
					<label>处理人:</label>
					<label>${ item.woor_handler}</label>
				</div>
				<!--  
				<div class="mui-input-row">
					<label>备注:</label>
					<input id='woor_remark' name="woor_remark"  value="${item.woor_remark}" type="text" class="mui-input-clear mui-input" placeholder="请输入备注">
				</div>
				-->
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">提交</button>
			</div>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
	  
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/wo/list'/>";
				}
			var saveButton = doc.getElementById('saveButton');
			saveButton.addEventListener('tap', function(event) {
				
				var woor_WorkOrderID=doc.getElementById('woor_WorkOrderID').value;
				var woor_solution=doc.getElementById('woor_solution').value;
				var woor_status=doc.getElementById('woor_status').value;
				//var woor_remark=doc.getElementById('woor_remark').value;
				//doc.getElementById("consumerform").submit();
				 if(woor_solution==''){
	                	mui.alert("解决方案不能为空");
						return ;
				  }
				
				var spinner= new Spinner();
				mui.ajax("<c:url value='/wo/doEdit'/>",{
					data:{
						woor_WorkOrderID:woor_WorkOrderID,
						woor_solution:woor_solution,
						woor_status:woor_status
					},
					dataType:'json',//服务器返回json格式数据
					type:'post',//HTTP请求类型
					headers:null,
					beforeSend: function() {
						spinner.spin(document.getElementById('preview'));
						saveButton.disabled=true;
					},
					complete: function() {
						spinner.spin();
						saveButton.disabled=false;
					},
					success:function(data){
						if(data.StatusCode==1){
							//成功
							//
							alert(data.Message);
							//mui.back();
							window.location.href="<c:url value='/wo/list'/>";
						}else{
							alert(data.Message);
						}
						
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						console.log(type);
					}
				});
			});		
			}(mui, document));
		</script>
	</body>

</html>