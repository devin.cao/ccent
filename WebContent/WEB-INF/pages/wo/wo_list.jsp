<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>报修工单信息</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/mui.picker.min.css' />" />
	<style type="text/css">
	a {
	color: black;
	}
	</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<!--  <a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>-->
			<h1 class="mui-title">报修工单信息</h1>
		</header>

		<div class="mui-content">
		 <div id="searchdv"  <c:if test="${q ne 1}">style="display:none;"</c:if>   >
			<form class="mui-input-group" id="queryform" method="get" action="<c:url value='/wo/list' />" >
				<input id='q' name="q" value="1" type="hidden" class="mui-input-clear mui-input" >
				<div class="mui-input-row">
					<label class="itemlabel">&nbsp;&nbsp;&nbsp;工单号:</label>
					<input  class="iteminput" id='woor_name' name="woor_name" value="${woor_name }" type="text" class="mui-input-clear mui-input" placeholder="请输入工单号">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">开始日期:</label>
					<input class="iteminput" id='BeginDate' name="BeginDate" value="${BeginDate }" data-options='{"type":"date"}' readonly type="text" class="mui-input-clear mui-input" placeholder="请输入开始日期">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">结束日期:</label>
					<input class="iteminput"  id='EndDate' name="EndDate" value="${EndDate }" data-options='{"type":"date"}' readonly type="text" class="mui-input-clear mui-input" placeholder="请输入结束日期">
				</div>
				
			</form>
			</div>
			<div class="mui-content-padded" style="text-align: center;">
				<button type="button" id="queryButton" class="mui-btn mui-btn-primary">查询</button>
			</div>
			<ul class="mui-table-view">
			 <c:forEach items="${entityList}" var="item" varStatus="s">
				<li class="mui-table-view-cell" id="cell${s.index+1}">
					<a href="javascript:;" class="mui-navigate-right">

						<div class="mui-media-body">
						    <form method="post" id="formcell${s.index+1}" action="<c:url value='/wo/get' />">
						   <input type="hidden" name="info" id="entitycell${s.index+1}" value="{'woor_WorkOrderID':'${ item.woor_WorkOrderID}','woor_company':'${ item.woor_company}','woor_companyid':'${ item.woor_companyid}','woor_consumer':'${ item.woor_consumer}','woor_consumerid':'${ item.woor_consumerid}','woor_description':'${ item.woor_description}','woor_handler':'${ item.woor_handler}','woor_name':'${ item.woor_name}','woor_product':'${ item.woor_product}','woor_productid':'${ item.woor_productid}','woor_remark':'${ item.woor_remark}','woor_solution':'${ item.woor_solution}','woor_status':'${ item.woor_status}','woor_title':'${ item.woor_title}','woor_type':'${ item.woor_type}'}">
							</form>
							工单号：${ item.woor_name}<br /> 工单类型：<c:if test="${ item.woor_type eq 1}">故障</c:if>
							<c:if test="${ item.woor_type eq 2}">咨询</c:if>
							<br /> 订单日期：${ item.woor_createddate}<br /> 订单状态：
							<c:if test="${ item.woor_status eq 1}">新建</c:if>
							<c:if test="${ item.woor_status eq 2}">提交</c:if>
							<c:if test="${ item.woor_status eq 3}">处理中</c:if>
							<c:if test="${ item.woor_status eq 4}">处理完成</c:if>
							<br />
						</div>
					</a>
				</li>
				</c:forEach>
				
			</ul>
			<c:if test="${empty entityList}">
			<div class="mui-collapse-content">
			    <p style="height: 2%"></p>
			    <p class="nodataclass" >${nodatalisttrips}</p>
			</div>
			</c:if>
			<c:if test="${!empty entityList}">
			<jsp:include page="../share/page.jsp">
			<jsp:param name="url" value="/wo/list?woor_name=${woor_name }&BeginDate=${BeginDate }&EndDate=${EndDate}" />
			</jsp:include>
			</c:if>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/mui.picker.min.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var BeginDate = doc.getElementById('BeginDate');
				BeginDate.addEventListener('focus', function(event) {
					doc.activeElement.blur();
					var optionsJson = this.getAttribute('data-options') || '{}';
					var options = JSON.parse(optionsJson);
					var dtPicker = new $.DtPicker(options);
					dtPicker.show(function(selectItems) {
						BeginDate.value = selectItems.text;
						dtPicker.dispose();
					})
				}, false);
				var EndDate = doc.getElementById('EndDate');
				EndDate.addEventListener('focus', function(event) {
					doc.activeElement.blur();
					var optionsJson = this.getAttribute('data-options') || '{}';
					var options = JSON.parse(optionsJson);
					var dtPicker = new $.DtPicker(options);
					dtPicker.show(function(selectItems) {
						EndDate.value = selectItems.text;
						dtPicker.dispose();
					})
				}, false);
				var queryButton = doc.getElementById('queryButton');
				queryButton.addEventListener('tap', function(event) {
					var searchdv=doc.getElementById("searchdv").style.display;
					if(searchdv=='none'){
						doc.getElementById("searchdv").style.display="block";
					}else{
						var startdate=BeginDate.value;
						var enddate=EndDate.value;
						if(startdate!=''&&enddate!=''){
							if(!DateDiff(startdate,enddate)){
								mui.alert("开始日期不能大于结束日期");
								return ;
							}
						}
						doc.getElementById("queryform").submit();
					}
				}, false);
				mui(".mui-table-view").on('tap', '.mui-table-view-cell', function() {
					var id = this.getAttribute("id");
					var formid="form"+id;
					//alert(formid);
					doc.getElementById(formid).submit()
				})
				
			}(mui, document));
			function DateDiff(d1,d2){    
			    start_at = new Date(d1.replace(/^(\d{4})(\d{2})(\d{2})$/,"$1/$2/$3"));
			    end_at = new Date(d2.replace(/^(\d{4})(\d{2})(\d{2})$/,"$1/$2/$3"));
			    if(start_at > end_at) {
			      return false;
			    }
			    return true;
			}
			
		</script>
	</body>

</html>