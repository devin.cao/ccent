<%--
  Created by IntelliJ IDEA.
  User: cmt
  Date: 2018/3/26
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>业务指标详情</title>
    <style>
        .table-container::-webkit-scrollbar
        {
            -webkit-appearance: none;
            width: 14px;
            height: 14px;
        }

        .table-container::-webkit-scrollbar-thumb
        {
            border-radius: 8px;
            border: 3px solid #fff;
            background-color: rgba(0, 0, 0, .3);
        }
        .jsgrid-cell {
        //font-size: 2em;
            text-align: center;
        }
        .jsgrid-header-cell{
        //font-size: 2em;
        }
        .form-control {
            display: inline-block;
            width: 30%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
        .jsgrid-cell{
            word-break: break-all;
        }
        #pager::-webkit-input-placeholder{
            font-size: 1px;
        }
        #years{
            border: 1px solid !important;
        }
        #jidu{
            border: 1px solid !important;
        }
        #mbNum{
            border: 1px solid !important;
        }
        input.error, select.error {
            border: 1px solid #ff9999;
            background: #ffeeee;
        }

        label.error {
            float: right;
            margin-left: 100px;
            font-size: .8em;
            color: #ff6666;
        }
    </style>
    <link href=<c:url value="/css/jsgrid.min.css"/> rel="stylesheet">
    <link href=<c:url value="/css/jsgrid-theme.min.css" /> rel="stylesheet">
    <link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
    <link href="<c:url value='/css/jquery-ui.min.css' />" rel="stylesheet" />
</head>
<body>
<header class="mui-bar mui-bar-nav">
    <!--  <a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>-->
    <h1 class="mui-title">业务指标详情</h1>
</header>
<div class="mui-input-row" >
    <br><br>
</div>
<form class="mui-input-group" id="queryform" method="post"  >
    <div class="mui-input-row" >
        <label class="itemlabel">客户代码</label>   <input class="form-iteminput" type="text" readonly id="compCode" name="compCode" value="${compCode}">
    </div>

    <div class="mui-input-row" >
        <label class="itemlabel">客户名称</label>   <input class="form-iteminput" type="text" readonly id="compName" name="compName" value="${compName}">
    </div>
</form>
<div class=""  style="text-align: center;">
    <button type="button" id="addButton" class="mui-btn mui-btn-primary">新增</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<div id="jsGrid" class="table-container"></div>
<div id="detailsDialog" style="display: none">
    <form id="detailsForm">
        <div class="details-form-field">
            <label for="years">年份:</label>
            <select id="years" name="years">
                <option value="">(选择年份)</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
            </select>
        </div>
        <%--  <div class="">
              <label for="jidu">季度:</label>
              <select id="jidu" name="jidu">
                  <option value="">(选择季度)</option>
                  <option value="1">第一季度</option>
                  <option value="2">第二季度</option>
                  <option value="3">第三季度</option>
                  <option value="4">第四季度</option>
              </select>
          </div>--%>
        <div class="details-form-field">
            <label for="firstJidu">第一季度:</label>
            <input id="firstJidu" name="firstJidu" type="number"  step="0.0001" min="0" placeholder="单位万元"/>
        </div>
        <div class="details-form-field">
            <label for="secondJidu">第二季度:</label>
            <input id="secondJidu" name="secondJidu" type="number"step="0.0001" min="0"placeholder="单位万元"/>
        </div>
        <div class="details-form-field">
            <label for="thirdJidu">第三季度:</label>
            <input id="thirdJidu" name="thirdJidu" type="number"step="0.0001"min="0" placeholder="单位万元"/>
        </div>
        <div class="details-form-field">
            <label for="fourJidu">第四季度:</label>
            <input id="fourJidu" name="fourJidu" type="number" step="0.0001"min="0" placeholder="单位万元"/>
        </div>
        <%--
                    <div class="details-form-field">
                        <label for="mbNum">销售指标:</label>
                        <input id="mbNum" name="mbNum" type="number" placeholder="单位万元"/>
                    </div>--%>
        <div class="details-form-field">
            <button type="submit" id="save">保存</button>
        </div>
    </form>
</div>
<script src="<c:url value="/js/jquery-1.8.3.js"/>"></script>
<script src="<c:url value="/js/jsgrid.min.js"/>"></script>
<script src="<c:url value='/js/mui.min.js' />"></script>
<script src="<c:url value='/js/jquery-ui.min.js'/>"></script>
<script src="<c:url value='/js/jquery.validate.min.js'/>"></script>

<script>
    $(function() {
        /* mui.init({
         statusBarBackground: '#000000'
         });*/
        var pageIndex ="";
        var pageSize ="";
        var compCode = document.getElementById('compCode');
        var compName = document.getElementById('compName');
        var addButton = document.getElementById('addButton');
        addButton.addEventListener('tap', function(event) {
            showDetailsDialog("Add", {});
        }, false);

        $("#jsGrid").jsGrid({
            width: "100%",
            height: "480px",
            //filtering: true,
            //editing: true,
            noDataContent: "无数据",
            sorting: true,
            paging: true,
            pageLoading:true,
            autoload: true,
            pageIndex: 1,
            pageSize: 7,
            pageButtonCount: 2,
            pageIndex: 1,
            pagerFormat: "{first} {prev} {pages} {next} {last}    {pageIndex} 至 {pageCount}",
            pagePrevText:"上一页",
            pageNextText:"下一页",
            pageFirstText:"首页",
            pageLastText:"尾页",
            controller: {
                loadData: function(filter) {
                    var startIndex = (filter.pageIndex - 1) * filter.pageSize;
                    return {
                        data: (${entityList}).slice(startIndex, startIndex + filter.pageSize),
                        itemsCount: (${entityList}).length
                    };
                }
            },
            //data:(${entityList}),
            fields: [
                /*{ type: "control" },*/
                { title:"年份" ,name: "targ_year", type: "text" ,width: 100},
                { title:"季度" ,name: "targ_season", type: "text" ,width: 100},
                { title:"销售指标" ,name: "targ_targetamt", type: "text" ,width: 150}
                /*  ,{ title:"完成金额" ,name: "targ_actualamt", type: "text" ,width: 150}*/
            ]
        });
        $("#detailsDialog").dialog({
            autoOpen: false,
            width: 350,
            title:"新建指标",
            close: function() {
                $("#detailsForm").validate().resetForm();
                $("#detailsForm").find(".error").removeClass("error");
            }
        });

        var showDetailsDialog = function(dialogType, client) {
            $("#years").val(client.years);
            $("#firstJidu").val(client.firstJidu);
            $("#secondJidu").val(client.secondJidu);
            $("#thirdJidu").val(client.thirdJidu);
            $("#fourJidu").val(client.fourJidu);

            formSubmitHandler = function() {
                saveClient(client, dialogType === "Add");
            };

            $("#detailsDialog").dialog("option", "title", "新建指标" )
                    .dialog("open");
        };
        $("#detailsForm").validate({
            rules: {
                years: "required",
                firstJidu: "required",
                secondJidu: "required",
                thirdJidu: "required",
                fourJidu: "required"
            },
            messages: {
                years: "请选择年份",
                firstJidu: "请填写第一季度指标",
                secondJidu: "请填写第二季度指标",
                thirdJidu: "请填写第三季度指标",
                fourJidu: "请填写第四季度指标"
            },
            submitHandler: function() {
                formSubmitHandler();
            }
        });
        var saveClient = function(client, isNew) {
            $.extend(client, {
                years: $("#years").val(),
                firstJidu: parseFloat($("#firstJidu").val()),
                secondJidu: parseFloat($("#secondJidu").val()),
                thirdJidu: parseFloat($("#thirdJidu").val()),
                fourJidu: parseFloat($("#fourJidu").val())
            });
            var years = client.years;
            var firstJidu = client.firstJidu;
            var secondJidu = client.secondJidu;
            var thirdJidu = client.thirdJidu;
            var fourJidu = client.fourJidu;
            window.location.href="<c:url value='/target/saveTarget?years=' />"+years+"&firstJidu="+firstJidu+"&secondJidu="+secondJidu+"&thirdJidu="+thirdJidu+"&fourJidu="+fourJidu+"&compCode="+$("#compCode").val()+"&compName="+encodeURI(encodeURI($("#compName").val()))+"&Comp_companyid="+${compCompanyid}


                    /* $("#jsGrid").jsGrid(isNew ? "insertItem" : "updateItem", client);*/
                    $("#detailsDialog").dialog("close");
        };
    })

</script>
</body>
</html>
