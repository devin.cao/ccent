<%--
  Created by IntelliJ IDEA.
  User: cmt
  Date: 2018/3/26
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>业务指标维护</title>
    <style>
        .table-container::-webkit-scrollbar
        {
            -webkit-appearance: none;
            width: 14px;
            height: 14px;
        }

        .table-container::-webkit-scrollbar-thumb
        {
            border-radius: 8px;
            border: 3px solid #fff;
            background-color: rgba(0, 0, 0, .3);
        }
        .jsgrid-cell {
            //font-size: 2em;
            text-align: center;
        }
        .jsgrid-header-cell{
            //font-size: 2em;
        }
        .form-control {
            display: inline-block;
            width: 30%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
        .jsgrid-cell{
            word-break: break-all;
        }
        #pager::-webkit-input-placeholder{
            font-size: 1px;
        }

    </style>
    <link href=<c:url value="/css/jsgrid.min.css"/> rel="stylesheet">
    <link href=<c:url value="/css/jsgrid-theme.min.css" /> rel="stylesheet">
    <link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
    <link href="<c:url value='/css/style.css' />" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mui.picker.min.css' />" />
</head>
<body>
    <header class="mui-bar mui-bar-nav">
        <!--  <a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>-->
        <h1 class="mui-title">业务指标维护</h1>
    </header>
    <div class="mui-input-row" >
        <br><br>
    </div>
    <form class="mui-input-group" id="queryform" method="post" action="<c:url value='/target/targetList' />" >

        <div class="mui-input-row" >
            <label class="itemlabel">客户代码</label>   <input class="form-iteminput"  type="text" placeholder="请输入客户代码" id="compCode" name="compCode" value="${compCode}">
        </div>

        <div class="mui-input-row" >
            <label class="itemlabel">客户名称</label>   <input class="form-iteminput"  type="text" placeholder="请输入客户名称" id="compName" name="compName" value="${compName}">
        </div>

        <div style="display: none"><input type="text" id="pageIndex" name="pageIndex" ></div>
        <div style="display: none"><input type="text" id="pageSize" name="pageSize" ></div>
    </form>
    <div class=""  style="text-align: center;">
        <button type="button" id="queryButton" class="mui-btn mui-btn-primary">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div style="float: right;padding:11px;">
        <span>页码</span><input style="width: 50px;height: 28px" type="text" placeholder="输入页码" id="pager" name="pager">
    </div>
    <div id="jsGrid" class="table-container"></div>

    <script src="<c:url value="/js/jquery-1.8.3.js"/>"></script>
    <script src="<c:url value="/js/jsgrid.min.js"/>"></script>
    <script src="<c:url value='/js/mui.min.js' />"></script>
    <script src="<c:url value='/js/mui.picker.min.js' />"></script>
    <script src="<c:url value='/js/distpicker.data.js' />"></script>
    <script src="<c:url value='/js/distpicker.js' />"></script>
    <script>
        $(function() {
            mui.init({
                statusBarBackground: '#000000'
            });
            var pageIndex ="";
            var pageSize ="";
            var compCode = document.getElementById('compCode');
            var compName = document.getElementById('compName');
            var queryButton = document.getElementById('queryButton');
            queryButton.addEventListener('tap', function(event) {
                document.getElementById("queryform").submit()
            }, false);

            $("#jsGrid").jsGrid({
                width: "100%",
                height: "480px",
                //filtering: true,
                //editing: true,
                noDataContent: "无数据",
                sorting: true,
                paging: true,
                pageLoading:true,
                autoload: true,
                pageIndex: 1,
                pageSize: 7,
                pageButtonCount: 2,
                pageIndex: 1,
                pagerFormat: "{first} {prev} {pages} {next} {last}    {pageIndex} 至 {pageCount}",
                pagePrevText:"上一页",
                pageNextText:"下一页",
                pageFirstText:"首页",
                pageLastText:"尾页",
                editing:true,
                controller: {
                    loadData: function(filter) {
                        var startIndex = (filter.pageIndex - 1) * filter.pageSize;
                        return {
                            data: (${entityList}).slice(startIndex, startIndex + filter.pageSize),
                            itemsCount: (${entityList}).length
                        };
                    }
                },
                rowClick: function(args){
                    var compCompanyid = args.item.Comp_companyid;
                    var compCode = args.item.comp_code;
                    var compName = args.item.comp_name;
                    window.location.href="<c:url value='/target/targetDetail?Comp_companyid=' />"+compCompanyid+"&compCode="+compCode+"&compName="+encodeURI(encodeURI(compName))
                },
                //data:(${entityList}),
                fields: [
                    /*{ type: "control" },*/
                    { title:"客户ID" ,name: "Comp_companyid", type: "text" ,width: 100,visible:false},
                    { title:"客户代码" ,name: "comp_code", type: "text" ,width: 100},
                    { title:"客户名称" ,name: "comp_name", type: "text" ,width: 100},
/*
                    { title:"返利金额" ,name: "Rebate", type: "text" ,width: 100}
*/
                ]
            });
            $("#pager").on("change", function() {
                var page = parseInt($(this).val(), 10);
                $("#jsGrid").jsGrid("openPage", page);
            });

        })
        $(function () {
            var isPageHide = false;
            window.addEventListener('pageshow', function () {
                if (isPageHide) {
                    window.location.reload();
                }
            });
            window.addEventListener('pagehide', function () {
                isPageHide = true;
            });
        })
    </script>
</body>
</html>
