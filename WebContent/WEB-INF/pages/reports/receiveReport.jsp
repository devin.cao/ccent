<%--
  Created by IntelliJ IDEA.
  User: cmt
  Date: 2018/3/26
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>回款明细查询</title>
    <style>
        .table-container::-webkit-scrollbar
        {
            -webkit-appearance: none;
            width: 14px;
            height: 14px;
        }

        .table-container::-webkit-scrollbar-thumb
        {
            border-radius: 8px;
            border: 3px solid #fff;
            background-color: rgba(0, 0, 0, .3);
        }
        .jsgrid-cell {
            //font-size: 2em;
            text-align: center;
        }
        .jsgrid-header-cell{
            //font-size: 2em;
        }
        .form-control {
            display: inline-block;
            width: 30%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
        .jsgrid-cell{
            word-break: break-all;
        }
        #pager::-webkit-input-placeholder{
            font-size: 1px;
        }
    </style>
    <link href=<c:url value="/css/jsgrid.min.css"/> rel="stylesheet">
    <link href=<c:url value="/css/jsgrid-theme.min.css" /> rel="stylesheet">
    <link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
    <link href="<c:url value='/css/style.css' />" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mui.picker.min.css' />" />
</head>
<body>
    <header class="mui-bar mui-bar-nav">
        <!--  <a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>-->
        <h1 class="mui-title">回款明细查询</h1>
    </header>
    <div class="mui-input-row" >
        <br><br>
    </div>
    <form class="mui-input-group" id="queryform" method="get" action="<c:url value='/reports/receivelist' />" >

        <div class="mui-input-row" >
            <label class="itemlabel">开始时间</label>
            <input class="form-iteminput" id='start_date'
            name="start_date" type="text" value="${BeginDate}"
            placeholder="请输入开始时间">
        </div>

        <div  class="mui-input-row">
            <label class="itemlabel">结束时间</label>
            <input class="form-iteminput" id='end_date'
            name="end_date" type="text" value="${EndDate}"
             placeholder="请输入结束时间">
        </div>
        <div class="mui-input-row" >
            <label class="itemlabel">客户代码</label>   <input class="form-iteminput" type="text" placeholder="请输入客户代码" id="compCode" name="compCode" value="${compCode}">
        </div>
        <div>

        </div>
    </form>
    <div class=""  style="text-align: center;">
        <button type="button" id="queryButton" class="mui-btn mui-btn-primary">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div style="float: right; padding:11px;">
        <span>页码</span><input style="width: 50px;height: 28px" type="text" placeholder="输入页码" id="pager" name="pager">
    </div>
    <div style="float: left; padding:11px;">
        <span>总回款</span><input style="width: 150px;height: 28px" type="text" readonly id="receiveAmtTotal" name="receiveAmtTotal" value=${receiveAmtTotal}>元
    </div>
    <div id="jsGrid" class="table-container"></div>

    <script src="<c:url value="/js/jquery-1.8.3.js"/>"></script>
    <script src="<c:url value="/js/jsgrid.min.js"/>"></script>
    <script src="<c:url value='/js/mui.min.js' />"></script>
    <script src="<c:url value='/js/mui.picker.min.js' />"></script>
    <script src="<c:url value='/js/distpicker.data.js' />"></script>
    <script src="<c:url value='/js/distpicker.js' />"></script>
    <script>
        $(function() {
            mui.init({
                statusBarBackground: '#000000'
            });
            var compCode = document.getElementById('compCode');
            var end_date = document.getElementById('end_date');
            var start_date = document.getElementById('start_date');
            var queryButton = document.getElementById('queryButton');
            queryButton.addEventListener('tap', function(event) {
                document.getElementById("queryform").submit()
            }, false);

            start_date.addEventListener('focus', function(event) {
                document.activeElement.blur();
                var today = new Date();
                var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) });
                dtPicker.show(function(selectItems) {
                    start_date.value = selectItems.text;
                    //dtPicker.dispose();
                    dtPicker.hide();
                });
            });
            end_date.addEventListener('focus', function(event) {
                document.activeElement.blur();
                var today = new Date();
                var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) });
                dtPicker.show(function(selectItems) {
                    end_date.value = selectItems.text;
                    //dtPicker.dispose();
                    dtPicker.hide();
                });
            });
            $("#jsGrid").jsGrid({
                width: "100%",
                height: "480px",
                //filtering: true,
                //editing: true,
                sorting: true,
                noDataContent: "无数据",
                paging: true,
                pageLoading:true,
                autoload: true,
                pageIndex: 1,
                pageSize: 7,
                pageButtonCount: 2,
                pageIndex: 1,
                pagerFormat: "{first} {prev} {pages} {next} {last}    {pageIndex} 至 {pageCount}",
                pagePrevText:"上一页",
                pageNextText:"下一页",
                pageFirstText:"首页",
                pageLastText:"尾页",
                controller: {
                    loadData: function(filter) {
                        var startIndex = (filter.pageIndex - 1) * filter.pageSize;
                        return {
                            data: (${entityList}).slice(startIndex, startIndex + filter.pageSize),
                            itemsCount: (${entityList}).length
                        };
                    }
                },
               // data:(${entityList}),
                fields: [
                    /*{ type: "control" },*/
                    { title:"客户代码" ,name: "CompanyCode", type: "text" ,width: 100},
                    { title:"客户名称" ,name: "CompanyName", type: "text" ,width: 100},
                    { title:"收款金额" ,name: "ReceiveAmt", type: "text" ,width: 100},
                    { title:"类型" ,name: "Type", type: "text" ,width: 100},
                    { title:"收款日期" ,name: "ReceiveDate", type: "text" ,width: 110},
                    { title:"收款单号" ,name: "ReceiveNo", type: "text" ,width: 150},
                ]
            });
            $("#pager").on("change", function() {
                var page = parseInt($(this).val(), 10);
                $("#jsGrid").jsGrid("openPage", page);
            });
        })
    </script>
</body>
</html>
