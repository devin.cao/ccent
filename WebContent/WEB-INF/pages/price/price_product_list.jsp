<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title></title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
	<style>
	.scroll-wrapper {  
   	 	-webkit-overflow-scrolling: touch;  
    		overflow-y: scroll;  
	} 
	
	</style>	
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">价格查询</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
			 <section id="preview"></section>
			 <form class="mui-input-group" id="queryform" method="get" action="<c:url value='/product/price' />" >

				<div class="mui-input-row">
					<label class="itemlabel">产品型号</label>
					<input class="iteminput"  id='prod_name' name="prod_name" value="${prod_name}"  type="text"  class="mui-input-clear mui-input "
					 placeholder="请输入产品型号" onkeypress="myFunction(event)">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">产品代码</label>
					<input class="iteminput"  id='prod_code' name="prod_code" value="${prod_code}" type="text" class="mui-input-clear mui-input" 
					 placeholder="请输入产品代码" onkeypress="myFunction(event)">
				</div>
				 <input class="iteminput"  id='flag' name="flag" value="1"type="hidden" />
			 </form>
			<div class="mui-content-padded" style="text-align: center;">
				<button type="button" id="queryButton" class="mui-btn">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</div>
			<ul class="mui-table-view">
			 <c:forEach items="${entityList}" var="item" varStatus="s">
				<li class="mui-table-view-cell" id="cell${s.index+1}">
					
						<div class="mui-media-body">
						<input type="hidden" id="infoidcell${s.index+1}"  value="${item.prod_productid}">
						<input type="hidden" id="infonamecell${s.index+1}" value="${item.prod_name}">
						<input type="hidden" id="infomodelcell${s.index+1}" value="${item.prod_model}">
						<input type="hidden" id="infopricecell${s.index+1}" value="${item.prod_price}">
						<input type="hidden" id="infounitcell${s.index+1}" value="${item.prod_unit}">
						
						 ${ item.prod_name }<br />${ item. prod_code}<br />
							单价：${item.prod_price} 元
						</div>
					
				</li>
				</c:forEach>
				
			</ul>
			
			<c:if test="${empty entityList}">
			<div class="mui-collapse-content">
			    <p style="height: 2%"></p>
			    <p class="nodataclass" >${nodatalisttrips}</p>
			</div>
			</c:if>
			<c:if test="${!empty entityList}">
			<jsp:include page="../share/page.jsp">
			<jsp:param name="url" value="/product/price?prod_name=${prod_name}&prod_code=${prod_code }&flag=1" />
			</jsp:include>
			</c:if>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="http://lib.sinaapp.com/js/jquery/1.9.1/jquery-1.9.1.min.js"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script src="<c:url value='/layer/layer.js' />"></script>
		<script>
			function myFunction(event) {
				var x = event.which || event.keyCode;
				if(x==13){
					var queryButton = document.getElementById('queryButton');
						document.getElementById("queryform").submit()
				}
			}
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					var index = parent.layer.getFrameIndex(window.name);
					 parent.layer.close(index);
				}
				var queryButton = doc.getElementById('queryButton');
				queryButton.addEventListener('tap', function(event) {
					doc.getElementById("queryform").submit()
				}, false);
				mui(".mui-table-view").on('tap', '.mui-table-view-cell', function() {
						var id = this.getAttribute("id");
						var productid=doc.getElementById("infoid"+id).value;
						var productname=doc.getElementById("infoname"+id).value;
						var prod_model=doc.getElementById("infomodel"+id).value;
						var infoprice=doc.getElementById("infoprice"+id).value;
						var infounit=doc.getElementById("infounit"+id).value;
						//
						if(prod_model==''){
							prod_model=productname;
						}
						
					var spinner= new Spinner();
					mui.ajax("<c:url value='/product/getPrice'/>",{
						data:{
							productid:productid
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型；
						headers:null,	
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
						},
						complete: function() {
							spinner.spin();
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								//
								
								parent.$('#productid').val(productid);
								parent.$('#productname').val(productname);
								parent.$('#pade_producttype').val(prod_model);
								parent.$('#productprice').val(parseFloat(data.Price));
								parent.$('#productunit').val(infounit);
								var quantity=parent.$('#quantity').val();
								if(quantity!=''){
									var sum=Number(data.Price)*quantity;
									parent.$('#amount').val(sum);
									
								}
								//alert(quantity);
								var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
								  // alert(index);
		                        parent.layer.close(index);
		                           
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					});
						
						
						
				})
		       
			}(mui, document));
			
			
		</script>
	</body>

</html>