<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>余额查询</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
			#goor_companyid{
				margin-top: 5px;
				margin-bottom: 5px;
				margin-left: 0px;
				margin-right: 0px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
			#goor_address{
				margin-top: 5px;
				margin-bottom: 5px;
				margin-left: 0px;
				margin-right: 0px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">余额查询</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		</header>
		<div class="mui-content">
		  <section id="preview"></section>
			<form class="mui-input-group">
			
			   <div class="mui-input-row">
					<label class="itemlabel">可用余额:</label>
					<input class="iteminput" id='credit' name="credit" readonly="readonly" value="${item.credit}元" type="text" class="mui-input" >
			   </div>
			   <div class="mui-input-row">
					<label class="itemlabel">押金余额:</label>
					<input class="iteminput" id='deposit' name="deposit" readonly="readonly" value="${item.deposit}元" type="text" class="mui-input" >
			   </div>
			</form>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
	  
		<script>

		</script>
	</body>

</html>