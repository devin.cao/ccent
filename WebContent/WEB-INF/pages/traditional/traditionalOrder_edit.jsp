<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>新增传统机订单</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
			#goor_companyid{
				margin-top: 5px;
				margin-bottom: 5px;
				margin-left: 0px;
				margin-right: 0px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
			#goor_address{
				margin-top: 5px;
				margin-bottom: 5px;
				margin-left: 0px;
				margin-right: 0px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">新增传统机订单</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		  <section id="preview"></section>
			<form class="mui-input-group">
			
			   <div class="mui-input-row">
					<label class="itemlabel">可用余额:</label>
					<input class="iteminput" id='credit' name="credit" readonly="readonly" value="${item.credit}元" type="text" class="mui-input" >
				</div>
                <div class="mui-input-row">
                    <label class="itemlabel">押金余额:</label>
                    <input class="iteminput" id='deposit' name="deposit" readonly="readonly" value="${item.deposit}元" type="text" class="mui-input" >
                </div>
			  <div class="mui-input-row" style="height: 90px;">
					<label class="itemlabel">客户名称:</label>
					<textarea class="iteminput"  id='goor_companyid' name="goor_companyid" rows="2"  cols="2"   placeholder="请输入客户名称">${item.comp_name}</textarea>  
				
				</div>
				
				<div class="mui-input-row" id="addressdiv" style="height: 90px;">
					<label class="itemlabel">客户地址:</label>
                   <textarea class="iteminput"  id='goor_address' name="goor_address" rows="2"  cols="2"   placeholder="请输入客户地址">${item.comp_address}</textarea>  
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">订货日期:</label>
					<input class="iteminput" id='goor_orderdate' name="goor_orderdate" readonly="readonly" value="${goor_orderdate}" type="text" class="mui-input" placeholder="请输入订货日期">
				</div>
				<!-- 
				<div class="mui-input-row">
					<label>专卖店地址:</label>
					<input id='goor_storeaddress' name="goor_storeaddress" type="text" class="mui-input-clear mui-input" placeholder="请输入专卖店地址">
				</div>
				<div class="mui-input-row">
					<label>专卖店电话:</label>
					<input id='goor_storetel' name="goor_storetel" type="text" class="mui-input-clear mui-input" placeholder="请输入专卖店电话">
				</div>
				 -->
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">保存</button>
			</div>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
	  
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/traditional/list?flag=1'/>";
				}
			   var saveButton = doc.getElementById('saveButton');
			    saveButton.addEventListener('tap', function(event) {
			    	var goor_companyid=doc.getElementById('goor_companyid').value;
					var goor_address=doc.getElementById('goor_address').value;
					var goor_orderdate=doc.getElementById('goor_orderdate').value;
					//var goor_earnest=doc.getElementById('goor_earnest').value;
					//var goor_storeaddress=doc.getElementById('goor_storeaddress').value;
					//var goor_storetel=doc.getElementById('goor_storetel').value;
					
					
					if(goor_companyid==''){
						mui.alert("客户名称不能为空");
						return ;
					}
					if(goor_address==''){
						mui.alert("客户地址不能为空");
						return ;
					}
					if(goor_orderdate==''){
						mui.alert("订货日期不能为空");
						return ;
					}
					
					
					var spinner= new Spinner();
					mui.ajax("<c:url value='/traditional/doEdit'/>",{
						data:{
							goor_companyid:goor_companyid,
							goor_address:goor_address,
							goor_orderdate:goor_orderdate
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型；
						headers:null,
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							saveButton.disabled=true;
						},
						complete: function() {
							spinner.spin();
							saveButton.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								//
								alert(data.Message);
								//mui.back();
								window.location.href="<c:url value='/traditional/get?goor_status=1&id='/>"+data.ID;
								
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					});
					
				}, false);
			}(mui, document));
		</script>
	</body>

</html>