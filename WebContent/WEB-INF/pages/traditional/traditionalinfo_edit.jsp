<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title></title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			.mui-input-group label {
				width: 38%;
			}
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 62%;
			}
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">新增传统机订单明细</h1>
		    <button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		 <section id="preview"></section>
			<form class="mui-input-group">
			  <input id='gode_goodsorderid' name="gode_goodsorderid" type="hidden" class="mui-input-clear mui-input" value="${gode_goodsorderid}">
				<input id='productid' name="gode_productid" type="hidden" class="mui-input-clear mui-input" >
				<input id='pade_producttype' name="pade_producttype" type="hidden" class="mui-input-clear mui-input" >
			    <input id='productunit' name="productunit" type="hidden" class="mui-input-clear mui-input" >
			
				<div class="mui-input-row">
					<label  class="itemlabel"  style="width:30%;">产品名称:</label>
					<input  class="iteminput" id='productname' style="width:50%;float:left;" readonly="readonly" name="productname" type="text" class="mui-input" placeholder="选择产品名称">
					<input type="button" id="qryProductname" value="选择" style="color: blue;width:20%;float:left;"  />
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">单价(元):</label>
					<input  class="iteminput" id='productprice' readonly="readonly" name="gode_price" type="text" class="mui-input" placeholder="请输入单价">
				</div>
				
				<div class="mui-input-row">
					<label  class="itemlabel">数量:</label>
					<input  class="iteminput" id='quantity' name="gode_quantity" type="number" class="mui-input-clear mui-input" placeholder="请输入数量" value= quantity >
				</div>
				
				<div class="mui-input-row">
					<label  class="itemlabel">合计(元):</label>
					<input  class="iteminput" id='amount' name="gode_amount" readonly="readonly" type="text" class="mui-input" >
				</div>
				
				<div class="mui-input-row">
					<label  class="itemlabel">特殊说明:</label>
					<input  class="iteminput" id='gode_remarks' name="gode_remarks" type="text" class="mui-input" >
				</div>
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">保存</button>
			</div>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
	  	<script src="<c:url value='/layer/layer.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/traditional/detaillist?gode_goodsorderid=${gode_goodsorderid}&goor_status=1'/>";
				}
				var productname = doc.getElementById('productname');
				productname.addEventListener('tap', function(event) {
					layer.open({
						   type: 2,
						  area: ['100%', '100%'],
						  title: false, //不显示标题
						  closeBtn: 0,
						  fixed: true, //不固定
						  maxmin: false,
						  content: "<c:url value='/product/list?prp=4' />"
					});
				 }, false);
				
				var qryProductname=doc.getElementById('qryProductname');
				qryProductname.addEventListener('tap', function(event) {
					layer.open({
						   type: 2,
						  area: ['100%', '100%'],
						  title: false, //不显示标题
						  closeBtn: 0,
						  fixed: true, //不固定
						  maxmin: false,
						  content: "<c:url value='/product/list?prp=4' />"
					});
				 }, false);
				
			   var saveButton = doc.getElementById('saveButton');
			    saveButton.addEventListener('tap', function(event) {
			    	var gode_goodsorderid=doc.getElementById('gode_goodsorderid').value;
					var gode_productid=doc.getElementById('productid').value;
					//var gode_color=doc.getElementById('gode_color').value;
					var gode_price=doc.getElementById('productprice').value;
					var gode_quantity=doc.getElementById('quantity').value;
					var gode_amount=doc.getElementById('amount').value;
					var gode_remarks=doc.getElementById('gode_remarks').value;//新增传统机订单明细备注by mars
					if(gode_productid==''){
						mui.alert("传统机名不能为空");
						return ;
					}
					
					if(gode_price==''){
						mui.alert("单价不能为空");
						return ;
					}
					if(gode_quantity==''){
						mui.alert("数量不能为空");
						return ;
					}
					
					var r = /^\+?[1-9][0-9]*$/;
					if(!r.test(gode_quantity)){  
                       mui.alert("数量必须为整数");
                       return ;
                    }
					
					if(Number(gode_quantity)<=0){
						mui.alert("数量必须大于0");
						return ;
					}
					if(gode_quantity.length>4){
						mui.alert("数量不能超过四位数");
						return ;
					}
					if(gode_amount==''){
						mui.alert("合计不能为空");
						return ;
					}
					 var loding=false;
					 var spinner= new Spinner();
					mui.ajax("<c:url value='/traditional/detailDoAdd'/>",{
						data:{
							gode_productid:gode_productid,
							gode_price:gode_price,
							gode_quantity:gode_quantity,
							gode_goodsorderid:gode_goodsorderid,
							gode_amount:gode_amount,
							gode_remarks:gode_remarks
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型；
						headers:null,	
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							saveButton.disabled=true;
						},
						complete: function() {
							spinner.spin();
							saveButton.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								//
								alert(data.Message);
								//mui.back();
								//保存数量和传统机到session
								localStorage.setItem("quantity",doc.getElementById('quantity').value);
								localStorage.setItem("productname",doc.getElementById('productname').value);
								window.location.href="<c:url value='/traditional/detaillist?goor_status=1&gode_goodsorderid='/>"+gode_goodsorderid;
								
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					});
					
				}, false);
			   
			}(mui, document));
			 $('#quantity').bind('input', function () {
				
            	 var quantity=$('#quantity').val().trim();
            	 if(quantity!=''){
            		 var productprice=$('#productprice').val().trim();
            		 if(productprice!=''){
            			 var sum=Number(productprice)*quantity;
            			 //alert(sum);
            			 $('#amount').val(sum);
            		 }
            	 }
				
			 });
		</script>
	</body>

</html>