<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>传统机订单明细</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/mui.picker.min.css' />" />
	
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">传统机订单明细</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		 <section id="preview"></section>
		  <input id="gode_goodsorderid" type="hidden" name="gode_goodsorderid" value="${gode_goodsorderid}">
		   <input id="gode_status" type="hidden" name="gode_status" value="${gode_status}">
			<c:if test="${gode_status eq 1 }">
			<div class="mui-content-padded" style="text-align: center;"  id="submitdiv">
			
				&nbsp;&nbsp;<button type="button" id="addButton" class="mui-btn mui-btn-primary">
				<c:if test="${fn:length(entityList)>0}">继续订购</c:if>
			<c:if test="${fn:length(entityList)==0}">订购传统机</c:if>
				</button>
				&nbsp;&nbsp;<button type="button" id="submitButton" class="mui-btn mui-btn-primary">提交</button>
			</div>
            </c:if>
			<ul class="mui-table-view">
			 <c:forEach items="${entityList}" var="item" varStatus="s">
				<li class="mui-table-view-cell" id="cell${s.index+1}">
					<div class="mui-media-body">
					     <form method="post" id="formcell${s.index+1}" action="<c:url value='/traditional/getinfo' />">
						 <input type="hidden" name="gode_goodsorderid"  value="${gode_goodsorderid}">
						 <input type="hidden" name="info" id="entitycell${s.index+1}" value="{'gode_GoodsDetailsID':'${ item.gode_GoodsDetailsID}','gode_name':'${ item.gode_name}','gode_productid':'${ item.gode_productid}','gode_color':'${ item.gode_color}','gode_price':'${ item.gode_price}','gode_quantity':'${ item.gode_quantity}','gode_remarks':'${ item.gode_remarks}','gode_goodsorderid':'${ item.gode_goodsorderid}','gode_status':'${ item.gode_status}','gode_productname':'${item.gode_productname}','gode_shipments':'${item.gode_shipments}'}">
						 </form>
							序号：${ item.gode_name }<br /> 产品名称：${ item.gode_productname }
							<br />单价：${ item.gode_price}元
							<br />数量：<fmt:parseNumber var="num" integerOnly="true" value="${ item.gode_quantity }" />
							 <c:out value="${num}" />
							<br />合计：${ item.gode_price*item.gode_quantity}元<br />

							已发货数量：<fmt:parseNumber var="sendnum" integerOnly="true" value="${ item.gode_shipments }" />
							<c:out value="${sendnum}" /><br />
							特殊说明：${ item.gode_remarks }<br/>
					</div>
					
				</li>
				</c:forEach>
				
			</ul>
			<c:if test="${empty entityList}">
			<div class="mui-collapse-content">
			    <p style="height: 2%"></p>
			    <p class="nodataclass" >${nodatalisttrips}</p>
			</div>
			</c:if>
			<c:if test="${!empty entityList}">
			<jsp:include page="../share/page.jsp">
			<jsp:param name="url" value="/traditional/detaillist?gode_goodsorderid=${gode_goodsorderid}" />
			</jsp:include>
			</c:if>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script src="<c:url value='/js/mui.picker.min.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/traditional/get?id=${gode_goodsorderid}'/>";
				}
				mui(".mui-table-view").on('tap', '.mui-table-view-cell', function() {
					var id = this.getAttribute("id");
					var formid="form"+id;//
					var status= doc.getElementById("gode_status").value;
					if(status==1){
						doc.getElementById(formid).submit();
					}
				});
				var addButton = doc.getElementById('addButton');
				addButton.addEventListener('tap', function(event) {
					window.location.href="<c:url value='/traditional/detailAdd?gode_goodsorderid=${gode_goodsorderid}'/>";
				}, false);
				var submitButton = doc.getElementById('submitButton');
				submitButton.addEventListener('tap', function(event) {
					var btnArray = ['取消', '确定'];   
	                mui.confirm('确定要提交订单吗', '提示', btnArray, function(e) {
	                    if (e.index == 1) {
	                    	dataSubmit();
	                    } else {
	                    }
	                })
				}, false);
				function dataSubmit(){
					 var orderid= doc.getElementById('gode_goodsorderid').value;
					 var spinner= new Spinner();
						mui.ajax("<c:url value='/traditional/submit'/>",{
							data:{
								orderid:orderid
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(doc.getElementById('preview'));
								submitButton.disabled=true;
								addButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								submitButton.disabled=false;
								addButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									alert(data.Message);
									 doc.getElementById('submitdiv').style.display = "none";
									doc.getElementById("gode_status").value="2"; 
									 //change status
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				};
			}(mui, document));

			
		</script>
	</body>

</html>