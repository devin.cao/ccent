<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html class="ui-page-login">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>产品基本信息</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			
			.mui-input-group {
				margin-top: 10px;
			}
			
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-input-group label {
				width: 27%;
			}
			
			#vcode {
				width: 50%;
				float: left;
			}
			
			#code {
				width: 23%;
				float: right;
			}
			
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 73%;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			
			.mui-content-padded {
				margin-top: 25px;
			}
			
			.mui-btn {
				padding: 10px;
			}
			
			.link-area {
				display: block;
				margin-top: 25px;
				text-align: center;
			}
			
			.spliter {
				color: #bbb;
				padding: 0px 8px;
			}
			
			.oauth-area {
				position: absolute;
				bottom: 20px;
				left: 0px;
				text-align: center;
				width: 100%;
				padding: 0px;
				margin: 0px;
			}
			
			.oauth-area .oauth-btn {
				display: inline-block;
				width: 50px;
				height: 50px;
				background-size: 30px 30px;
				background-position: center center;
				background-repeat: no-repeat;
				margin: 0px 20px;
				/*-webkit-filter: grayscale(100%); */
				border: solid 1px #ddd;
				border-radius: 25px;
			}
			
			.oauth-area .oauth-btn:active {
				border: solid 1px #aaa;
			}
			
			.oauth-area .oauth-btn.disabled {
				background-color: #ddd;
			}
		</style>

	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<!--  <a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>-->
			<h1 class="mui-title">产品基本信息</h1>
		</header>
		<div class="mui-content">
		  <c:if test="${!empty entity.prin_name }">
		  <ul class="mui-table-view">
			    <li class="mui-table-view-cell">
					<label>产品名称:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>${entity.prin_name }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>产品类型:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>${entity.prin_producttype }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>产品型号:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>${entity.prin_productmodel }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>机头编号:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>${entity.prin_headsn }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>机身编号:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>${entity.prin_bodysn }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>生产日期:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>
					<c:set var="s_date" value="${ entity.prin_manufacturedate}"/>
						<c:choose>  
                         <c:when test="${fn:length(s_date) > 10}">  
                           <c:out value="${fn:substring(s_date, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${s_date}" />  
                     </c:otherwise>  
                    </c:choose>
			      
					</label>
				</li>
				<li class="mui-table-view-cell">
					<label>代理商名称:</label>&nbsp;&nbsp;&nbsp;<label>${entity.prin_company }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>出货日期:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>
					<c:set var="p_date" value="${ entity.prin_shipmentdate}"/>
						<c:choose>  
                         <c:when test="${fn:length(p_date) > 10}">  
                           <c:out value="${fn:substring(p_date, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${p_date}" />  
                     </c:otherwise>  
                    </c:choose>
					</label>
				</li>
				<li class="mui-table-view-cell">
					<label>用户姓名:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>${entity.prin_consumer }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>安装日期:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>
					<c:set var="pi_date" value="${ item.prin_installationdate}"/>
						<c:choose>  
                         <c:when test="${fn:length(pi_date) > 10}">  
                           <c:out value="${fn:substring(pi_date, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${pi_date}" />  
                     </c:otherwise>  
                    </c:choose>
					</label>
				</li>
				<li class="mui-table-view-cell">
					<label>购买日期:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>
					<c:set var="pp_date" value="${ entity.prin_purchasedate}"/>
						<c:choose>  
                         <c:when test="${fn:length(pp_date) > 10}">  
                           <c:out value="${fn:substring(pp_date, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${pp_date}" />  
                     </c:otherwise>  
                    </c:choose>
					</label>
				</li>
			</ul>
		  </c:if>
		  <c:if test="${empty entity.prin_name }">
			<div class="mui-collapse-content" style="background: white;height: 100%">
			    <p style="height: 2%"></p>
			    <p style="text-align: center;color: red;">无法找到该产品</p>
			</div>
			</c:if>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/mui.enterfocus.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				
			}(mui, document));
		</script>
	</body>

</html>