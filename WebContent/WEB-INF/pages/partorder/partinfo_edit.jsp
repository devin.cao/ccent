<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>新增配件明细</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">新增配件明细</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		  <section id="preview"></section>
			<form class="mui-input-group">
				
				<input id='pade_partsorderid' name="pade_partsorderid" value="${pade_partsorderid}" type="hidden" class="mui-input-clear mui-input">
				<input id='productid' name="pade_productsid" type="hidden" class="mui-input-clear mui-input" >
				<div class="mui-input-row" >
					<label  class="itemlabel" style="width:30%;">配件名称:</label>
					<input id='productname' style="width:50%;float:left;" readonly="readonly" name="productname" type="text" class="mui-input" placeholder="选择配件名称">					
					<input type="button" id="qryProductname" value="选择" style="color: blue;width:20%;float:left;"  />
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">产品型号:</label>
					
					<input   class="iteminput" id='pade_producttype' readonly="readonly" name="pade_producttype" type="text" class="mui-input" placeholder="">
					
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">单价(元):</label>
					<input   class="iteminput" id='productprice' name="pade_price" readonly="readonly" type="text" class="mui-input" placeholder="请输入单价">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">单位:</label>
					<input   class="iteminput" id='productunit'  name="pade_unit"  readonly="readonly" type="text" class="mui-input" placeholder="请输入单位">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">数量:</label>
					<input   class="iteminput" id='quantity' name="pade_quantity" type="number" class="mui-input-clear mui-input" placeholder="请输入数量">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">合计(元):</label>
					<input   class="iteminput" id='amount' readonly="readonly" name="pade_amount" type="text" class="mui-input" placeholder="">
				</div>
				 
				<div class="mui-input-row">
					<label>特殊说明:</label>
					<input id='pade_specialremark' name="pade_specialremark" type="text" class="mui-input-clear mui-input" placeholder="请输入特殊说明">
				</div> 
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">保存</button>
			</div>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
	   <script src="<c:url value='/js/spin.js' />"></script>
	   	<script src="<c:url value='/layer/layer.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/partorder/detaillist?pade_partsorderid=${pade_partsorderid}&paor_status=1'/>";
				}
				var productname = doc.getElementById('productname');
				productname.addEventListener('tap', function(event) {
					layer.open({
						   type: 2,
						  area: ['100%', '100%'],
						  title: false, //不显示标题
						  closeBtn: 0,
						  fixed: true, //不固定
						  maxmin: false,
						  content: "<c:url value='/product/list?prp=2' />",
						  success: function(layero){
				               $(layero).addClass("scroll-wrapper");//苹果 iframe 滚动条失效解决方式
				            }
					});
				 }, false);
				var qryProductname=doc.getElementById('qryProductname');
				qryProductname.addEventListener('tap', function(event) {
					layer.open({
						   type: 2,
						  area: ['100%', '100%'],
						  title: false, //不显示标题
						  closeBtn: 0,
						  fixed: true, //不固定
						  maxmin: false,
						  content: "<c:url value='/product/list?prp=2' />",
						  success: function(layero){
				               $(layero).addClass("scroll-wrapper");//苹果 iframe 滚动条失效解决方式
				            }
					});
				 }, false);
				
			   var saveButton = doc.getElementById('saveButton');
			    saveButton.addEventListener('tap', function(event) {
			    	var pade_partsorderid=doc.getElementById('pade_partsorderid').value;
			    	
					var pade_producttype=doc.getElementById('pade_producttype').value;
					var pade_productsid=doc.getElementById('productid').value;
					var pade_price=doc.getElementById('productprice').value;
					var pade_unit=doc.getElementById('productunit').value;
					var pade_quantity=doc.getElementById('quantity').value;
					var pade_amount=doc.getElementById('amount').value;
					var pade_specialremark=doc.getElementById('pade_specialremark').value;
					
					//var productname=doc.getElementById('productname').value;
					//alert(pade_productsid);
					//if(pade_producttype==''){
					//	mui.alert("产品型号不能为空");
					//	return ;
					//}
					
					if(pade_productsid==''){
						mui.alert("配件名称不能为空");
						return ;
					}
					if(pade_price==''){
						mui.alert("单价不能为空");
						return ;
					}
					if(pade_unit==''){
						mui.alert("单位不能为空");
						return ;
					}
					if(pade_quantity==''){
						mui.alert("数量不能为空");
						return ;
					}
					 var r = /^\+?[1-9][0-9]*$/;
					if(!r.test(pade_quantity)){ 
                       mui.alert("数量必须为整数");
					   return ;
                    }
					if(Number(pade_quantity)<=0){
						mui.alert("数量必须大于0");
						return ;
					}
					if(pade_quantity.length>4){
						mui.alert("数量不能超过四位数");
						return ;
					}
					if(pade_amount==''){
						mui.alert("合计不能为空");
						return ;
					}
					var spinner= new Spinner();
					mui.ajax("<c:url value='/partorder/detailDoAdd'/>",{
						data:{
							pade_partsorderid:pade_partsorderid,
							pade_producttype:pade_producttype,
							pade_productsid:pade_productsid,
							pade_price:pade_price,
							pade_unit:pade_unit,
							pade_quantity:pade_quantity,
							pade_amount:pade_amount,
							pade_specialremark
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,	
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							saveButton.disabled=true;
						},
						complete: function() {
							spinner.spin();
							saveButton.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								//
								alert(data.Message);
								window.location.href="<c:url value='/partorder/detaillist?paor_status=1&pade_partsorderid='/>"+pade_partsorderid;
								
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					});
					
				}, false);
			    function fun1(e){  
                    var val=e.detail.inputVal  
                    alert(val);
                }
                window.addEventListener('doit',fun1); 
			}(mui, document));
             $('#quantity').bind('input', function () {
            	 var quantity=$('#quantity').val().trim();
            	 if(quantity!=''){
            		 var productprice=$('#productprice').val().trim();
            		 if(productprice!=''){
            			 var sum=Number(productprice)*quantity;
            			 $('#amount').val(sum);
            		 }
            	 }
				
			 });
		</script>
	</body>

</html>