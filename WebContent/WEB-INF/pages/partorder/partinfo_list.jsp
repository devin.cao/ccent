<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>配件明细</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/mui.picker.min.css' />" />
	
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">配件明细</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		   <section id="preview"></section>
		   
			<input id="pade_partsorderid" type="hidden" name="pade_partsorderid" value="${pade_partsorderid}">
			<c:if test="${pade_status eq 1 }">
			<div class="mui-content-padded" style="text-align: center;" id="submitdiv">
			&nbsp;&nbsp;<button type="button" id="addButton" class="mui-btn mui-btn-primary">
			<c:if test="${fn:length(entityList)>0}">继续订购</c:if>
			<c:if test="${fn:length(entityList)==0}">订购配件</c:if>
			</button>
			&nbsp;&nbsp;<button type="button" id="submitButton" class="mui-btn mui-btn-primary">提交</button>
			</div>
            </c:if>
            <input type="hidden" name="pade_status" id="pade_status"  value="${pade_status}">
			<ul class="mui-table-view">
			 <c:forEach items="${entityList}" var="item" varStatus="s">
				<li class="mui-table-view-cell" id="cell${s.index+1}">
					<a href="javascript:;" style="color: black;">
						<div class="mui-media-body">
						 <form method="post" id="formcell${s.index+1}" action="<c:url value='/partorder/getinfo' />">
						 <input type="hidden" name="pade_partsorderid"  value="${pade_partsorderid}">
						
						 <input type="hidden" name="info" id="entitycell${s.index+1}" value="{'pade_name':'${ item.pade_name}','pade_price':'${ item.pade_price}','pade_unit':'${ item.pade_unit}','pade_partsorderid':'${ item.pade_partsorderid}','pade_producttype':'${ item.pade_producttype}','pade_productsid':'${ item.pade_productsid}','pade_products':'${ item.pade_products}','pade_amount':'${ item.pade_amount}','pade_specialremark':'${ item.pade_specialremark}','pade_quantity':'${ item.pade_quantity}','pade_status':'${ item.pade_status}','pade_PartsDetailsID':'${ item.pade_PartsDetailsID}','pade_shipments':'${item.pade_shipments}'}">
						 </form>
							序号：${ item.pade_name }<br /> 型号：${ item.pade_producttype }
							<br />配件名称：${ item.pade_products }<br />单价：${ item.pade_price }元
							<br />数量：
							<fmt:parseNumber var="num" integerOnly="true" value="${ item.pade_quantity }" />
							 <c:out value="${num}" />
							<br />合计：${ item.pade_amount }元<br />
							已发货数量：<fmt:parseNumber var="sendnum" integerOnly="true" value="${ item.pade_shipments }" />
							<c:out value="${sendnum}" /><br />
							特殊说明：${ item.pade_specialremark }<br/>
						</div>
						
					</a>
				</li>
				</c:forEach>
				
			</ul>
			<c:if test="${empty entityList}">
			<div class="mui-collapse-content">
			    <p style="height: 2%"></p>
			    <p class="nodataclass" >${nodatalisttrips}</p>
			</div>
			</c:if>
			<c:if test="${!empty entityList}">
			<jsp:include page="../share/page.jsp">
			<jsp:param name="url" value="/partorder/detaillist?pade_partsorderid=${pade_partsorderid}" />
			</jsp:include>
			</c:if>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script src="<c:url value='/js/mui.picker.min.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/partorder/get?id=${pade_partsorderid}'/>";
				}
				mui(".mui-table-view").on('tap', '.mui-table-view-cell', function() {
					var id = this.getAttribute("id");
					var status= doc.getElementById("pade_status").value;
					if(status==1){
						var formid="form"+id;
						//alert(formid);
						doc.getElementById(formid).submit()
					}
				});
				var addButton = doc.getElementById('addButton');
				addButton.addEventListener('tap', function(event) {
					//window.location.href="<c:url value='/partorder/detailAdd?pade_partsorderid=${pade_partsorderid}'/>";
					 mui.openWindow({  
						    url: "<c:url value='/partorder/detailAdd?pade_partsorderid=${pade_partsorderid}'/>",   
						    id:'partinfo_eidit'  
					 });  
				}, false);

				var submitButton = doc.getElementById('submitButton');
				submitButton.addEventListener('tap', function(event) {
						
						var btnArray = ['取消', '确定'];   
		                mui.confirm('确定要提交订单吗', '提示', btnArray, function(e) {
		                    if (e.index == 1) {
		                    	dataSubmit();
		                    } else {
		                    }
		                })
				}, false);
				function dataSubmit(){
					 var paor_PartsOrderID= doc.getElementById('pade_partsorderid').value;
					 var submitButton = doc.getElementById('submitButton');
					 var spinner= new Spinner();
					mui.ajax("<c:url value='/partorder/submit'/>",{
							data:{
								orderid:paor_PartsOrderID
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(doc.getElementById('preview'));
								submitButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								submitButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									 alert(data.Message);
									 doc.getElementById('submitdiv').style.display = "none";
										doc.getElementById("pade_status").value="2"; 
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				};
			}(mui, document));
			
			
		</script>
	</body>

</html>