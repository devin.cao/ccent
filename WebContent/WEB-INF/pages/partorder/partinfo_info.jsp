<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>配件明细</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
.wrap{
    margin: 0 auto;
    overflow: hidden;   /*用overflow: hidden 到容器里的元素*/
}
.sideleft{                /*设置左边通栏*/
    width: 30%;
    float: left;
    text-align: left;
}

.sideright{                /*设置左边通栏*/
    width: 70%;
    float: left;
    text-align: left;
    word-break: break-all;
}

.wrap{
    display: inline-block;
	display: block;
}
#quantity{
 padding: 0px 0px;
 height: 26px
}
.sideleft, .sideright{
    padding-bottom: 32767px !important;  /*应用 padding-bottom（足够大的值）到列的块元素*/
    margin-bottom: -32767px !important;  /*应用 margin-bottom（足够大的值）到列的块元素*/
}
@media all and (min-width: 0px){
    .sideleft, .sideright{
        padding-bottom: 0 !important;
        margin-bottom: 0 !important; 
    }
    .sideleft:before, .sideright:before{
        content: '[DO NOT LEAVE IT IS NOT REAL]';
        display: block;
        background: inherit;
        padding-top: 32767px !important;
        margin-bottom: -32767px !important;
        height: 0;

    }

}
</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">配件明细</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		<section id="preview"></section>
			<form class="mui-input-group">
		<ul class="mui-table-view">
			<li class="mui-table-view-cell">
			<input id='pade_partsorderid' name="pade_partsorderid" value="${pade_partsorderid}" type="hidden" class="mui-input">
			<input id='pade_PartsDetailsID' name="pade_PartsDetailsID" value="${item.pade_PartsDetailsID}" type="hidden" class="mui-input">
			<input id='price' name="price" value="${item.pade_price}" type="hidden" class="mui-input"/>
		    <input id='amount' name="amount" value="${item.pade_price*item.pade_quantity}" type="hidden" class="mui-input"/>
			
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">序号:</div><div class="sideright">${item.pade_name }</div></div>
			</li>
			
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">产品型号:</div><div class="sideright">${item.pade_producttype }</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">配件名称:</div><div class="sideright">${item.pade_products }</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">单价:</div><div class="sideright">${item.pade_price }元</div></div>
			</li>
			
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">数量:</div><div class="sideright">
			<c:if test="${item.pade_status eq 1}">
			<input id='quantity' name="quantity" value="${item.pade_quantity}" type="number" class="mui-input"/>
		    </c:if>
		    <c:if test="${item.pade_status ne 1}">
			${item.pade_quantity }
		    </c:if></div></div>
			</li>
			
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">合计:</div><div class="sideright"  id="amountcontent">${item.pade_amount }元</div></div>
			</li>
			<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">已发货数量:</div><div class="sideright" id="sendquantity">${ item.pade_shipments}</div></div>
			</li>
			<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">特殊说明:</div><div class="sideright"  >${item.pade_specialremark }</div></div>
			</li>
		</ul>
		</form>
		<c:if test="${item.pade_status eq 1}">
		<div class="mui-content-padded" id="deletediv">
		         <button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">保存</button>
				<button id='deleteButton' class="mui-btn mui-btn-block mui-btn-primary">删除</button>
		</div>
		</c:if>
	</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
			<script src="<c:url value='/js/spin.js' />"></script>
	   <script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/partorder/detaillist?pade_partsorderid=${pade_partsorderid}'/>";
				}
				var saveButton = doc.getElementById('saveButton');
				saveButton.addEventListener('tap', function(event) {
					var quantity=doc.getElementById('quantity').value;
					 var r = /^\+?[1-9][0-9]*$/;
					if(!r.test(quantity)){  
                       mui.alert("数量必须为整数");
                       return ;
                    }
					if(Number(quantity)<=0){
						mui.alert("数量必须大于0");
						return ;
					}
					if(quantity.length>4){
						mui.alert("数量不能超过四位数");
						return ;
					}
					dataChange();
				}, false);
				var deleteButton = doc.getElementById('deleteButton');
				deleteButton.addEventListener('tap', function(event) {
					var btnArray = ['取消', '确定'];   
		                mui.confirm('确定要删除明细订单吗', '提示', btnArray, function(e) {
		                    if (e.index == 1) {
		                    	dataDelete();
		                    } else {
		                    }
		                })
				}, false);
				function dataChange(){
					 var id= doc.getElementById('pade_PartsDetailsID').value;
					 var amount= doc.getElementById('amount').value;
					 var quantity= doc.getElementById('quantity').value;
					 var spinner= new Spinner();
						mui.ajax("<c:url value='/partorder/changeNum'/>",{
							data:{
								id:id,
								amount:amount,
								quantity:quantity
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(document.getElementById('preview'));
								saveButton.disabled=true;
								deleteButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								saveButton.disabled=false;
								deleteButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									 alert(data.Message);
									 window.location.href="<c:url value='/partorder/detaillist?pade_partsorderid=${pade_partsorderid}'/>";
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				};
				function dataDelete(){
					 var id= doc.getElementById('pade_PartsDetailsID').value;
					 var spinner= new Spinner();
						mui.ajax("<c:url value='/partorder/detailDelete'/>",{
							data:{
								id:id
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(document.getElementById('preview'));
								saveButton.disabled=true;
								deleteButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								saveButton.disabled=false;
								deleteButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									 alert(data.Message);
									 window.location.href="<c:url value='/partorder/detaillist?pade_partsorderid=${pade_partsorderid}'/>";
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				};
			}(mui, document));
			$('#quantity').bind('input', function () {
				
	           	 var quantity=$('#quantity').val().trim();
	           	 if(quantity!=''){
	           		 var productprice=$('#price').val().trim();
	           		 if(productprice!=''){
	           			 var sum=Number(productprice)*quantity;
	           			 //alert(sum);
	           			 $('#amountcontent').text(sum+"元");
	           			 $('#amount').val(sum);
	           		 }
	           	 }
					
				 });
			</script>
		
	</body>

</html>