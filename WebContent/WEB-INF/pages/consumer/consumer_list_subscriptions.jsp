<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>安装工单信息</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style type="text/css">
	a {
	color: black;
	}
	</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<!--  <a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>-->
			<h1 class="mui-title">安装工单信息</h1>
		</header>
		<div class="mui-content">
		  <div id="searchdv"  <c:if test="${q ne 1}">style="display:none;"</c:if>   >
			<form class="mui-input-group" id="queryform" method="get" action="<c:url value='/consumer/consumer_list_subscriptions' />" >
			<input id='q' name="q" value="1" type="hidden" class="mui-input-clear mui-input" >
				<div class="mui-input-row">
					<label  class="itemlabel">用户名称:</label>
					<input class="iteminput" id='cons_name' name="cons_name" value="${cons_name}" type="text" class="mui-input-clear mui-input" placeholder="请输入用户名称">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">手机号码:</label>
					<input class="iteminput" id='cons_phonenumber' name="cons_phonenumber" value="${cons_phonenumber}" type="text" class="mui-input-clear mui-input" placeholder="请输入手机号码">
				</div>
				 
			</form>
			</div>
			<div class="mui-content-padded" style="text-align: center;">
				<button type="button" id="queryButton" class="mui-btn mui-btn-primary">查询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" id="addButton" class="mui-btn mui-btn-primary">新增</button>
			</div>
			<ul class="mui-table-view">
			  <c:forEach items="${entityList}" var="item" varStatus="s">
				  <li class="mui-table-view-cell" id="cell${s.index+1}">
					 <a href="#" class="mui-navigate-right" >
						<div class="mui-media-body">
							<form method="post" id="formcell${s.index+1}" action="<c:url value='/consumer/get' />">
								<input type="hidden" name="info" id="entitycell${s.index+1}" value="{'cons_name':'${ item.cons_name}','cons_Status':'${ item.cons_Status}','cons_phonenumber':'${ item.cons_phonenumber}','cons_source':'${ item.cons_source}','cons_birthday':'${ item.cons_birthday}','cons_address':'${ item.cons_address}','cons_productoption':'${ item.cons_productoption}','cons_province':'${ item.cons_province}','cons_city':'${ item.cons_city}','cons_county':'${ item.cons_county}','cons_purchaseplace':'${ item.cons_purchaseplace}','cons_purchasedate':'${ item.cons_purchasedate}','cons_installationdate':'${ item.cons_installationdate}','cons_Installationname':'${ item.cons_Installationname}','cons_Installationtel':'${ item.cons_Installationtel}','cons_company':'${ item.cons_company}','cons_headsn':'${ item.cons_headsn}','cons_bodysn':'${ item.cons_bodysn}','cons_producttype':'${ item.cons_producttype}','cons_manufacturedate':'${ item.cons_manufacturedate}','cons_productcategory':'${item.cons_productcategory}','cons_screenwificode':'${item.cons_screenwificode }','cons_gender':'${item.cons_gender }','consumer_body':${item.consumer_body_string}}">
							</form>
							用户名称：${ item.cons_name }
							<br /> 手机号码：${ item.cons_phonenumber}<br />
							<nobr>详细地址：${ item.cons_address}</nobr><br />
						<%--	用户来源：
            <c:if test="${ item.cons_source eq 1}">专卖店</c:if>
			<c:if test="${ item.cons_source eq 2}">高铁广告</c:if>
			<c:if test="${ item.cons_source eq 3}">机场广告</c:if>
			<c:if test="${ item.cons_source eq 4}">央视广告</c:if>
			<c:if test="${ item.cons_source eq 5}">网站</c:if>
			<c:if test="${ item.cons_source eq 6}">卖场</c:if>
			<c:if test="${ item.cons_source eq 7}">建材城</c:if>
							<br />--%>
						</div>
					</a>
				</li>
				</c:forEach>
				
			</ul>
			<c:if test="${empty entityList}">
			<div class="mui-collapse-content">
			    <p style="height: 2%"></p>
			    <p class="nodataclass" >${nodatalisttrips}</p>
			</div>
			</c:if>
			<c:if test="${!empty entityList}">
			<jsp:include page="../share/page.jsp">
			<jsp:param name="url" value="/consumer/list?cons_name=${cons_name }&cons_phonenumber=${cons_phonenumber}" />
			</jsp:include>
			</c:if>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var queryButton = doc.getElementById('queryButton');
				queryButton.addEventListener('tap', function(event) {
					var searchdv=doc.getElementById("searchdv").style.display;
					if(searchdv=='none'){
						doc.getElementById("searchdv").style.display="block";
					}else{
						doc.getElementById("queryform").submit();
					}
					
				}, false);
				var addButton = doc.getElementById('addButton');
				addButton.addEventListener('tap', function(event) {
					mui.openWindow({
					id: 'consumer_edit',
					url: '<c:url value='/consumer/toEditForSubscriptions' />'
				   });
				}, false);
				mui(".mui-table-view").on('tap', '.mui-table-view-cell', function() {
					//获取id
					var id = this.getAttribute("id");
					var formid="form"+id;
					//alert(formid);
					doc.getElementById(formid).submit()
					
					//打开新闻详情
					/**mui.openWindow({
						id: 'partinfo',
						url: 'partinfo.html'
					});**/
				})
			}(mui, document));
           
			
		</script>
	</body>

</html>