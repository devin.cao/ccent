<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html class="ui-page-login">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>安装工单信息</title>
<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
<style>
.wrap{
    margin: 0 auto;
    overflow: hidden;   /*用overflow: hidden 到容器里的元素*/
}
.sideleft{                /*设置左边通栏*/
    width: 30%;
    float: left;
    text-align: left;
}

.sideright{                /*设置左边通栏*/
    width: 70%;
    float: left;
    text-align: left;
    word-break: break-all;
}

.wrap{
    display: inline-block;
	display: block;
}
.sideleft, .sideright{
    padding-bottom: 32767px !important;  /*应用 padding-bottom（足够大的值）到列的块元素*/
    margin-bottom: -32767px !important;  /*应用 margin-bottom（足够大的值）到列的块元素*/
}
@media all and (min-width: 0px){
    .sideleft, .sideright{
        padding-bottom: 0 !important;
        margin-bottom: 0 !important; 
    }
    .sideleft:before, .sideright:before{
        content: '[DO NOT LEAVE IT IS NOT REAL]';
        display: block;
        background: inherit;
        padding-top: 32767px !important;
        margin-bottom: -32767px !important;
        height: 0;

    }

}
</style>

</head>

<body>
	<header class="mui-bar mui-bar-nav">
		<h1 class="mui-title">安装工单信息</h1>
		<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		<span class="mui-icon mui-icon-left-nav"></span>返回</button>
	</header>
	<div class="mui-content">
		<ul class="mui-table-view">
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">用户名称:</div><div class="sideright">${ item.cons_name}</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">用户手机:</div><div class="sideright">${ item.cons_phonenumber}</div></div>
			</li>
			<li class="mui-table-view-cell"><div class="wrap"><div class="sideleft">用户来源:</div><div class="sideright">
			<c:if test="${ item.cons_source eq 1}">专卖店</c:if>
			<c:if test="${ item.cons_source eq 2}">高铁广告</c:if>
			<c:if test="${ item.cons_source eq 3}">机场广告</c:if>
			<c:if test="${ item.cons_source eq 4}">央视广告</c:if>
			<c:if test="${ item.cons_source eq 5}">网站</c:if>
			<c:if test="${ item.cons_source eq 6}">卖场</c:if>
			<c:if test="${ item.cons_source eq 7}">建材城</c:if>
			</div></div>
			</li>
			<li class="mui-table-view-cell"><div class="wrap"><div class="sideleft">用户生日:</div><div class="sideright">
					<c:set var="birthdayStr" value="${ item.cons_birthday}"/>
						<c:choose>  
                         <c:when test="${fn:length(birthdayStr) > 10}">  
                           <c:out value="${fn:substring(birthdayStr, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${birthdayStr}" />  
                     </c:otherwise>  
                    </c:choose>
		
					</div></div>
					</li>
					<li class="mui-table-view-cell"><div class="wrap"><div class="sideleft">用户性别:</div><div class="sideright">
			<c:if test="${ item.cons_gender eq 1}">男</c:if>
			<c:if test="${ item.cons_gender eq 2}">女</c:if>
			</div></div>
			</li>
			<li class="mui-table-view-cell"><div class="wrap"><div class="sideleft">省:</div><div class="sideright">${ item.cons_province}</div></div>
			</li>
			<li class="mui-table-view-cell"><div class="wrap"><div class="sideleft">市:</div><div class="sideright">${ item.cons_city}</div></div>
			</li>
			<li class="mui-table-view-cell"><div class="wrap"><div class="sideleft">县:</div><div class="sideright">${ item.cons_county}</div></div>
			</li>
			
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">详细地址:</div><div class="sideright">${ item.cons_address}</div></div>
			</li>
			<c:forEach items ="${item.consumer_body}" var="item1" varStatus="s">
                <li class="mui-table-view-cell">
						<a class="a_table" href="#">产品${s.index+1}</a>
				</li>
				<li class="mui-table-view-cell">
					<div class="wrap"><div class="sideleft">产品类别:</div><div class="sideright">
							${ item1.cons_productcategory}
					</div></div>
				</li>
				<li class="mui-table-view-cell">
						<div class="wrap"><div class="sideleft">产品选项:</div><div class="sideright">
							 <c:if test="${ item1.cons_productoption eq 1}">带二维码产品</c:if>
							 <c:if test="${ item1.cons_productoption eq 2}">不带二维码产品</c:if>
						</div></div>
				</li>

				<c:if test="${(item1.cons_productoption eq 1)&&(item1.cons_productcategory eq '林志颖S1款')}">
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">机头编号:</div><div class="sideright">${ item1.cons_headsn}</div></div>
				</li>
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">机身编号:</div><div class="sideright">${ item1.cons_bodysn}</div></div>
				</li>
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">设备号:</div><div class="sideright">${ item1.cons_screenwificode}</div></div>
				</li>
				</c:if>
				<c:if test="${(item1.cons_productoption eq 1)&&(item1.cons_productcategory eq '集成灶'||
							item1.cons_productcategory eq '集成电烤箱')}">
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">机头编号:</div><div class="sideright">${ item1.cons_headsn}</div></div>
				</li>
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">机身编号:</div><div class="sideright">${ item1.cons_bodysn}</div></div>
				</li>

				</c:if>

				<c:if test="${item1.cons_productcategory eq '油烟机'||
							item1.cons_productcategory eq '燃气灶'||
							item1.cons_productcategory eq '消毒柜'||
							item1.cons_productcategory eq '电蒸箱'||
							item1.cons_productcategory eq '电烤箱'||
							item1.cons_productcategory eq '水槽'||
							item1.cons_productcategory eq '集成水槽'||
							item1.cons_productcategory eq '洗碗机'||
							item1.cons_productcategory eq '集成洗碗机'||
							item1.cons_productcategory eq '净水机'}">
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">机身编号:</div><div class="sideright">${ item1.cons_bodysn}</div></div>
				</li>
				 </c:if>
				<li class="mui-table-view-cell">
				<div class="wrap"><div class="sideleft">产品型号:</div><div class="sideright">${ item1.cons_producttype}</div></div>
				</li>
				<c:if test="!${item1.cons_manufacturedate} eq ''">
				<li class="mui-table-view-cell">
					<div class="wrap"><div class="sideleft">生产日期:</div>
						<div class="sideright">
							<c:set var="cons_manufacturedate" value="${item1.cons_manufacturedate}"/>
							<c:choose>
								<c:when test="${fn:length(cons_manufacturedate) > 10}">
									<c:out value="${fn:substring(cons_manufacturedate, 0, 10)}" />
								</c:when>
								<c:otherwise>
									<c:out value="${cons_manufacturedate}" />
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</li>
				</c:if>
			</c:forEach>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">购买地点:</div><div class="sideright">${ item.cons_purchaseplace}</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">购买日期:</div>
			<div class="sideright">
			<c:set var="cons_purchasedateStr" value="${ item.cons_purchasedate}"/>
						<c:choose>  
                         <c:when test="${fn:length(cons_purchasedateStr) > 10}">  
                           <c:out value="${fn:substring(cons_purchasedateStr, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${cons_purchasedateStr}" />  
                     </c:otherwise>  
                    </c:choose>
			</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">安装日期:</div><div class="sideright">
			<c:set var="cons_installationdateStr" value="${ item.cons_installationdate}"/>
						<c:choose>  
                         <c:when test="${fn:length(cons_installationdateStr) > 10}">  
                           <c:out value="${fn:substring(cons_installationdateStr, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${cons_installationdateStr}" />  
                     </c:otherwise>  
                    </c:choose>
			</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap">
			  <div class="sideleft">安装人姓名:</div><div class="sideright">${ item.cons_Installationname}</div></div>
			</li>
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">安装人手机:</div><div class="sideright">${ item.cons_Installationtel}</div></div>
			</li>
			
			<li class="mui-table-view-cell">
			<div class="wrap"><div class="sideleft">代理商名称:</div><div class="sideright">${ item.cons_company}</div></div>
			</li>
			
			 <!-- 			
			<li class="mui-table-view-cell"><label>生产日期:</label>&nbsp;&nbsp;&nbsp;<label>
			<c:set var="cons_manufacturedateStr" value="${ item.cons_manufacturedate}"/>
						<c:choose>  
                         <c:when test="${fn:length(cons_manufacturedateStr) > 10}">  
                           <c:out value="${fn:substring(cons_manufacturedateStr, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${cons_manufacturedateStr}" />  
                     </c:otherwise>  
                    </c:choose>
			</label> 
			</li>-->
		</ul>
	</div>
	<script src="<c:url value='/js/mui.min.js' />"></script>
	<script src="<c:url value='/js/mui.enterfocus.js' />"></script>
	<script src="<c:url value='/js/app.js' />"></script>
	<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/consumer/list'/>";
				}
			}(mui, document));
		</script>
</body>

</html>