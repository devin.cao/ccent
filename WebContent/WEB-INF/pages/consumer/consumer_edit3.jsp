<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html class="ui-page-login">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>新增安装工单信息</title>
<jsp:include page="../share/commoncss.jsp"></jsp:include>
<style>
.area {
	margin: 20px auto 0px auto;
}

.mui-input-group:first-child {
	margin-top: 20px;
}

.mui-input-group label {
	width: 40%;
}

.mui-input-row label ~input, .mui-input-row label ~select,
	.mui-input-row label ~textarea {
	width: 60%;
}

.mui-checkbox input[type=checkbox], .mui-radio input[type=radio] {
	top: 6px;
}

.mui-content-padded {
	margin-top: 25px;
}

.mui-btn {
	padding: 10px;
}

.mui-input-row label ~input, .mui-input-row label ~select,
	.mui-input-row label ~textarea{
	margin-top: 1px;
}

#cons_headsn {
	width: 47%;
	float: left;
}

#cons_bodysn {
	width: 47%;
	float: left;
}

#cons_screenwificode {
	width: calc(50% - 35px);
	float: left;
}

.scanbt {
	float: right;
	width: 30px;
	height: 30px;
	margin-top: 5px;
	margin-right: 5px;
}

#cons_address {
	margin-top: 5px;
	margin-bottom: 5px;
	padding-right: 8px;
	border: 1px solid rgba(0, 0, 0, .2);
}

#cons_phonecode {
	width: calc(50% - 53px);
	float: left;
}

#codebtn{
	float: right;
	width: 82px;
	height: 30px;
	margin-top: 5px;
	margin-right: 2px;
}
</style>
</head>

<body>
	<header class="mui-bar mui-bar-nav">

		<h1 class="mui-title">新增安装工单信息</h1>
		<button
			class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
			<span class="mui-icon mui-icon-left-nav"></span>返回
		</button>
	</header>
	<div class="mui-content">
		<section id="preview"></section>
		<form class="mui-input-group" id="consumerform" name="getpwdform"
			method="post" action="<c:url value='/consumer/doEdit'/>">
			<div class="mui-input-row">
				<label>用户姓名:</label> <input id='cons_name' name="cons_name"
					type="text" class="mui-input-clear mui-input" placeholder="请输入用户姓名">
			</div>
			<div class="mui-input-row">
				<label>用户手机:</label> <input id='cons_phonenumber' type="number"
					name="cons_phonenumber" class="mui-input-clear mui-input"
					placeholder="请输入用户手机">
			</div>
			<div class="mui-input-row">
				<label>验证码:</label> <input id='cons_phonecode' type="text"
					name="cons_phonecode" class="mui-input"
					placeholder="请输入验证码"> <input type="button" id="codebtn"
					value="获取验证码" style="color: blue;"  />
			</div>
			<div class="mui-input-row">
				<label>用户来源:</label> <select id='cons_source' name="cons_source">
					<option value="">请选择用户来源</option>
					<option value="1">专卖店</option>
					<option value="2">高铁广告</option>
					<option value="3">机场广告</option>
					<option value="4">央视广告</option>
					<option value="5">网站</option>
					<option value="6">卖场</option>
					<option value="7">建材城</option>
				</select>

			</div>
			<div class="mui-input-row">
				<label>用户生日:</label> <input id='cons_birthday' name="cons_birthday"
					type="text" readonly class="mui-input-clear mui-input"
					placeholder="请输入用户生日">
			</div>
			<div class="mui-input-row">
				<label>用户性别:</label> <select class="form-control" id="cons_gender">
					<option value="">请选择性别</option>
					<option value="1">男</option>
					<option value="2">女</option>
				</select>
			</div>
			<div data-toggle="distpicker">
				<div class="mui-input-row">
					<label>省:</label> <select class="form-control" id="cons_province">
					</select>

				</div>
				<div class="mui-input-row">
					<label>市:</label> <select class="form-control" id="cons_city"></select>
				</div>
				<div class="mui-input-row">
					<label>区/县 :</label> <select class="form-control" id="cons_county"></select>
				</div>
			</div>
			<div class="mui-input-row" id="addressdiv" style="height: 113px;">
				<label>详细地址:</label>
				<textarea id='cons_address' name="cons_address" rows="3" cols="2"
					placeholder="请输入详细地址"></textarea>
			</div>

			<div class="mui-input-row">
				<label>产品类别:</label> <select id='cons_productcategory'
					name="cons_productcategory" onchange="productCategoryChange()">
					
					<option value="林志颖S1款">林志颖S1款</option>
					<!--  
					
					<option value="">请选择产品类别</option>
					<option value="集成灶">集成灶</option>
					<option value="油烟机">油烟机</option>
					<option value="燃气灶">燃气灶</option>
					<option value="消毒柜">消毒柜</option>
					<option value="电蒸箱">电蒸箱</option>
					
					<option value="电烤箱">电烤箱</option>
					<option value="集成电烤箱">集成电烤箱</option>
					<option value="水槽">水槽</option>
					<option value="集成水槽">集成水槽</option>
					<option value="洗碗机">洗碗机</option>
					<option value="集成洗碗机">集成洗碗机</option>
					<option value="净水机">净水机</option>-->
				</select>
			</div>
			<div class="mui-input-row">
				<label>产品选项:</label> <select id='cons_productoption'
					name="cons_productoption" onchange="productoptionChange()">
					<option value="1">带二维码产品</option>
					<!--  如果开放其他产品类别需要打开此注解
					<option value="2">不带二维码产品</option>-->
				</select>
			</div>
			<div class="mui-input-row" id="dv_headsn">
				<label>机头编号:</label> <input id='cons_headsn' readonly="readonly"
					name="cons_headsn" type="text"
					class="iconfont icon-saomiao  mui-input-clear"
					placeholder="请扫描机头编号"> <img id="cons_headsn_bt"
					class="scanbt" alt="" src="<c:url value='/img/scan.png'/>">
			</div>
			<div class="mui-input-row" id="dv_bodysn">
				<label>机身编号:</label> <input id='cons_bodysn' readonly="readonly"
					name="cons_bodysn" type="text" class="mui-input-clear mui-input"
					placeholder="请扫描机身编号"> <img id="cons_bodysn_bt"
					class="scanbt" alt="" src="<c:url value='/img/scan.png'/>">

			</div>
			<div class="mui-input-row" id="dv_screenwificode">
				<label>设备号:</label> <input id='cons_screenwificode'
					readonly="readonly" name="cons_screenwificode" type="text"
					class="mui-input-clear mui-input" placeholder="请扫描设备号"> <img
					id="screenwificode_bt" class="scanbt" alt=""
					src="<c:url value='/img/scan.png'/>">

			</div>
			<div class="mui-input-row">
				<label>购买地点:</label> <input id='cons_purchaseplace'
					name="cons_purchaseplace" type="text"
					class="mui-input-clear mui-input" placeholder="请输入购买地点">
			</div>
			<div class="mui-input-row">
				<label>购买日期:</label> <input id='cons_purchasedate'
					name="cons_purchasedate" type="text"
					class="mui-input-clear mui-input" placeholder="请输入购买日期">
			</div>
			<div class="mui-input-row">
				<label>安装日期 :</label> <input id='cons_installationdate'
					readonly="readonly" name="cons_installationdate" type="text"
					value="${cons_installationdate}" class="mui-input"
					placeholder="请输入安装日期 ">
			</div>
			<div class="mui-input-row">
				<label>安装人姓名:</label> <input id='cons_Installationname'
					name="cons_Installationname" type="text"
					class="mui-input-clear mui-input" placeholder="请输入安装人姓名">
			</div>
			<div class="mui-input-row">
				<label>安装人手机:</label> <input id='cons_Installationtel'
					name="cons_Installationtel" type="number"
					class="mui-input-clear mui-input" placeholder="请输入安装人手机">
			</div>


			<!--  
				<div class="mui-input-row">
					<label>生产日期:</label>
					<input id='cons_manufacturedate' name="cons_manufacturedate" type="text" class="mui-input-clear mui-input" placeholder="请输入生产日期">
				</div>
				-->
		</form>
		<div class="mui-content-padded">
			<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">提交</button>
		</div>
	</div>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<jsp:include page="../share/commonjs.jsp" />
	<script src="<c:url value='/js/mui.picker.min.js' />"></script>
	<script src="<c:url value='/js/distpicker.data.js' />"></script>
	<script src="<c:url value='/js/distpicker.js' />"></script>

	<script>
		  
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/consumer/list'/>";
				}
				var appId='${appId}';
				var timestamp='${timestamp}';
				var nonceStr='${nonceStr}';
				var signature='${signature}';
				wx.config({
				    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				    appId: appId, // 必填，公众号的唯一标识
				    timestamp: timestamp, // 必填，生成签名的时间戳
				    nonceStr: nonceStr, // 必填，生成签名的随机串
				    signature: signature,// 必填，签名，见附录1
				    jsApiList: ['scanQRCode'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});
				//code validate
				var countdown=60; 
				var codebtn = doc.getElementById('codebtn');
				codebtn.addEventListener('tap', function(event) {
				    var cons_phonenumber=doc.getElementById('cons_phonenumber').value;
					if(cons_phonenumber==''){
						//showTrip("用户手机不能为空");
						showTrip("用户手机不能为空");
						return ;
					}
					if(cons_phonenumber.length!=11){
						showTrip("用户手机长度为11位");
						return ;
					}
					var spinner= new Spinner();
					mui.ajax("<c:url value='/consumer/getCode'/>",{
						data:{
							phone:cons_phonenumber
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							codebtn.disabled=true;
						},
						complete: function() {
							spinner.spin();
							codebtn.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								settime(codebtn);
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					  });
					
				});
				var province=doc.getElementById("cons_province");
				//alert(province.selectedIndex);
				province.selectedIndex=-1;
				var cons_birthday = doc.getElementById('cons_birthday');
				cons_birthday.addEventListener('focus', function(event) {
					
					doc.activeElement.blur();
					var today = new Date();
					var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) }); 
					dtPicker.show(function(selectItems) {
						cons_birthday.value = selectItems.text;
						//dtPicker.dispose();
						dtPicker.hide();
					});
				}, false);
				var cons_purchasedate = doc.getElementById('cons_purchasedate');
				cons_purchasedate.addEventListener('focus', function(event) {
					doc.activeElement.blur(); 
					var today = new Date();
					var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) }); 
					dtPicker.show(function(selectItems) {
						cons_purchasedate.value = selectItems.text;
						//dtPicker.dispose();
						dtPicker.hide();
					});
				}, false);
				
				
				var cons_headsn_bt = doc.getElementById('cons_headsn_bt');
				cons_headsn_bt.addEventListener('tap', function(event) {
					wx.ready(function(){
						wx.scanQRCode({
						    needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
						    scanType: ["qrCode"], // 可以指定扫二维码还是一维码，默认二者都有
						    success: function (res) {
						    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
						    //alert(result);
						   
						    var cons_productcategory=doc.getElementById('cons_productcategory').value;
							if(cons_productcategory=='林志颖S1款'||
									cons_productcategory=='集成灶'||
									cons_productcategory=='集成电烤箱'){
								if(result.indexOf("T")<0){
									showTrip("机头编码不符合规则");
									document.getElementById('cons_headsn').value='';
									return ;
								}
								 
							}
							document.getElementById('cons_headsn').value=result;
						 }
						});
						
					});
				}, false);
				var cons_bodysn_bt = doc.getElementById('cons_bodysn_bt');
				cons_bodysn_bt.addEventListener('tap', function(event) {
					wx.ready(function(){
						wx.scanQRCode({
						    needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
						    scanType: ["qrCode"], // 可以指定扫二维码还是一维码，默认二者都有
						    success: function (res) {
						    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
						    //alert(result);
						    var cons_productcategory=doc.getElementById('cons_productcategory').value;
							if(cons_productcategory=='林志颖S1款'||
									cons_productcategory=='集成灶'||
									cons_productcategory=='集成电烤箱'){
								if(result.indexOf("G")<0){
									showTrip("机身编码不符合规则");
									document.getElementById('cons_bodysn').value='';
									return ;
								}
							}
						    document.getElementById('cons_bodysn').value=result;
						   
						 }
						});
						
					});
				}, false);
				var screenwificode_bt = doc.getElementById('screenwificode_bt');
				screenwificode_bt.addEventListener('tap', function(event) {
					wx.ready(function(){
						wx.scanQRCode({
						    needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
						    scanType: ["qrCode"], // 可以指定扫二维码还是一维码，默认二者都有
						    success: function (res) {
						    var result = res.resultStr; // 当needResult 为 1 时，扫码返回的结果
						    //alert(result);
						    document.getElementById('cons_screenwificode').value=result;
						   
						 }
						});
						
					});
				}, false);
				
			var saveButton = doc.getElementById('saveButton');
			saveButton.addEventListener('tap', function(event) {
				var cons_name=doc.getElementById('cons_name').value;
				var cons_phonenumber=doc.getElementById('cons_phonenumber').value;
				var cons_phonecode=doc.getElementById('cons_phonecode').value;
				
				//var cons_phonenumber=doc.getElementById('cons_phonenumber').value;
				var cons_source=doc.getElementById('cons_source').value;
				var cons_birthday=doc.getElementById('cons_birthday').value;
				var cons_gender=doc.getElementById('cons_gender').value;
				
				var cons_address=doc.getElementById('cons_address').value;
				var cons_productoption=doc.getElementById('cons_productoption').value;
				var cons_province=doc.getElementById('cons_province').value;
				var cons_city=doc.getElementById('cons_city').value;
				var cons_county=doc.getElementById('cons_county').value;
				var cons_purchaseplace=doc.getElementById('cons_purchaseplace').value;
				var cons_purchasedate=doc.getElementById('cons_purchasedate').value;
				var cons_installationdate=doc.getElementById('cons_installationdate').value;
				var cons_Installationname=doc.getElementById('cons_Installationname').value;
				var cons_Installationtel=doc.getElementById('cons_Installationtel').value;
				//var cons_company=doc.getElementById('cons_company').value;
				
				//var cons_manufacturedate=doc.getElementById('cons_manufacturedate').value;
				//doc.getElementById("consumerform").submit();
				if(cons_name==''){
					//showTrip("用户名称不能为空");
					 showTrip("用户名称不能为空");
					return ;
				}
				if(cons_phonenumber==''){
					//showTrip("用户手机不能为空");
					showTrip("用户手机不能为空");
					return ;
				}
				if(cons_phonenumber.length!=11){
					showTrip("用户手机长度为11位");
					return ;
				}
				if(cons_phonecode==''){
					//showTrip("用户手机不能为空");
					showTrip("验证码不能为空");
					return ;
				}
				if(cons_source==''){
					showTrip("请选择用户来源");
					//mui.toast('用户名不能为空');
					return ;
				}
				if(cons_birthday==''){
					showTrip("用户生日不能为空");
					return ;
				}
				if(cons_gender==''){
					showTrip("请选择性别");
					return ;
				}
				
				if(cons_province==''){
					showTrip("请选择省");
					return;
				}
				
				if(cons_city==''){
					showTrip("请选择市");
					return ;
				}
				if(cons_county==''){
					showTrip("请选择区/县");
					return ;
				}
				if(cons_address==''){
					showTrip("详细地址不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				//获取产品型号
				var cons_productcategory=doc.getElementById('cons_productcategory').value;
				if(cons_productcategory==''){
					showTrip("请选择产品类别");
					return ;
				}
				//var sngp =/^[A-Za-z0-9]+$/;
				var sngp = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
				if(cons_productcategory=='林志颖S1款'||
						cons_productcategory=='集成灶'||
						cons_productcategory=='集成电烤箱'){
					var cons_headsn=doc.getElementById('cons_headsn').value;
				    var cons_bodysn=doc.getElementById('cons_bodysn').value;
					var cons_productoption = document.getElementById('cons_productoption').value;
					if(cons_productoption==1){
						//有二维码机身、机头、设备号都需要
					    var cons_screenwificode=doc.getElementById('cons_screenwificode').value;
				        if(cons_headsn==''){
							showTrip("机头编号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
				       if(sngp.test(cons_headsn) ){
							showTrip("机头编号不能含有中文");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(cons_bodysn==''){
							showTrip("机身编号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
					    if(sngp.test(cons_bodysn) ){
								showTrip("机身编号不能含有中文");
								return ;
						}
						if(cons_screenwificode==''){
							showTrip("设备号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
					   if(sngp.test(cons_screenwificode) ){
							showTrip("设备号不能含有中文");
							return ;
					    }
					}else{
						//机身、机头需要
						
						if(cons_headsn==''){
							showTrip("机头编号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
					   if(sngp.test(cons_headsn) ){
							showTrip("机头编号不能含有中文");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(cons_bodysn==''){
							showTrip("机身编号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
						 if(sngp.test(cons_bodysn) ){
							showTrip("机身编号不能含有中文");
							return ;
					    }
					}
				}else if(cons_productcategory=='油烟机'||
						cons_productcategory=='燃气灶'||
						cons_productcategory=='消毒柜'||
						cons_productcategory=='电蒸箱'||
						cons_productcategory=='电烤箱'||
						cons_productcategory=='水槽'||
						cons_productcategory=='集成水槽'||
						cons_productcategory=='洗碗机'||
						cons_productcategory=='集成洗碗机'||
						cons_productcategory=='净水机'){
					//只有机身字段
					var cons_bodysn=doc.getElementById('cons_bodysn').value;
					if(cons_bodysn==''){
						showTrip("机身编号不能为空");
						//mui.toast('用户名不能为空');
						return ;
					}
				if(sngp.test(cons_bodysn) ){
						showTrip("机身编号不能含有中文");
						//mui.toast('用户名不能为空');
						return ;
					}
				}
				if(cons_purchaseplace==''){
					showTrip("购买地点不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				if(cons_purchasedate==''){
					showTrip("购买日期不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				if(cons_installationdate==''){
					showTrip("安装日期不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				//
				if(!DateDiff(cons_purchasedate,cons_installationdate)){
					showTrip("安装日期应在购买日期之后");
					return ;
				}
				if(cons_Installationname==''){
					showTrip("安装人姓名不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				var sngp =/^[\u0391-\uFFE5-A-Za-z]+$/; 
				 if(!sngp.test(cons_Installationname) ){
					 showTrip("安装人姓名只能是中英文");
						return ;
				}
				if(cons_Installationtel==''){
					showTrip("安装人员手机不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				
				if(cons_Installationtel.length!=11){
					showTrip("安装人员手机长度为11位");
					return ;
				}
				var spinner= new Spinner();
				mui.ajax("<c:url value='/consumer/doEdit'/>",{
					data:{
						cons_name:cons_name,
						cons_phonenumber:cons_phonenumber,
						cons_phonecode:cons_phonecode,
						cons_source:cons_source,
						cons_birthday:cons_birthday,
						cons_gender:cons_gender,
						cons_address:cons_address,
						cons_productoption:cons_productoption,
						cons_province:cons_province,
						cons_city:cons_city,
						cons_county:cons_county,
						cons_purchaseplace:cons_purchaseplace,
						cons_purchasedate:cons_purchasedate,
						cons_installationdate:cons_installationdate,
						cons_Installationname:cons_Installationname,
						cons_Installationtel:cons_Installationtel,
						cons_headsn:cons_headsn,
						cons_bodysn:cons_bodysn,
						cons_productcategory:cons_productcategory,
						cons_screenwificode:cons_screenwificode
					},
					dataType:'json',//服务器返回json格式数据
					type:'post',//HTTP请求类型
					headers:null,
					beforeSend: function() {
						spinner.spin(document.getElementById('preview'));
						saveButton.disabled=true;
					},
					complete: function() {
						spinner.spin();
						saveButton.disabled=false;
					},
					success:function(data){
						if(data.StatusCode==1){
							//成功
							//
							alert(data.Message);
							//mui.back();
							window.location.href="<c:url value='/consumer/list'/>";
						}else{
							alert(data.Message);
						}
						
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						console.log(type);
					}
				});
			});		
			}(mui, document));
			function DateDiff(d1,d2){    
			    start_at = new Date(d1.replace(/^(\d{4})(\d{2})(\d{2})$/,"$1/$2/$3"));
			    end_at = new Date(d2.replace(/^(\d{4})(\d{2})(\d{2})$/,"$1/$2/$3"));
			    if(start_at > end_at) {
			      return false;
			    }
			    return true;
			}
			function productoptionChange(){ 
				var cons_productoption = document.getElementById('cons_productoption').value;
				var screenwificode_bt=document.getElementById("screenwificode_bt");
				var cons_bodysn_bt=document.getElementById("cons_bodysn_bt");
				var cons_headsn_bt=document.getElementById("cons_headsn_bt");
				var cons_headsn=document.getElementById("cons_headsn");
				var cons_bodysn=document.getElementById("cons_bodysn");
				var cons_screenwificode=document.getElementById("cons_screenwificode");
				//
				var cons_productcategory = document.getElementById('cons_productcategory').value;
				var dv_headsn=document.getElementById("dv_headsn");
				var dv_bodysn=document.getElementById("dv_bodysn");
				var dv_screenwificode=document.getElementById("dv_screenwificode");
				//alert(dv_screenwificode);
				if(cons_productoption==1){
					//HAS QRCODE
					screenwificode_bt.style.visibility ="visible";
					cons_bodysn_bt.style.visibility ="visible";
					cons_headsn_bt.style.visibility ="visible";
					cons_headsn.setAttribute("readOnly",'true');
					cons_bodysn.setAttribute("readOnly",'true');
					cons_screenwificode.setAttribute("readOnly",'true');
					
					cons_headsn.setAttribute("placeholder","请扫描机头编号");
					cons_bodysn.setAttribute("placeholder","请扫描机身编号");
					cons_screenwificode.setAttribute("placeholder","请扫描设备号");
					
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电烤箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="block";
					}else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}
				}else{
					screenwificode_bt.style.visibility ="hidden";
					cons_bodysn_bt.style.visibility ="hidden";
					cons_headsn_bt.style.visibility ="hidden";
					cons_headsn.removeAttribute("readOnly");
					cons_bodysn.removeAttribute("readOnly");
					cons_screenwificode.removeAttribute("readOnly");
					cons_headsn.setAttribute("placeholder","请输入机头编号");
					cons_bodysn.setAttribute("placeholder","请输入机身编号");
					cons_screenwificode.setAttribute("placeholder","请输入设备号");
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电烤箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}
				}
				
			}; 
			function changeOption(type){
				   document.getElementById("cons_productoption").options.length=0;
				   if(type==1){
					   $("#cons_productoption").append("<option value='1'>带二维码产品</option>");
				   }else{
					   $("#cons_productoption").append("<option value='1'>带二维码产品</option>"); 
					   $("#cons_productoption").append("<option value='2'>不带二维码产品</option>"); 
				   }
				   productoptionChange();
			}
			
			function productCategoryChange(){
				var cons_productcategory = document.getElementById('cons_productcategory').value;
				var dv_headsn=document.getElementById("dv_headsn");
				var dv_bodysn=document.getElementById("dv_bodysn");
				var dv_screenwificode=document.getElementById("dv_screenwificode");
				
				if(cons_productcategory=='林志颖S1款'||
						cons_productcategory=='集成灶'||
						cons_productcategory=='集成电烤箱'){
					//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
					dv_headsn.style.display ="block";
					dv_bodysn.style.display ="block";
					changeOption(1);
					var cons_productoption = document.getElementById('cons_productoption').value;
					if(cons_productoption==1&cons_productcategory=='林志颖S1款'){
						//有二维码
						dv_screenwificode.style.display ="block";
					}else{
						dv_screenwificode.style.display ="none";
					}
				
				}else if(cons_productcategory=='油烟机'||
						cons_productcategory=='燃气灶'||
						cons_productcategory=='消毒柜'||
						cons_productcategory=='电蒸箱'||
						cons_productcategory=='电烤箱'||
						cons_productcategory=='水槽'||
						cons_productcategory=='集成水槽'||
						cons_productcategory=='洗碗机'||
						cons_productcategory=='集成洗碗机'||
						cons_productcategory=='净水机'){
					//只有机身字段
					changeOption(2);
					dv_headsn.style.display ="none";
					dv_bodysn.style.display ="block";
					dv_screenwificode.style.display ="none";
				}
				
			}
			
		</script>
	<script type="text/javascript"> 
var countdown=60; 
function settime(obj) { 
    if (countdown == 0) { 
        obj.removeAttribute("disabled");    
        obj.value="获取验证码"; 
        countdown = 60; 
        return;
    } else { 
        obj.setAttribute("disabled", true); 
        obj.value="重新获取(" + countdown + ")"; 
        countdown--; 
    } 
setTimeout(function() { 
    settime(obj) }
    ,1000) 
}
  
</script>
</body>

</html>