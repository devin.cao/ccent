<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html class="ui-page-login">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title>新增安装工单信息</title>
<jsp:include page="../share/commoncss.jsp"></jsp:include>
<style>
.area {
	margin: 20px auto 0px auto;
}

.mui-input-group:first-child {
	margin-top: 20px;
}
@media ( min-width :320px) {
	     .mui-input-group .itemlabel {
				width: 35%;
				float: left;
			}
			.mui-input-row .iteminput{
				width: 65%;
			}
}
@media ( min-width :360px) {
	      .mui-input-group .itemlabel  {
				width: 31%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 69%;
			}
}
@media ( min-width :768px) {
	      .mui-input-group .itemlabel  {
				width: 15%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 85%;
			}
}
@media ( min-width :800px) {
	      .mui-input-group .itemlabel  {
				width: 14%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 86%;
			}
}
@media ( min-width :980px) {
	      .mui-input-group .itemlabel  {
				width: 12%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 88%;
			}
}
@media ( min-width :1080px) {
	      .mui-input-group .itemlabel  {
				width: 11%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 89%;
			}
}
@media ( min-width :1280px) {
	      .mui-input-group .itemlabel  {
				width: 9%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 91%;
			}
}
@media ( min-width :1920px) {
	      .mui-input-group .itemlabel  {
				width: 6%;
				float: left;
			}
			.mui-input-row .iteminput {
				width: 94%;
			}
}


.cons_headsn {
	width: 50% !important;
	float: left !important;
}

.cons_bodysn {
	width: 50% !important;
	float: left !important;
}

.cons_screenwificode {
	width: 50% !important;
	float: left !important;
}

.mui-checkbox input[type=checkbox], .mui-radio input[type=radio] {
	top: 6px;
}

.mui-content-padded {
	margin-top: 25px;
}

.mui-btn {
	padding: 10px;
}

.mui-input-row label ~input, .mui-input-row label ~select,
	.mui-input-row label ~textarea{
	margin-top: 1px;
}

.scanbt {
	float: right;
	width: 30px;
	height: 30px;
	margin-top: 5px;
	margin-right: 15px;
}
.add_remove {
	float: right;
	width: 25px;
	height: 25px;
	margin-right: 11px;

}
.add_img {
	float: right;
	width: 25px;
	height: 25px;
	margin-right: 11px;

}
.a_table {
	display:block;/*//将a设置为块级元素*/
	float:left;
	width:68px;
}

#cons_address {
	margin-top: 5px;
	margin-bottom: 5px;
	padding-right: 8px;
	border: 1px solid rgba(0, 0, 0, .2);
}

#cons_phonecode {
	width: calc(50% - 53px);
	float: left;
}

#codebtn{
	float: right;
	width: 82px;
	height: 30px;
	margin-top: 5px;
	margin-right: 2px;
}
.content1{
	padding: 0 10px;
	margin-top: 5px;
	overflow: hidden;
}


</style>
</head>

<body>
	<header class="mui-bar mui-bar-nav">

		<h1 class="mui-title">新增安装工单信息</h1>
		<button
			class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
			<span class="mui-icon mui-icon-left-nav"></span>返回
		</button>
	</header>
	<div class="mui-content">
		<section id="preview"></section>
		<form class="mui-input-group" id="consumerform" name="getpwdform"
			method="post" action="<c:url value='/consumer/doEditMultiple'/>">
			<div class="mui-input-row">
				<label class="itemlabel">用户姓名:</label>
				 <input class="iteminput" id='cons_name' name="cons_name"
					type="text" class="mui-input-clear mui-input" placeholder="请输入用户姓名">
			</div>
			<div class="mui-input-row">
				<label class="itemlabel">用户手机:</label> <input class="iteminput" id='cons_phonenumber' type="number"
					name="cons_phonenumber" class="mui-input-clear mui-input"
					placeholder="请输入用户手机">
			</div>

			<div class="mui-input-row">
				<label class="itemlabel">用户验证码:</label> <input class="iteminput" id='cons_phonecode' type="text"
															   name="cons_phonecode" class="mui-input"
															   placeholder="请输入验证码"> <input type="button" id="codebtn"
																							value="获取验证码" style="color: blue; "  />
			</div>

			<div class="mui-input-row">
				<label class="itemlabel">用户来源:</label> <select class="iteminput" id='cons_source' name="cons_source">
					<option value="">请选择用户来源</option>
					<option value="1">专卖店</option>
					<option value="2">高铁广告</option>
					<option value="3">机场广告</option>
					<option value="4">央视广告</option>
					<option value="5">网站</option>
					<option value="6">卖场</option>
					<option value="7">建材城</option>
				</select>

			</div>
			<div class="mui-input-row">
				<label class="itemlabel">用户生日:</label> <input class="iteminput" id='cons_birthday' name="cons_birthday"
					type="text" readonly class="mui-input-clear mui-input"
					placeholder="请输入用户生日">
			</div>
			<div class="mui-input-row">
				<label class="itemlabel">用户性别:</label> <select class="iteminput" class="form-control" id="cons_gender">
					<option value="">请选择性别</option>
					<option value="1">男</option>
					<option value="2">女</option>
				</select>
			</div>
			<div data-toggle="distpicker">
				<div class="mui-input-row">
					<label class="itemlabel">省:</label> <select class="iteminput" class="form-control" id="cons_province"  data-province=""  onchange="changeAdd(this.value,1)">
					</select>

				</div>
				<div class="mui-input-row">
					<label class="itemlabel">市:</label> <select class="iteminput" class="form-control" id="cons_city"  data-province=""  onchange="changeAdd(this.value,2)"></select>
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">区/县 :</label> <select class="iteminput" class="form-control" id="cons_county"  data-province=""  onchange="changeAdd(this.value,3)"></select>
				</div>
			</div>
			<div class="mui-input-row" id="addressdiv" style="height: 113px;">
				<label class="itemlabel">详细地址:</label>
				<textarea class="iteminput" id='cons_address' name="cons_address" rows="3" cols="2"
					placeholder="请输入详细地址"></textarea>
			</div>
			<li class="" style="list-style-type:none "  id="prod1" >

				<div class="content1">
					<a class="a_table" href="#">产品1</a>
					<img id="remove_prod_img1"class="add_remove" alt="" onclick="remove(this.id)" src="<c:url value='/img/remove.png'/>">
				</div>
				<div class="mui-input-row mui-page">
					<label class="itemlabel">产品类别:</label> <select class="iteminput" id='cons_productcategory1'
																   name="cons_productcategory" onchange="productCategoryChangeClass(this.id)">
					<option value="">请选择产品类别</option>
					<option value="林志颖S1款">林志颖S1款</option>
					<option value="集成灶">集成灶</option>
					<option value="油烟机">油烟机</option>
					<option value="燃气灶">燃气灶</option>
					<option value="消毒柜">消毒柜</option>
					<option value="电蒸箱">电蒸箱</option>

					<option value="电烤箱">电烤箱</option>
					<option value="集成电蒸箱">集成电蒸箱</option>
					<option value="集成电烤箱">集成电烤箱</option>
					<option value="水槽">水槽</option>
					<option value="集成水槽">集成水槽</option>
					<option value="洗碗机">洗碗机</option>
					<option value="集成洗碗机">集成洗碗机</option>
					<option value="净水机">净水机</option>
				</select>
				</div>
				<div class="mui-input-row mui-page">
					<label class="itemlabel">产品选项:</label> <select class="iteminput  cons_productoption_iteminput" id='cons_productoption1'
																   name="cons_productoption" onchange="productoptionChange(this.id)">
					<option value="1">带二维码产品</option>
					<!--  如果开放其他产品类别需要打开此注解-->
					<option value="2">不带二维码产品</option>
				</select>
				</div>
				<div class="mui-input-row mui-page" id="dv_headsn1">
					<label class="itemlabel">机头编号:</label> <input  id='cons_headsn1' readonly="readonly"
																   name="cons_headsn" type="text"
																   class="cons_headsn  "
																   placeholder="请扫描机头编号"> <img id="cons_headsn_bt1" onclick="scan(this.id)"
																							   class="scanbt" alt="" src="<c:url value='/img/scan.png'/>">
				</div>
				<div class="mui-input-row mui-page" id="dv_bodysn1">
					<label class="itemlabel">机身编号:</label> <input id='cons_bodysn1' readonly="readonly"
																  name="cons_bodysn" type="text" class="cons_bodysn"
																  placeholder="请扫描机身编号"> <img id="cons_bodysn_bt1" onclick="scan(this.id)"
																							  class="scanbt" alt="" src="<c:url value='/img/scan.png'/>">

				</div>
				<div class="mui-input-row mui-page" id="dv_screenwificode1">
					<label class="itemlabel">设备号:</label> <input  id='cons_screenwificode1'
																  readonly="readonly" name="cons_screenwificode" type="text"
																  class="cons_screenwificode" placeholder="请扫描设备号"> <img
						id="cons_screenwificode_bt1" onclick="scan(this.id)" class="scanbt" alt=""
						src="<c:url value='/img/scan.png'/>">

				</div>
				<div class="mui-input-row mui-page" id="dv_producttype1">
					<label class="itemlabel">产品型号:</label> <input class="iteminput" id='cons_producttype1'
																  name="cons_producttype" type="text"
																  class="mui-input-clear mui-input" placeholder="请输入产品型号">
				</div>
				<div class="mui-input-row mui-page" id="dv_manufacturedate1">
					<label class="itemlabel">生产日期:</label>
					<input class="iteminput" id='cons_manufacturedate1' onclick="time(this.id)" name="cons_manufacturedate" type="text"  class="mui-input-clear mui-input " placeholder="请输入生产日期">
				</div>
			</li>

			<li  class="mui-table-view-cell mui-collapse" id="add_prod" style="list-style-type:none">新增产品
				<img id="add_prod_img"class="add_img" alt="" src="<c:url value='/img/plus.png'/>">
			</li>

			<div class="mui-input-row">
				<label class="itemlabel">购买地点:</label> <input class="iteminput" id='cons_purchaseplace'
					name="cons_purchaseplace" type="text"
					class="mui-input-clear mui-input" placeholder="请输入购买地点">
			</div>
			<div class="mui-input-row">
				<label class="itemlabel">购买日期:</label> <input class="iteminput" id='cons_purchasedate'
					name="cons_purchasedate" type="text"
					class="mui-input-clear mui-input" placeholder="请输入购买日期">
			</div>
			<div class="mui-input-row">
				<label class="itemlabel">安装日期 :</label> <input class="iteminput" id='cons_installationdate'
					readonly="readonly" name="cons_installationdate" type="text"
					value="${cons_installationdate}" class="mui-input"
					placeholder="请输入安装日期 ">
			</div>
			<div class="mui-input-row">
				<label class="itemlabel">安装人姓名:</label> <input class="iteminput" id='cons_Installationname'
					name="cons_Installationname" type="text"
					class="mui-input-clear mui-input" placeholder="请输入安装人姓名">
			</div>
			<div class="mui-input-row">
				<label class="itemlabel">安装人手机:</label> <input class="iteminput" id='cons_Installationtel'
					name="cons_Installationtel" type="number"
					class="mui-input-clear mui-input" placeholder="请输入安装人手机">
			</div>

			<!--
				<div class="mui-input-row">
					<label>生产日期:</label>
					<input id='cons_manufacturedate' name="cons_manufacturedate" type="text" class="mui-input-clear mui-input" placeholder="请输入生产日期">
				</div>
				-->
		</form>
		<div class="mui-content-padded">
			<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">提交</button>
		</div>
	</div>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<jsp:include page="../share/commonjs.jsp" />
	<script src="<c:url value='/js/mui.picker.min.js' />"></script>
	<script src="<c:url value='/js/distpicker.data.js' />"></script>
	<script src="<c:url value='/js/distpicker.js' />"></script>

	<script>
			var add_prod=document.getElementById('add_prod');
			var elementsByName = document.getElementsByName("cons_productcategory");
			var num = 1;
			var cons_manufacturedate = null;

			add_prod.addEventListener('tap', function(event) {
				var count1  = 0;
				for (var i = 0; i < elementsByName.length; i++) {
					var num1 = elementsByName[i].id.substring(20);
					if (num1 > count1){
						count1 = num1;
					}
				}
				//alert(num);//最大的id
				num = count1;
				num ++;
				var prod=document.getElementById('prod'+(num-1));

				var cons_productcategory=document.getElementById('cons_productcategory'+(num-1)).value;
				cons_manufacturedate=document.getElementById('cons_manufacturedate'+(num-1)).value;
				var cons_producttype=document.getElementById('cons_producttype'+(num-1)).value;
				if (cons_productcategory==""){
					showTrip("产品类别不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				else{
					var sngp = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
					if(cons_productcategory=='林志颖S1款'||cons_productcategory=='集成灶'||	cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱')
					{
						var cons_headsn=document.getElementById('cons_headsn'+(num-1)).value;
						var cons_bodysn=document.getElementById('cons_bodysn'+(num-1)).value;
						var cons_productoption = document.getElementById('cons_productoption'+(num-1)).value;
						if(cons_productoption==1){
							//有二维码机身、机头、设备号都需要
							var cons_screenwificode=document.getElementById('cons_screenwificode'+(num-1)).value;
							if(cons_headsn==''){
								showTrip("机头编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_headsn) ){
								showTrip("机头编号不能含有中文");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(cons_bodysn==''){
								showTrip("机身编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_bodysn) ){
								showTrip("机身编号不能含有中文");
								return ;
							}
							if(cons_productcategory=='林志颖S1款'){

								if(cons_screenwificode==''){
									showTrip("设备号不能为空");
									//mui.toast('用户名不能为空');
									return ;
								}
								if(sngp.test(cons_screenwificode) ){
									showTrip("设备号不能含有中文");
									return ;
								}
							}
						}else{
							//机身、机头需要

							if(cons_headsn==''){
								showTrip("机头编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_headsn) ){
								showTrip("机头编号不能含有中文");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(cons_bodysn==''){
								showTrip("机身编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_bodysn) ){
								showTrip("机身编号不能含有中文");
								return ;
							}
						}
					}
					else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						var cons_bodysn=document.getElementById('cons_bodysn'+(num-1)).value;
						if(cons_bodysn==''){
							showTrip("机身编号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(sngp.test(cons_bodysn) ){
							showTrip("机身编号不能含有中文");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(cons_producttype==''){
							showTrip("产品型号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(cons_manufacturedate==''){
							showTrip("生产日期不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}

					}


				}

				var div_data = '';
				if (num<=10) {
					var div_data = document.createElement("div");
					div_data.innerHTML = div_elem(num);

					/*for(var i = 2; i<num; i++){
						div_data += div_elem(i);
					}*/
				}else{
					alert('不能再增加了');
					return;
				}
				//prod.innerHTML = div_data;
				prod.after(div_data);
			}, false);

			function remove(id) {
				var count = id.substring(15);
				var prod=document.getElementById('prod'+count);
				if (elementsByName.length >1){
					prod.remove();
					num--;
				}else {
					alert('必须选择一个产品');
				}

			}

			function div_elem(i){
				return '<li class="" style="list-style-type:none"  id="prod'+i+'"> <div class="content1"><a class="mui-navigate-right" href="#">产品'+i+'<img id="remove_prod_img'+i+'" class="add_remove" alt="" onclick="remove(this.id)" src="<c:url value='/img/remove.png'/>"></div></a> <div class="mui-input-row mui-page"> <label class="itemlabel">产品类别:</label> <select class="iteminput" id="cons_productcategory'+i+'"  name="cons_productcategory" onchange="productCategoryChangeClass(this.id)"> <option value="">请选择产品类别</option> <option value="林志颖S1款">林志颖S1款</option> <option value="集成灶">集成灶</option> <option value="油烟机">油烟机</option> <option value="燃气灶">燃气灶</option> <option value="消毒柜">消毒柜</option> <option value="电蒸箱">电蒸箱</option> <option value="电烤箱">电烤箱</option> <option value="集成电蒸箱">集成电蒸箱</option> <option value="集成电烤箱">集成电烤箱</option> <option value="水槽">水槽</option> <option value="集成水槽">集成水槽</option> <option value="洗碗机">洗碗机</option> <option value="集成洗碗机">集成洗碗机</option> <option value="净水机">净水机</option> </select> </div> <div class="mui-input-row mui-page"> <label class="itemlabel">产品选项:</label> <select class="iteminput  cons_productoption_iteminput" id="cons_productoption'+i+'" name="cons_productoption" onchange="productoptionChange()"> <option value="1">带二维码产品</option> <!--  如果开放其他产品类别需要打开此注解--> <option value="2">不带二维码产品</option> </select> </div> <div class="mui-input-row mui-page" id="dv_headsn'+i+'"> <label class="itemlabel">机头编号:</label> <input  id="cons_headsn'+i+'" readonly="readonly" name="cons_headsn" type="text" class="cons_headsn" placeholder="请扫描机头编号"> <img id="cons_headsn_bt'+i+'" onclick="scan(this.id)" class="scanbt" alt="" src="<c:url value="/img/scan.png"/>"> </div> <div class="mui-input-row mui-page" id="dv_bodysn'+i+'"> <label class="itemlabel">机身编号:</label> <input  id="cons_bodysn'+i+'" readonly="readonly" name="cons_bodysn" type="text" class="cons_bodysn" placeholder="请扫描机身编号"> <img id="cons_bodysn_bt'+i+'" onclick="scan(this.id)" class="scanbt" alt="" src="<c:url value="/img/scan.png"/>"> </div> <div class="mui-input-row mui-page" id="dv_screenwificode'+i+'"> <label class="itemlabel">设备号:</label> <input  id="cons_screenwificode'+i+'" readonly="readonly" name="cons_screenwificode" type="text" class="cons_screenwificode" placeholder="请扫描设备号"> <img id="cons_screenwificode_bt'+i+'" onclick="scan(this.id)" class="scanbt" alt="" src="<c:url value="/img/scan.png"/>"> </div> <div class="mui-input-row mui-page" id="dv_producttype'+i+'"> <label class="itemlabel">产品型号:</label> <input class="iteminput" id="cons_producttype'+i+'" name="cons_producttype" type="text" class="mui-input-clear mui-input" placeholder="请输入产品型号"> </div> <div class="mui-input-row mui-page" id="dv_manufacturedate'+i+'"> <label class="itemlabel">生产日期:</label> <input class="iteminput" id="cons_manufacturedate'+i+'" onclick="time(this.id)" name="cons_manufacturedate" type="text"  class="mui-input-clear mui-input " placeholder="请输入生产日期"> </div> </li>'
			}

			function time(id) {
				document.activeElement.blur();
				var today = new Date();
				var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) });
				dtPicker.show(function(selectItems) {
					document.getElementById(id).value = selectItems.text;
					//dtPicker.dispose();
					dtPicker.hide();
				});
			}

			var appId='${appId}';

			var timestamp='${timestamp}';

			var nonceStr='${nonceStr}';

			var signature='${signature}';

			function scan(id) {
				if(id.indexOf("cons_screenwificode_bt")>=0){
					var count = id.substring(22);
				}else {
					var count = id.substring(14);
				}
				var cons_productcategory=document.getElementById('cons_productcategory'+count).value;
				wx.config({
					debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
					appId: appId, // 必填，公众号的唯一标识
					timestamp: timestamp, // 必填，生成签名的时间戳
					nonceStr: nonceStr, // 必填，生成签名的随机串
					signature: signature,// 必填，签名，见附录1
					jsApiList: ['scanQRCode'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});
				wx.ready(function(){
					//alert("运行到了这里 scan" );
					wx.scanQRCode({
						needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
						scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
						success: function (res) {
							var resultArray = res.resultStr.split(",");
							var result = resultArray[resultArray.length-1]; // 当needResult 为 1 时，扫码返回的结果
							//alert(result);

							if(cons_productcategory=='林志颖S1款'||
									cons_productcategory=='集成灶'||
									cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
								if(id.indexOf("cons_headsn_bt")>=0){
									if(result.indexOf("T")<0){
										showTrip("机头编码不符合规则");
										document.getElementById(id.substr(0,id.indexOf("_bt"))+count).value='';
										return ;
									}
								}else if(id.indexOf("cons_bodysn_bt")>=0){
									if(result.indexOf("G")<0){
										showTrip("机身编码不符合规则");
										document.getElementById(id.substr(0,id.indexOf("_bt"))+count).value='';
										return ;
									}
								}

							}
							document.getElementById(id.substr(0,id.indexOf("_bt"))+count).value=result;
						}
					});

				});
			}

			(function($, doc) {
				/*for (var i=0;i<elementsByClassName.length;i++){
					elementsByClassName.item(i).style.display="none"
				}*/

				mui.init({
					statusBarBackground: '#000000'
				});
				
				changeAdd(1,0);//省市区公用函数
				
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/consumer/list'/>";
				}
				var appId='${appId}';
				var timestamp='${timestamp}';
				var nonceStr='${nonceStr}';
				var signature='${signature}';
				wx.config({
				    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				    appId: appId, // 必填，公众号的唯一标识
				    timestamp: timestamp, // 必填，生成签名的时间戳
				    nonceStr: nonceStr, // 必填，生成签名的随机串
				    signature: signature,// 必填，签名，见附录1
				    jsApiList: ['scanQRCode'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});
				//code validate
				var countdown=60; 
				var codebtn = doc.getElementById('codebtn');
				codebtn.addEventListener('tap', function(event) {
				    var cons_phonenumber=doc.getElementById('cons_phonenumber').value;
					if(cons_phonenumber==''){
						//showTrip("用户手机不能为空");
						showTrip("用户手机不能为空");
						return ;
					}
					if(cons_phonenumber.length!=11){
						showTrip("用户手机长度为11位");
						return ;
					}
					var spinner= new Spinner();
					mui.ajax("<c:url value='/consumer/getCode'/>",{
						data:{
							phone:cons_phonenumber
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							codebtn.disabled=true;
						},
						complete: function() {
							spinner.spin();
							codebtn.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								settime(codebtn);
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					  });
					
				});
				var province=doc.getElementById("cons_province");
				//alert(province.selectedIndex);
				province.selectedIndex=-1;
				var cons_birthday = doc.getElementById('cons_birthday');
				cons_birthday.addEventListener('focus', function(event) {
					
					doc.activeElement.blur();
					var today = new Date();
					var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) }); 
					dtPicker.show(function(selectItems) {
						cons_birthday.value = selectItems.text;
						//dtPicker.dispose();
						dtPicker.hide();
					});
				}, false);

				var cons_purchasedate = doc.getElementById('cons_purchasedate');
				cons_purchasedate.addEventListener('focus', function(event) {
					doc.activeElement.blur(); 
					var today = new Date();
					var dtPicker = new mui.DtPicker({"type": "date",beginDate:new Date(1900, 01, 01), endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()) }); 
					dtPicker.show(function(selectItems) {
						cons_purchasedate.value = selectItems.text;
						//dtPicker.dispose();
						dtPicker.hide();
					});
				}, false);

			var saveButton = doc.getElementById('saveButton');
			saveButton.addEventListener('tap', function(event) {
				var cons_name=doc.getElementById('cons_name').value;
				var cons_phonenumber=doc.getElementById('cons_phonenumber').value;
				var cons_phonecode=doc.getElementById('cons_phonecode').value;
				
				//var cons_phonenumber=doc.getElementById('cons_phonenumber').value;
				var cons_source=doc.getElementById('cons_source').value;
				var cons_birthday=doc.getElementById('cons_birthday').value;
				var cons_gender=doc.getElementById('cons_gender').value;
				
				var cons_address=doc.getElementById('cons_address').value;
				//var cons_productoption=doc.getElementById('cons_productoption').value;
				var cons_province=doc.getElementById('cons_province').value;
				var cons_city=doc.getElementById('cons_city').value;
				var cons_county=doc.getElementById('cons_county').value;
				var cons_purchaseplace=doc.getElementById('cons_purchaseplace').value;
				var cons_purchasedate=doc.getElementById('cons_purchasedate').value;
				var cons_installationdate=doc.getElementById('cons_installationdate').value;
				var cons_Installationname=doc.getElementById('cons_Installationname').value;
				var cons_Installationtel=doc.getElementById('cons_Installationtel').value;

				if(cons_name==''){
					//showTrip("用户名称不能为空");
					 showTrip("用户名称不能为空");
					return ;
				}
				if(cons_phonenumber==''){
					//showTrip("用户手机不能为空");
					showTrip("用户手机不能为空");
					return ;
				}
				if(cons_phonenumber.length!=11){
					showTrip("用户手机长度为11位");
					return ;
				}

				if(cons_source==''){
					showTrip("请选择用户来源");
					//mui.toast('用户名不能为空');
					return ;
				}
				if(cons_birthday==''){
					showTrip("用户生日不能为空");
					return ;
				}
				if(cons_gender==''){
					showTrip("请选择性别");
					return ;
				}
				
				if(cons_province==''){
					showTrip("请选择省");
					return;
				}
				
				if(cons_city==''){
					showTrip("请选择市");
					return ;
				}
				if(cons_county==''){
					showTrip("请选择区/县");
					return ;
				}

				if(cons_address==''){
					showTrip("详细地址不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				//获取产品型号
				for (var i = 0; i < elementsByName.length; i++) {
					var num2 = elementsByName[i].id.substring(20);
					var cons_productcategory=doc.getElementById('cons_productcategory'+num2).value;
					var cons_manufacturedate=doc.getElementById('cons_manufacturedate'+num2).value;
					var cons_producttype=doc.getElementById('cons_producttype'+num2).value;

					if(cons_productcategory==''){
						showTrip("请选择产品类别");
						return ;
					}
					//var sngp =/^[A-Za-z0-9]+$/;
					var sngp = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
						var cons_headsn=doc.getElementById('cons_headsn'+num2).value;
						var cons_bodysn=doc.getElementById('cons_bodysn'+num2).value;
						var cons_productoption = document.getElementById('cons_productoption'+num2).value;
						if(cons_productoption==1){
							//有二维码机身、机头、设备号都需要
							var cons_screenwificode=doc.getElementById('cons_screenwificode'+num2).value;
							if(cons_headsn==''){
								showTrip("产品"+num2+"机头编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_headsn) ){
								showTrip("产品"+num2+"机头编号不能含有中文");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(cons_bodysn==''){
								showTrip("产品"+num2+"机身编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_bodysn) ){
								showTrip("产品"+num2+"机身编号不能含有中文");
								return ;
							}
							if(cons_productcategory=='林志颖S1款'){

								if(cons_screenwificode==''){
									showTrip("产品"+num2+"设备号不能为空");
									//mui.toast('用户名不能为空');
									return ;
								}
								if(sngp.test(cons_screenwificode) ){
									showTrip("产品"+num2+"设备号不能含有中文");
									return ;
								}
							}
						}
						else{
							//机身、机头需要

							if(cons_headsn==''){
								showTrip("产品"+num2+"机头编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_headsn) ){
								showTrip("产品"+num2+"机头编号不能含有中文");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(cons_bodysn==''){
								showTrip("产品"+num2+"机身编号不能为空");
								//mui.toast('用户名不能为空');
								return ;
							}
							if(sngp.test(cons_bodysn) ){
								showTrip("产品"+num2+"机身编号不能含有中文");
								return ;
							}
						}
					}
					else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						var cons_bodysn=doc.getElementById('cons_bodysn'+num2).value;
						if(cons_bodysn==''){
							showTrip("产品"+num2+"机身编号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(sngp.test(cons_bodysn) ){
							showTrip("产品"+num2+"机身编号不能含有中文");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(cons_producttype==''){
							showTrip("产品"+num2+"产品型号不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
						if(cons_manufacturedate==''){
							showTrip("产品"+num2+"生产日期不能为空");
							//mui.toast('用户名不能为空');
							return ;
						}
					}


				}

				if(cons_purchaseplace==''){
					showTrip("购买地点不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				if(cons_purchasedate==''){
					showTrip("购买日期不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				if(cons_installationdate==''){
					showTrip("安装日期不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				//
				if(!DateDiff(cons_purchasedate,cons_installationdate)){
					showTrip("安装日期应在购买日期之后");
					return ;
				}
				if(cons_Installationname==''){
					showTrip("安装人姓名不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				var sngp =/^[\u0391-\uFFE5-A-Za-z]+$/; 
				 if(!sngp.test(cons_Installationname) ){
					 showTrip("安装人姓名只能是中英文");
						return ;
				}
				if(cons_Installationtel==''){
					showTrip("安装人员手机不能为空");
					//mui.toast('用户名不能为空');
					return ;
				}
				
				if(cons_Installationtel.length!=11){
					showTrip("安装人员手机长度为11位");
					return ;
				}
				if(cons_phonecode==''){
					//showTrip("用户手机不能为空");
					showTrip("验证码不能为空");
					return ;
				}
				var spinner= new Spinner();
				var prod = "";

				for (var a = 1 ;a<11;a++){
					var element = document.getElementById("cons_productcategory"+a);
					var cons_producttype ='';
					var cons_manufacturedate = '';
					if (element!=null){
						var cons_productcategory = document.getElementById("cons_productcategory"+a).value;
						var cons_productoption = document.getElementById("cons_productoption"+a).value;
						var cons_headsn = document.getElementById("cons_headsn"+a).value;
						var cons_bodysn = document.getElementById("cons_bodysn"+a).value;
						var cons_screenwificode = document.getElementById("cons_screenwificode"+a).value;
						if(document.getElementById("cons_producttype"+a)!=null){
							cons_producttype = document.getElementById("cons_producttype"+a).value;
						}
						if(document.getElementById("cons_manufacturedate"+a)!=null){
							cons_manufacturedate = document.getElementById("cons_manufacturedate"+a).value;
						}
						if(a>1){
							if(cons_headsn != ''& (prod.indexOf(cons_headsn)!=-1)){
								showTrip("不能包含重复的机头编号");
								return false;
							}else if(cons_bodysn!='' & (prod.indexOf(cons_bodysn)!=-1)){
								showTrip("不能包含重复的机身编号");
								return false;
							}
							if(cons_productcategory=='林志颖S1款'||
									cons_productcategory=='集成灶'||
									cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){

							}

						}
						prod +='{"cons_productcategory":"'+cons_productcategory+'","cons_productoption":"'+cons_productoption+'","cons_headsn":"'+cons_headsn+'","cons_bodysn":"'+cons_bodysn+'","cons_screenwificode":"'+cons_screenwificode+'","cons_producttype":"'+cons_producttype+'","cons_manufacturedate":"'+cons_manufacturedate+'"},'
					}
				}
				prod='['+prod.substring(0,prod.lastIndexOf(","))+']'
				mui.ajax("<c:url value='/consumer/doEditMultiple'/>",{
					data:{
						cons_name:cons_name,
						cons_phonenumber:cons_phonenumber,
						cons_phonecode:cons_phonecode,
						cons_source:cons_source,
						cons_birthday:cons_birthday,
						cons_gender:cons_gender,
						cons_address:cons_address,
						//cons_productoption:cons_productoption,
						cons_province:cons_province,
						cons_city:cons_city,
						cons_county:cons_county,
						cons_purchaseplace:cons_purchaseplace,
						cons_purchasedate:cons_purchasedate,
						cons_installationdate:cons_installationdate,
						cons_Installationname:cons_Installationname,
						cons_Installationtel:cons_Installationtel,
						//cons_headsn:cons_headsn,
						//cons_bodysn:cons_bodysn,
						//cons_productcategory:cons_productcategory,
						//cons_screenwificode:cons_screenwificode,
						//cons_manufacturedate:cons_manufacturedate,
						//cons_producttype :cons_producttype
						prod:prod
					},
					dataType:'json',//服务器返回json格式数据
					type:'post',//HTTP请求类型
					headers:null,
					beforeSend: function() {
						spinner.spin(document.getElementById('preview'));
						saveButton.disabled=true;
					},
					complete: function() {
						spinner.spin();
						saveButton.disabled=false;
					},
					success:function(data){
						if(data.StatusCode==1){
							//成功
							//
							alert(data.Message);
							//mui.back();
							window.location.href="<c:url value='/consumer/list'/>";
						}else{
							alert(data.Message);
						}
						
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						console.log(type);
					}
				});
			});		
			}(mui, document));

			function DateDiff(d1,d2){    
			    start_at = new Date(d1.replace(/^(\d{4})(\d{2})(\d{2})$/,"$1/$2/$3"));
			    end_at = new Date(d2.replace(/^(\d{4})(\d{2})(\d{2})$/,"$1/$2/$3"));
			    if(start_at > end_at) {
			      return false;
			    }
			    return true;
			}

			function productoptionChange(){ 
				var cons_productoption = document.getElementById('cons_productoption').value;
				var cons_screenwificode_bt=document.getElementById("cons_screenwificode_bt");
				var cons_bodysn_bt=document.getElementById("cons_bodysn_bt");
				var cons_headsn_bt=document.getElementById("cons_headsn_bt");
				var cons_headsn=document.getElementById("cons_headsn");
				var cons_bodysn=document.getElementById("cons_bodysn");
				var cons_screenwificode=document.getElementById("cons_screenwificode");
				//
				var cons_productcategory = document.getElementById('cons_productcategory').value;
				var dv_headsn=document.getElementById("dv_headsn");
				var dv_bodysn=document.getElementById("dv_bodysn");
				var dv_screenwificode=document.getElementById("dv_screenwificode");
				//alert(dv_screenwificode);
				if(cons_productoption==1){
					//HAS QRCODE
					cons_screenwificode_bt.style.visibility ="visible";
					cons_bodysn_bt.style.visibility ="visible";
					cons_headsn_bt.style.visibility ="visible";
					cons_headsn.setAttribute("readOnly",'true');
					cons_bodysn.setAttribute("readOnly",'true');
					cons_screenwificode.setAttribute("readOnly",'true');
					
					cons_headsn.setAttribute("placeholder","请扫描机头编号");
					cons_bodysn.setAttribute("placeholder","请扫描机身编号");
					cons_screenwificode.setAttribute("placeholder","请扫描设备号");
					
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="block";
					}else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}
				}else{
					cons_screenwificode_bt.style.visibility ="hidden";
					cons_bodysn_bt.style.visibility ="hidden";
					cons_headsn_bt.style.visibility ="hidden";
					cons_headsn.removeAttribute("readOnly");
					cons_bodysn.removeAttribute("readOnly");
					cons_screenwificode.removeAttribute("readOnly");
					cons_headsn.setAttribute("placeholder","请输入机头编号");
					cons_bodysn.setAttribute("placeholder","请输入机身编号");
					cons_screenwificode.setAttribute("placeholder","请输入设备号");
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电蒸箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}
				}
				
			};

			function productoptionChange(id){
				if (id.indexOf("cons_productcategory")>=0){
					var count = id.substring(20);
				}else if(id.indexOf("cons_productoption")>=0){
					var count = id.substring(18);
				}
				var cons_productoption = document.getElementById('cons_productoption'+count).value;
				var cons_screenwificode_bt=document.getElementById("cons_screenwificode_bt"+count);
				var cons_bodysn_bt=document.getElementById("cons_bodysn_bt"+count);
				var cons_headsn_bt=document.getElementById("cons_headsn_bt"+count);
				var cons_headsn=document.getElementById("cons_headsn"+count);
				var cons_bodysn=document.getElementById("cons_bodysn"+count);
				var cons_screenwificode=document.getElementById("cons_screenwificode"+count);
				//
				var cons_productcategory = document.getElementById('cons_productcategory'+count).value;
				var dv_headsn=document.getElementById("dv_headsn"+count);
				var dv_bodysn=document.getElementById("dv_bodysn"+count);
				var dv_screenwificode=document.getElementById("dv_screenwificode"+count);
				//alert(dv_screenwificode);
				if(cons_productoption==1){
					//HAS QRCODE
					cons_screenwificode_bt.style.visibility ="visible";
					cons_bodysn_bt.style.visibility ="visible";
					cons_headsn_bt.style.visibility ="visible";
					cons_headsn.setAttribute("readOnly",'true');
					cons_bodysn.setAttribute("readOnly",'true');
					cons_screenwificode.setAttribute("readOnly",'true');

					cons_headsn.setAttribute("placeholder","请扫描机头编号");
					cons_bodysn.setAttribute("placeholder","请扫描机身编号");
					cons_screenwificode.setAttribute("placeholder","请扫描设备号");

					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="block";
					}else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}
				}else{
					cons_screenwificode_bt.style.visibility ="hidden";
					cons_bodysn_bt.style.visibility ="hidden";
					cons_headsn_bt.style.visibility ="hidden";
					cons_headsn.removeAttribute("readOnly");
					cons_bodysn.removeAttribute("readOnly");
					cons_screenwificode.removeAttribute("readOnly");
					cons_headsn.setAttribute("placeholder","请输入机头编号");
					cons_bodysn.setAttribute("placeholder","请输入机身编号");
					cons_screenwificode.setAttribute("placeholder","请输入设备号");
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电蒸箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
					}
				}

			};

			function changeOption(type){
				   document.getElementById("cons_productoption").options.length=0;
				   if(type==1){
					   $("#cons_productoption").append("<option value='1'>带二维码产品</option>");
				   }else{
					  /* $("#cons_productoption").append("<option value='1'>带二维码产品</option>"); */
					   $("#cons_productoption").append("<option value='2'>不带二维码产品</option>"); 
				   }
				   productoptionChange();
			}

			function changeOption(type,id){
				var count = id.substring(20);
				document.getElementById("cons_productoption"+count).options.length=0;
				if(type==1){
					$("#cons_productoption"+count).append("<option value='1'>带二维码产品</option>");
				}else{
					/* $("#cons_productoption").append("<option value='1'>带二维码产品</option>"); */
					$("#cons_productoption"+count).append("<option value='2'>不带二维码产品</option>");
				}
				productoptionChange(id);
			}

			function productCategoryChange(){
				var nodeList = document.getElementsByClassName('cons_productoption_iteminput');
				var cons_productcategory = document.getElementById('cons_productcategory').value;
				var dv_headsn=document.getElementById("dv_headsn");
				var dv_bodysn=document.getElementById("dv_bodysn");
				var dv_screenwificode=document.getElementById("dv_screenwificode");
				var dv_manufacturedate=document.getElementById("dv_manufacturedate");
				var dv_producttype=document.getElementById("dv_producttype");
				for (var i=0;i<nodeList.length;i++){
					var cons_productcategory =nodeList.item(i);
					if(cons_productcategory=='林志颖S1款'||
							cons_productcategory=='集成灶'||
							cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
						//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
						dv_headsn.style.display ="block";
						dv_bodysn.style.display ="block";
						dv_manufacturedate.style.display ="none";
						dv_producttype.style.display ="none";
						changeOption(1);
						var cons_productoption = document.getElementById('cons_productoption').value;
						if(cons_productoption==1&cons_productcategory=='林志颖S1款'){
							//有二维码
							dv_screenwificode.style.display ="block";
						}else{
							dv_screenwificode.style.display ="none";
						}

					}
					else if(cons_productcategory=='油烟机'||
							cons_productcategory=='燃气灶'||
							cons_productcategory=='消毒柜'||
							cons_productcategory=='电蒸箱'||
							cons_productcategory=='电烤箱'||
							cons_productcategory=='水槽'||
							cons_productcategory=='集成水槽'||
							cons_productcategory=='洗碗机'||
							cons_productcategory=='集成洗碗机'||
							cons_productcategory=='净水机'){
						//只有机身字段
						changeOption(2);
						dv_headsn.style.display ="none";
						dv_bodysn.style.display ="block";
						dv_screenwificode.style.display ="none";
						dv_manufacturedate.style.display ="block";
						dv_producttype.style.display ="block";
					}
				}
				if(cons_productcategory=='林志颖S1款'||
						cons_productcategory=='集成灶'||
						cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
					//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
					dv_headsn.style.display ="block";
					dv_bodysn.style.display ="block";
					dv_manufacturedate.style.display ="none";
					dv_producttype.style.display ="none";
					changeOption(1);
					var cons_productoption = document.getElementById('cons_productoption').value;
					if(cons_productoption==1&cons_productcategory=='林志颖S1款'){
						//有二维码
						dv_screenwificode.style.display ="block";
					}else{
						dv_screenwificode.style.display ="none";
					}
				
				}
				else if(cons_productcategory=='油烟机'||
						cons_productcategory=='燃气灶'||
						cons_productcategory=='消毒柜'||
						cons_productcategory=='电蒸箱'||
						cons_productcategory=='电烤箱'||
						cons_productcategory=='水槽'||
						cons_productcategory=='集成水槽'||
						cons_productcategory=='洗碗机'||
						cons_productcategory=='集成洗碗机'||
						cons_productcategory=='净水机'){
					//只有机身字段
					changeOption(2);
					dv_headsn.style.display ="none";
					dv_bodysn.style.display ="block";
					dv_screenwificode.style.display ="none";
					dv_manufacturedate.style.display ="block";
					dv_producttype.style.display ="block";
				}
				
			}

			function productCategoryChangeClass(id){
				var count = id.substring(20);
				var cons_productcategory = document.getElementById(id).value;
				var dv_headsn=document.getElementById("dv_headsn"+count);
				var dv_bodysn=document.getElementById("dv_bodysn"+count);
				var dv_screenwificode=document.getElementById("dv_screenwificode"+count);
				var dv_manufacturedate=document.getElementById("dv_manufacturedate"+count);
				var dv_producttype=document.getElementById("dv_producttype"+count);
				var cons_headsn=document.getElementById("cons_headsn"+count);
				var cons_bodysn=document.getElementById("cons_bodysn"+count);
				var cons_screenwificode=document.getElementById("cons_screenwificode"+count);
				var cons_manufacturedate=document.getElementById("cons_manufacturedate"+count);
				var cons_producttype=document.getElementById("cons_producttype"+count);
				if(cons_productcategory=='林志颖S1款'||
						cons_productcategory=='集成灶'||
						cons_productcategory=='集成电蒸箱'||cons_productcategory=='集成电烤箱'){
					//机身、机头、设备号都需要,dv_headsn,dv_bodysn,dv_screenwificode
					dv_headsn.style.display ="block";
					dv_bodysn.style.display ="block";
					dv_manufacturedate.style.display ="none";
					dv_producttype.style.display ="none";
					changeOption(1,id);
					var cons_productoption = document.getElementById('cons_productoption'+count).value;
					if(cons_productoption==1&cons_productcategory=='林志颖S1款'){
						//有二维码
						dv_screenwificode.style.display ="block";
					}else{
						dv_screenwificode.style.display ="none";
					}
					cons_bodysn.value = '';
					cons_headsn.value = '';
					cons_screenwificode.value = '';
					cons_manufacturedate.value = '';
					cons_producttype.value = '';
				}
				else if(cons_productcategory=='油烟机'||
						cons_productcategory=='燃气灶'||
						cons_productcategory=='消毒柜'||
						cons_productcategory=='电蒸箱'||
						cons_productcategory=='电烤箱'||
						cons_productcategory=='水槽'||
						cons_productcategory=='集成水槽'||
						cons_productcategory=='洗碗机'||
						cons_productcategory=='集成洗碗机'||
						cons_productcategory=='净水机'){
					//只有机身字段
					changeOption(2,id);
					dv_headsn.style.display ="none";
					dv_bodysn.style.display ="block";
					dv_screenwificode.style.display ="none";
					dv_manufacturedate.style.display ="block";
					dv_producttype.style.display ="block";
					cons_bodysn.value = '';
					cons_headsn.value = '';
					cons_screenwificode.value = '';
					cons_manufacturedate.value = '';
					cons_producttype.value = '';
				}

			}

			function changeAdd(value,type){ 
				//var obj=document.getElementById("agac_province");
				//alert(type);
				if(type==0){
					mui.ajax("<c:url value='/consumer/province'/>",{
						data:{
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						contentType:"application/x-www-form-urlencoded;charset=utf-8",
						beforeSend: function(XMLHttpRequest){
							   XMLHttpRequest.setRequestHeader("Accept", "text/plain");
							  },
						headers:null,
						success:function(data){
							 var json = eval(data);
							 var obj=document.getElementById("cons_province");
							 //obj.options.add(new Option("选择",""));
						        for(var i=0; i<json.length; i++)
						        {
						           var id=json[i].prov_ProvinceID;
								   var name=json[i].prov_Name;
						           obj.options.add(new Option(name,id));
						        }
						}
					});
				}
				if(type==1){//通过省份获取城市
					var obj=document.getElementById("cons_province");
					var index=obj.selectedIndex;
					var value2 = obj.options[index].value;

					mui.ajax("<c:url value='/consumer/city'/>",{
						data:{
							id:value2
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null, 
						success:function(data){    
							var json = eval(data); 
							//document.getElementById("paor_city").value="";
							var obj=document.getElementById("cons_city");
							 //obj.options.add(new Option("选择",""));
							for(var i=0; i<json.length; i++)  
					        {   
					           var id=json[i].city_CityID; 
							   var name=json[i].city_Name;
					           obj.options.add(new Option(name,id));
					        }
						}
					});
				}
				if(type==2){//通过城市获取区域
					var obj=document.getElementById("cons_city");
					var index=obj.selectedIndex;
					var value2 = obj.options[index].value;

					mui.ajax("<c:url value='/consumer/county'/>",{
						data:{
							id:value2
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null, 
						success:function(data){    
							//doc.getElementById("paor_county").value="";
							var json = eval(data); 
							var obj=document.getElementById("cons_county");
							//obj.options.add(new Option("选择",""));
							for(var i=0; i<json.length; i++)  
					        {   
					           var id=json[i].coun_CountyID; 
							   var name=json[i].coun_Name;
					           obj.options.add(new Option(name,id));
					        }
						}
					});
				}
				
			}  
			
		</script>
	<script type="text/javascript"> 
		var countdown=60*10;
		function settime(obj) {
			if (countdown == 0) {
				obj.removeAttribute("disabled");
				obj.value="获取验证码";
				countdown = 60*60;
				return;
			} else {
				obj.setAttribute("disabled", true);
				obj.value="重新获取(" + countdown + ")";
				countdown--;
			}
		setTimeout(function() {
			settime(obj) }
			,1000)
		}

</script>
</body>

</html>