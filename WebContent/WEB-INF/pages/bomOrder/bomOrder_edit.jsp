<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>新增物料订单</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}

			@media ( min-width :320px) {
				.mui-input-group .itemlabel {
					width: 40%;
				}
				.mui-input-row .iteminput{
					width: 60%;
				}
			}
			@media ( min-width :360px) {
				.mui-input-group .itemlabel {
					width: 36%;
				}
				.mui-input-row .iteminput{
					width: 64%;
				}
			}
			@media ( min-width :768px) {
				.mui-input-group .itemlabel {
					width: 17%;
				}
				.mui-input-row .iteminput{
					width: 83%;
				}
			}
			@media ( min-width :800px) {
				.mui-input-group .itemlabel {
					width: 16%;
				}
				.mui-input-row .iteminput{
					width: 84%;
				}
			}
			@media ( min-width :980px) {
				.mui-input-group .itemlabel {
					width: 13%;
				}
				.mui-input-row .iteminput{
					width: 87%;
				}
			}
			@media ( min-width :1080px) {
				.mui-input-group .itemlabel {
					width: 12%;
				}
				.mui-input-row .iteminput{
					width: 88%;
				}
			}
			@media ( min-width :1280px) {
				.mui-input-group .itemlabel {
					width: 10%;
				}
				.mui-input-row .iteminput{
					width: 90%;
				}
			}
			@media ( min-width :1920px) {
				.mui-input-group .itemlabel {
					width: 7%;
				}
				.mui-input-row .iteminput{
					width: 93%;
				}
			}
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
			#goor_companyid{
				margin-top: 5px;
				margin-bottom: 5px;
				margin-left: 0px;
				margin-right: 0px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
			#goor_address{
				margin-top: 5px;
				margin-bottom: 5px;
				margin-left: 0px;
				margin-right: 0px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">新增物料订单</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		  <section id="preview"></section>
			<form class="mui-input-group">
			
			   <div class="mui-input-row">
					<label class="itemlabel">可用余额:</label>
					<input class="iteminput" id='credit' name="credit" readonly="readonly" value="${item.credit}元" type="text" class="mui-input" >
				</div>
			   <div class="mui-input-row">
					<label class="itemlabel">押金余额:</label>
					<input class="iteminput" id='deposit' name="deposit" readonly="readonly" value="${item.deposit}元" type="text" class="mui-input" >
				</div>
			  <div class="mui-input-row" style="height: 90px;">
					<label class="itemlabel">客户名称:</label>
					<textarea class="iteminput"  id='goor_companyid' name="goor_companyid" rows="2"  cols="2"   placeholder="请输入客户名称">${item.comp_name}</textarea>  
				
				</div>
				<%--<div class="mui-input-row">
					<label class="itemlabel">联系电话:</label>
					<input class="iteminput" id='goor_tel' name="goor_tel" type="text" value="${item.comp_phone}" class="mui-input-clear mui-input" placeholder="请输入联系电话">
				</div>--%>
				<%--<div class="mui-input-row" id="addressdiv" style="height: 90px;">
					<label class="itemlabel">客户地址:</label>
                   <textarea class="iteminput"  id='goor_address' name="goor_address" rows="2"  cols="2"   placeholder="请输入客户地址">${item.comp_address}</textarea>
				</div>--%>

				<div class="mui-input-row">
					<label class="itemlabel">物料发放方式:</label>
					<select class="iteminput" id="goor_deliveringway" name="goor_deliveringway" onchange="deliveringwayChange(this.value)">

						<option value="">请选择物料发放方式</option>
						<option value="1">物流</option>
						<option value="3">快递</option>
						<option value="4">自提</option>
					</select>

				</div>
				<div id="procityinfo" style="display: none;">
					<div data-toggle="distpicker">
						<div class="mui-input-row">
							<label class="itemlabel">省:</label>
							<select class="iteminput" class="form-control" id="goor_province" data-province=""  onchange="changeAdd(this.value,1)"></select>
						</div>
						<div class="mui-input-row">
							<label class="itemlabel">市:</label>
							<select class="iteminput" class="form-control" id="goor_city"  data-city=""  onchange="changeAdd(this.value,2)"></select>
						</div>
						<div class="mui-input-row">
							<label class="itemlabel">区/县:</label>
							<select class="iteminput" class="form-control" id="goor_county"  data-district=""   onchange="changeAdd(this.value,3)"></select>
						</div>
					</div>
				</div>
				<div class="mui-input-row" id="addressdiv" style="height: 113px;"><%--display: none;--%>
					<label class="itemlabel">详细地址:</label>
					<textarea class="iteminput"  id='goor_address' name="goor_address" rows="3"  cols="2" placeholder="请输入详细地址">${item.comp_address}</textarea>
				</div>
				<div id="kuaidiinfo" style="display: none;">
					<div class="mui-input-row">
						<label class="itemlabel">收件人手机:</label>
						<input class="iteminput" id='goor_recipientsphone' name="goor_recipientsphone" value="${item.comp_recipientsphone}" type="number" class="mui-input-clear mui-input" placeholder="请输入收件人手机">
					</div>
					<div class="mui-input-row">
						<label class="itemlabel">收件人姓名:</label>
						<input class="iteminput" id='goor_recipients' name="goor_recipients" type="text" value="${item.comp_recipients}"  class="mui-input-clear mui-input" placeholder="请输入收件人姓名" >
					</div>
				</div>

				<div class="mui-input-row">
					<label class="itemlabel">订货日期:</label>
					<input class="iteminput" id='goor_orderdate' name="goor_orderdate" readonly="readonly" value="${goor_orderdate}" type="text" class="mui-input" placeholder="请输入订货日期">
				</div>

				<!-- 
				<div class="mui-input-row">
					<label>专卖店地址:</label>
					<input id='goor_storeaddress' name="goor_storeaddress" type="text" class="mui-input-clear mui-input" placeholder="请输入专卖店地址">
				</div>
				<div class="mui-input-row">
					<label>专卖店电话:</label>
					<input id='goor_storetel' name="goor_storetel" type="text" class="mui-input-clear mui-input" placeholder="请输入专卖店电话">
				</div>
				 -->
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">保存</button>
			</div>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
	  
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/bomOrder/list?flag=1'/>";
				}
			   var saveButton = doc.getElementById('saveButton');
			    saveButton.addEventListener('click', function(event) {
					var goor_province = '';
					var goor_city = '';
					var goor_county = '';
					var goor_recipientsphone = '';
					var goor_recipients = '';
			    	var goor_companyid=doc.getElementById('goor_companyid').value;
					var goor_address=doc.getElementById('goor_address').value;
					var goor_orderdate=doc.getElementById('goor_orderdate').value;
					//var goor_earnest=doc.getElementById('goor_earnest').value;
					//var goor_storeaddress=doc.getElementById('goor_storeaddress').value;
					//var goor_storetel=doc.getElementById('goor_storetel').value;
					//var goor_tel=doc.getElementById('goor_tel').value;
					var goor_deliveringway=doc.getElementById('goor_deliveringway').value;
					var sngp = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
					if(goor_companyid==''){
						mui.alert("客户名称不能为空");
						return ;
					}
					/*if(goor_address==''){
						mui.alert("客户地址不能为空");
						return ;
					}*/
					if(goor_orderdate==''){
						mui.alert("订货日期不能为空");
						return ;
					}

					/*if(goor_tel==''){
						mui.alert("联系电话不能为空");
						return ;
					}*/
					/*if(sngp.test(goor_tel) ){
						mui.alert("联系电话不能含有中文");
						return ;
					}*/
					if(goor_deliveringway==''){
						mui.alert("请选择物料发放方式");
						return ;
					}
					if(goor_deliveringway==3){
						goor_province=doc.getElementById('goor_province').value;
						goor_city=doc.getElementById('goor_city').value;
						goor_county=doc.getElementById('goor_county').value;
						goor_address=doc.getElementById('goor_address').value;
						goor_recipientsphone=doc.getElementById('goor_recipientsphone').value;

						goor_recipients=doc.getElementById('goor_recipients').value;
						if(goor_province==0){
							mui.alert("请选择省");
							return;
						}
						if(goor_city==0){
							mui.alert("请选择市");
							return ;
						}
						if(goor_county==0){
							mui.alert("请选择区/县");
							return ;
						}
					/*	if(goor_address==''){
							mui.alert("详细地址不能为空");
							return ;
						}*/
						if(goor_recipientsphone==''){
							mui.alert("收件人手机不能为空");
							return ;
						}

						if(goor_recipientsphone.length!=11){
							mui.alert("收件人手机长度为11位");
							return ;
						}

						if(goor_recipients==''){
							mui.alert("收件人姓名不能为空");
							return ;
						}
						var sngp =/^[\u0391-\uFFE5-A-Za-z]+$/;
						if(!sngp.test(goor_recipients) ){
							mui.alert("收件人姓名只能是中英文");
							return ;
						}

					}
					if(goor_address==''){
						mui.alert("详细地址不能为空");
						return ;
					}
					var spinner= new Spinner();
					mui.ajax("<c:url value='/bomOrder/doEdit'/>",{
						data:{
							goor_companyid:goor_companyid,
							goor_address:goor_address,
							//goor_tel:goor_tel,
							goor_deliveringway:goor_deliveringway,
							goor_province:goor_province,
							goor_city:goor_city,
							goor_county:goor_county,
							goor_orderdate:goor_orderdate,
							goor_recipientsphone:goor_recipientsphone,
							goor_recipients:goor_recipients
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型；
						headers:null,
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							saveButton.disabled=true;
						},
						complete: function() {
							spinner.spin();
							saveButton.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								//
								alert(data.Message);
								//mui.back();
								window.location.href="<c:url value='/bomOrder/get?goor_status=1&id='/>"+data.ID;
								
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					});
					
				}, false);
			}(mui, document));
			function changeAdd(value,type){
				if(type==0){
					mui.ajax("<c:url value='/consumer/province'/>",{
						data:{
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						contentType:"application/x-www-form-urlencoded;charset=utf-8",
						beforeSend: function(XMLHttpRequest){
							XMLHttpRequest.setRequestHeader("Accept", "text/plain");
						},
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("goor_province");
							//obj.options.add(new Option("选择",""));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].prov_ProvinceID;
								var name=json[i].prov_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
				if(type==1){//通过省份获取城市
					mui.ajax("<c:url value='/consumer/city'/>",{
						data:{
							id:value
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("goor_city");
							obj.options.length=0;//清除
							obj.options.add(new Option("请选择市","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].city_CityID;
								var name=json[i].city_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
				if(type==2){//通过城市获取区域
					mui.ajax("<c:url value='/consumer/county'/>",{
						data:{
							id:value
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("goor_county");
							obj.options.length=0;//清除
							obj.options.add(new Option("请选择区/县","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].coun_CountyID;
								var name=json[i].coun_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}

			}
			function deliveringwayChange(value){

				var procityinfo=document.getElementById("procityinfo");
				//var addressdiv=document.getElementById("addressdiv");
				var kuaidiinfo=document.getElementById("kuaidiinfo");
				if(value==3){
					//kuaidi
					mui.ajax("<c:url value='/consumer/province'/>",{
						data:{
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						contentType:"application/x-www-form-urlencoded;charset=utf-8",
						beforeSend: function(XMLHttpRequest){
							XMLHttpRequest.setRequestHeader("Accept", "text/plain");
						},
						headers:null,
						success:function(data){
							var json = eval(data);
							document.getElementById("goor_province").value="";
							var obj=document.getElementById("goor_province");
							var obj2=document.getElementById("goor_county");
							var obj1=document.getElementById("goor_city");
							obj.options.add(new Option("请选择省","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].prov_ProvinceID;
								var name=json[i].prov_Name;
								obj.options.add(new Option(name,id));
							}
							obj1.options.add(new Option("请选择市","0"));
							obj2.options.add(new Option("请选择区/县","0"));

							//doc.getElementById("showJson").value=data;
							/* var obj=doc.getElementById("goor_province");
							 $.each(data, function(i, n){
							 var id=n.prov_ProvinceID;

							 var name=n.prov_Name;
							 if(i==1)
							 alert(id);
							 //obj.options.add(new Option(name,id));
							 }); */
						}
					});

					procityinfo.style.display ="block";
					//addressdiv.style.display ="block";
					kuaidiinfo.style.display ="block";
				}else{
					procityinfo.style.display ="none";
					//addressdiv.style.display ="none";
					kuaidiinfo.style.display ="none";
				}
			}
		</script>
	</body>

</html>