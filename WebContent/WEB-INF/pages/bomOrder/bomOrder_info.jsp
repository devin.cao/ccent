<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>物料订单详情</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			
			.mui-input-group {
				margin-top: 10px;
			}
			
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-input-group label {
				width: 27%;
			}
			
			#vcode {
				width: 50%;
				float: left;
			}
			
			#code {
				width: 23%;
				float: right;
			}
			
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 73%;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			
			.mui-content-padded {
				margin-top: 25px;
			}
			
			.mui-btn {
				padding: 10px;
			}
			
			.link-area {
				display: block;
				margin-top: 25px;
				text-align: center;
			}
			
			.spliter {
				color: #bbb;
				padding: 0px 8px;
			}
			
			.oauth-area {
				position: absolute;
				bottom: 20px;
				left: 0px;
				text-align: center;
				width: 100%;
				padding: 0px;
				margin: 0px;
			}
			
			.oauth-area .oauth-btn {
				display: inline-block;
				width: 50px;
				height: 50px;
				background-size: 30px 30px;
				background-position: center center;
				background-repeat: no-repeat;
				margin: 0px 20px;
				/*-webkit-filter: grayscale(100%); */
				border: solid 1px #ddd;
				border-radius: 25px;
			}
			
			.oauth-area .oauth-btn:active {
				border: solid 1px #aaa;
			}
			
			.oauth-area .oauth-btn.disabled {
				background-color: #ddd;
			}
		</style>

	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">物料订单详情</h1>
		<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		 <section id="preview"></section>
			<ul class="mui-table-view">
				<li class="mui-table-view-cell" id="productodinfolist">
					
					<input id="gode_goodsorderid" type="hidden" name="gode_goodsorderid" value="${item.goor_GoodsOrderID}">
					<input id="goor_status" type="hidden" name="goor_status" value="${item.goor_status}">
					
					<label>订单编号: </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_name }</label>
					
				</li>
				<li class="mui-table-view-cell">
					<label>客户名称: </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_company }</label>
				</li>
				<%--<li class="mui-table-view-cell">
					<label>联系电话:</label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_tel }</label>
				</li>--%>
				<li class="mui-table-view-cell">
					<label>客户地址: </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_address }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>订货日期: </label>&nbsp;&nbsp;&nbsp;<label>
					<c:set var="pi_date" value="${ item.goor_orderdate}"/>
						<c:choose>  
                         <c:when test="${fn:length(pi_date) > 10}">  
                           <c:out value="${fn:substring(pi_date, 0, 10)}" />  
                         </c:when>  
                    <c:otherwise>  
                    <c:out value="${pi_date}" />  
                     </c:otherwise>  
                    </c:choose>
					</label>
				</li>
				<li class="mui-table-view-cell">
					<label>货款总额: </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_totalamount }元</label>
				</li>
				<!--  
				<li class="mui-table-view-cell">
					<label>专卖店地址: </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_storeaddress }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>专卖店电话:  </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_storetel }</label>
				</li>-->
				<li class="mui-table-view-cell">
					<label>物料发放方式:</label>&nbsp;&nbsp;&nbsp;<label>
					<c:if test="${ item.goor_deliveringway eq 1}">物流</c:if>
					<c:if test="${ item.goor_deliveringway eq 3}">快递</c:if>
					<c:if test="${ item.goor_deliveringway eq 4}">自提</c:if>
				</label>
				</li>

				<c:if test="${ item.goor_deliveringway eq 3}">
					<li class="mui-table-view-cell">
						<label>省:</label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_province }</label>
					</li>
					<li class="mui-table-view-cell">
						<label>市:</label>&nbsp;&nbsp;<label>${ item.goor_city }</label>
					</li>
					<li class="mui-table-view-cell">
						<label>县:</label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_county }</label>
					</li>
					<li class="mui-table-view-cell">
						<label>详细地址:</label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_address }</label>
					</li>
					<li class="mui-table-view-cell">
						<label>收件人手机:</label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_recipientsphone }</label>
					</li>
					<li class="mui-table-view-cell">
						<label>收件人姓名:</label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_recipients }</label>
					</li>
				</c:if>
				<li class="mui-table-view-cell">
					<label>状态:</label>&nbsp;&nbsp;&nbsp;<label id="statuslb">
					<c:if test="${ item.goor_status eq 1}">新建</c:if>
					<c:if test="${ item.goor_status eq 2}">已提交</c:if>
					<c:if test="${ item.goor_status eq 3}">审核中</c:if>
					<c:if test="${ item.goor_status eq 4}">已审核</c:if>
					<c:if test="${ item.goor_status eq 5}">发货中</c:if>
					<c:if test="${ item.goor_status eq 6}">执行关闭</c:if>
				</label>
				</li>
				<li class="mui-table-view-cell">
					<label>日期:</label>&nbsp;&nbsp;&nbsp;<label>
					<c:set var="orderdate" value="${ item.goor_orderdate}"/>
					<c:choose>
						<c:when test="${fn:length(orderdate) > 10}">
							<c:out value="${fn:substring(orderdate, 0, 10)}" />
						</c:when>
						<c:otherwise>
							<c:out value="${orderdate}" />
						</c:otherwise>
					</c:choose>
				</label>
				</li>
				<li class="mui-table-view-cell">
					<label>接单员姓名 :  </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_orderer }</label>
				</li>
				<li class="mui-table-view-cell">
					<label>接单员电话 :  </label>&nbsp;&nbsp;&nbsp;<label>${ item.goor_ordererphone }</label>
				</li>
				
			</ul>
			
			<div class="mui-content-padded"  id="submitdiv">
				<button id='addDetailButton' class="mui-btn mui-btn-block mui-btn-primary">
				<c:if test="${item.goor_status eq 1 }">
				物料下单
				</c:if>
				<c:if test="${item.goor_status ne 1 }">
				查看物料订单
				</c:if>
				</button>
			</div>
			
			<c:if test="${ item.goor_status eq 1 }">
			<div class="mui-content-padded" id="deletediv">
				<button id='deleteButton' class="mui-btn mui-btn-block mui-btn-primary">删除</button>
			</div>
			</c:if>
			
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/bomOrder/list?flag=1'/>";
				}
				
				
				var addDetailButton = doc.getElementById('addDetailButton');
				addDetailButton.addEventListener('tap', function(event) {
					var gode_goodsorderid= doc.getElementById('gode_goodsorderid').value;
					  var goor_status= doc.getElementById('goor_status').value;
				     mui.openWindow({
					    id: 'ordergoodslist',
					    url:  "<c:url value='/bomOrder/detaillist?gode_goodsorderid=' />"+gode_goodsorderid
				     });
				}, false);
				
				var deleteButton = doc.getElementById('deleteButton');
				deleteButton.addEventListener('tap', function(event) {
					var btnArray = ['取消', '确定'];   
		                mui.confirm('确定要删除订单吗', '提示', btnArray, function(e) {
		                    if (e.index == 1) {
		                    	dataDelete();
		                    } else {
		                    }
		                })
				}, false);
				
				
				function dataDelete(){
					 var orderid= doc.getElementById('gode_goodsorderid').value;
					 var spinner= new Spinner();
						mui.ajax("<c:url value='/bomOrder/delete'/>",{
							data:{
								orderid:orderid
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(document.getElementById('preview'));
								deleteButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								deleteButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									 alert(data.Message);
									 window.location.href="<c:url value='/bomOrder/list?flag=1'/>";
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				};
			}(mui, document));
		</script>
	</body>

</html>