<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="ui-page-login">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>权限验证</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
		    .mui-content-padded {
                background-color: white;
            }
			.area {
				margin: 20px auto 0px auto;
			}
			
			.mui-input-group {
				margin-top: 10px;
			}
			
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-input-group label {
				width: 30%;
			}
			
			
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 70%;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			
			.mui-content-padded {
				margin-top: 25px;
			}
			
			.mui-btn {
				padding: 10px;
			}
			
			.spliter {
				color: #bbb;
				padding: 0px 8px;
			}
			
			
		</style>

	</head>

	<body>
	<%--	<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">权限验证</h1>
		</header>--%>
		<c:set var="message" value="${sessionScope.Message}"/>
		<%--<div class="mui-content">
		    <span>${message} </span>

		</div>--%>
		<script src="<c:url value='/js/jweixin-1.0.0.js'/>"></script>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/mui.enterfocus.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script>
			mui.alert('${message}', function() {
				WeixinJSBridge.call('closeWindow');
			});
		</script>
	</body>

</html>