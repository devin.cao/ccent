<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>注册</title>
		<jsp:include page="../share/commoncss.jsp"></jsp:include>
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			@media ( min-width :320px) {
	     .mui-input-group .itemlabel {
				width: 40%;
			}
			.mui-input-row .iteminput{
				width: 60%;
			}
}
@media ( min-width :360px) {
	     .mui-input-group .itemlabel {
				width: 36%;
			}
			.mui-input-row .iteminput{
				width: 64%;
			}
}
@media ( min-width :768px) {
	      .mui-input-group .itemlabel {
				width: 17%;
			}
			.mui-input-row .iteminput{
				width: 83%;
			}
}
@media ( min-width :800px) {
	       .mui-input-group .itemlabel {
				width: 16%;
			}
			.mui-input-row .iteminput{
				width: 84%;
			}
}
@media ( min-width :980px) {
	      .mui-input-group .itemlabel {
				width: 13%;
			}
			.mui-input-row .iteminput{
				width: 87%;
			}
}
@media ( min-width :1080px) {
	       .mui-input-group .itemlabel {
				width: 12%;
			}
			.mui-input-row .iteminput{
				width: 88%;
			}
}
@media ( min-width :1280px) {
	     .mui-input-group .itemlabel {
				width: 10%;
			}
			.mui-input-row .iteminput{
				width: 90%;
			}
}
@media ( min-width :1920px) {
	      .mui-input-group .itemlabel {
				width: 7%;
			}
			.mui-input-row .iteminput{
				width: 93%;
			}
}	
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">注册</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		 <section id="preview"></section>
			<form class="mui-input-group">
				<div class="mui-input-row">
					<label   class="itemlabel">姓名:</label>
					<input  class="iteminput" id='agac_name' name="agac_name" type="text" class="mui-input-clear mui-input" placeholder="请输入姓名">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">登录密码:</label>
					<input class="iteminput" id='agac_password' name="agac_password" type="password" class="mui-input-clear mui-input" placeholder="请输入登录密码">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">重复登录密码:</label>
					<input class="iteminput" id='password_confirm' name="password_confirm" type="password" class="mui-input-clear mui-input" placeholder="请输入重复登录密码">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">二级密码:</label>
					<input class="iteminput" id='agac_orderpassword' name="agac_orderpassword" type="password" class="mui-input-clear mui-input" placeholder="请输入二级密码">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">重复二级密码:</label>
					<input class="iteminput" id='confirmorderpwd' name="confirmorderpwd" type="password" class="mui-input-clear mui-input" placeholder="请输入重复二级密码">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">在亿田账户名:</label>
					<input class="iteminput" id='agac_accountname' name="agac_accountname" type="text" class="mui-input-clear mui-input" placeholder="请输入在亿田账户名">
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">移动电话:</label>
					<input class="iteminput" id='agac_mobilephone1' name="agac_mobilephone1" type="text" class="mui-input-clear mui-input" placeholder="请输入移动电话">
				</div>
				<div class="mui-input-row" style="display: none">
					<label  class="itemlabel">移动电话2:</label>
					<input class="iteminput" id='agac_mobilephone2' name="agac_mobilephone2" type="text" class="mui-input-clear mui-input" placeholder="请输入移动电话2">
				</div>
				 <div data-toggle="distpicker">
				<div class="mui-input-row">
					<label  class="itemlabel">省:</label>
					 <select class="iteminput" class="form-control" id="agac_province" data-province=""  onchange="changeAdd(this.value,1)">
					 </select>
					
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">市:</label>
					 <select class="iteminput" class="form-control" id="agac_city" data-province=""  onchange="changeAdd(this.value,2)"></select>
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">区/县:</label>
					 <select class="iteminput" class="form-control" id="agac_county" onchange="changeAdd(this.value,3)"></select>
				</div>
				</div>
				<div class="mui-input-row">
					<label  class="itemlabel">详细地址:</label>
					<input class="iteminput" id='agac_address'  name='agac_address' type="text" class="mui-input-clear mui-input" placeholder="请输入详细地址">
				</div>
				<!--  
				<div class="mui-input-row">
					<label class="itemlabel">专卖店名称:</label>
					<input class="iteminput" id='agac_exclusiveshop' name='agac_exclusiveshop' type="text" class="mui-input-clear mui-input" placeholder="请输入专卖店名称">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">备注:</label>
					<input class="iteminput" id='agac_remark' name='agac_remark' type="text"  class="mui-input-clear mui-input" placeholder="请输入备注">
				</div>
				-->
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">注册</button>
			</div>
			
		</div>
		<jsp:include page="../share/commonjs.jsp"/>
	   <%--  <script src="<c:url value='/js/distpicker.datajquery-ui.min.js' />"></script> --%>
	    <script src="<c:url value='/js/distpicker.js' />"></script>
       
		<script>
			(function($, doc) { 
				changeAdd(1,0);//初始化获取
				
				
				var saveButton = doc.getElementById('saveButton');
				saveButton.addEventListener('tap', function(event) {
						
						var agac_name = doc.getElementById('agac_name').value;
						var agac_password = doc.getElementById('agac_password').value;
						var password_confirm = doc.getElementById('password_confirm').value;
						var agac_orderpassword = doc.getElementById('agac_orderpassword').value;
						var confirmorderpwd = doc.getElementById('confirmorderpwd').value;
						var agac_accountname = doc.getElementById('agac_accountname').value;
						var agac_mobilephone1 = doc.getElementById('agac_mobilephone1').value;
						var agac_mobilephone2 = doc.getElementById('agac_mobilephone2').value;
						var agac_address = doc.getElementById('agac_address').value;
						//var agac_exclusiveshop = doc.getElementById('agac_exclusiveshop').value;
						//var agac_remark = doc.getElementById('agac_remark').value;
						var agac_province =doc.getElementById('agac_province').value;
						var agac_city =doc.getElementById('agac_city').value;
						var agac_county =doc.getElementById('agac_county').value;
						
						if (agac_name=='') {
							showTrip("账号不能为空");
							return;
						}
						if (agac_password !=password_confirm) {
							showTrip("密码两次输入不一致");
							return;
						}
						if (agac_orderpassword=='') {
							showTrip("二级密码不能为空");
							return;
						}
						if (agac_orderpassword !=confirmorderpwd) {
							showTrip("二级密码两次输入不一致");
							return;
						}
						if(agac_password==agac_orderpassword){
							showTrip("二级密码和登录密码不能一样");
							return;
						}
						if (agac_mobilephone1=='') {
							showTrip("移动电话1不能为空");
							return;
						}
						
						if(agac_province==''){
							showTrip("请选择省");
							return;
						}
						
						if(agac_city==''){
							showTrip("请选择市");
							return ;
						}
						/**
						if(agac_county==''){
							showTrip("请选择区/县");
							return ;
						}
						**/
						//alert(agac_province+"_"+agac_city+"_"+agac_county);
						
						if (agac_address=='') {
							showTrip("详细地址不能为空");
							return;
						}
						
						
						var spinner= new Spinner();
						mui.ajax("<c:url value='/user/doreg'/>",{
							data:{
								agac_name:agac_name,
								agac_password:agac_password,
								agac_orderpassword:agac_orderpassword,
								agac_accountname:agac_accountname,
								agac_mobilephone1:agac_mobilephone1,
								agac_mobilephone2:agac_mobilephone2,
								agac_address:agac_address,
								agac_province:agac_province,
								agac_city:agac_city,
								agac_county:agac_county,
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(document.getElementById('preview'));
								saveButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								saveButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									//
									
									alert(data.Message);
									mui.back();
									
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
						
						
					});
				
			}(mui, document));
			function changeAdd(value,type){
				if(type==0){
					mui.ajax("<c:url value='/user/province'/>",{
						data:{
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						contentType:"application/x-www-form-urlencoded;charset=utf-8",
						beforeSend: function(XMLHttpRequest){
							XMLHttpRequest.setRequestHeader("Accept", "text/plain");
						},
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("agac_province");
							obj.options.add(new Option("选择",""));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].prov_ProvinceID;
								var name=json[i].prov_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
				if(type==1){//通过省份获取城市
					mui.ajax("<c:url value='/user/city'/>",{
						data:{
							id:value
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("agac_city");
							obj.options.length=0;//清除
							obj.options.add(new Option("请选择市","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].city_CityID;
								var name=json[i].city_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
				if(type==2){//通过城市获取区域
					mui.ajax("<c:url value='/user/county'/>",{
						data:{
							id:value
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("agac_county");
							obj.options.length=0;//清除
							obj.options.add(new Option("请选择区/县","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].coun_CountyID;
								var name=json[i].coun_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
			}
		</script>
		
	</body>

</html>