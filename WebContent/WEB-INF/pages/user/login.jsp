<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="ui-page-login">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>登录</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
		    .mui-content-padded {
                background-color: white;
            }
			.area {
				margin: 20px auto 0px auto;
			}
			
			.mui-input-group {
				margin-top: 10px;
			}
			
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-input-group label {
				width: 27%;
			}
			
            .mui-input-row label ~input, .mui-input-row label ~select,
	          .mui-input-row label ~textarea {
	           width: 73%;
            }

			#vcode {
				width: 50%;
				float: left;
			}
			
			#code {
				width: 23%;
				float: right;
			}
			
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 73%;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			
			.mui-content-padded {
				margin-top: 25px;
			}
			
			.mui-btn {
				padding: 10px;
			}
			
			.link-area {
				display: block;
				margin-top: 25px;
				text-align: center;
			}
			
			.spliter {
				color: #bbb;
				padding: 0px 8px;
			}
			
			
		</style>

	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">登录</h1>
		</header>
		 
		<div class="mui-content">
		    <section id="preview"></section>
		   
			<form id='toform' class="mui-input-group"  method="post" action="<c:url value='${backurl}'/>">
			
			<input id='backurl' name="backurl" type="hidden" value="${backurl}" class="mui-input-clear mui-input" >
				<div class="mui-input-row">
					<label  >姓&nbsp;&nbsp;&nbsp;名:</label>
					<input  id='agac_name' name="agac_name" type="text" class="mui-input-clear mui-input" placeholder="请输入姓名">
				</div>
				<div class="mui-input-row">
					<label >密&nbsp;&nbsp;&nbsp;码:</label>
					<input  id='agac_password' name="agac_password" type="password" class="mui-input-clear mui-input" placeholder="请输入密码">
				</div>
				<div class="mui-input-row">
					<label  >验证码:</label>
					<input  id="vcode" type="number" class="mui-input-clear mui-input2" placeholder="请输入验证码">
					<label id="code">5276</label>
				</div>
			</form>

			<div class="mui-content-padded">
				<button id='loginButton' class="mui-btn mui-btn-block mui-btn-primary">登录</button>
				<div class="link-area">
					<a id='regButton'>注册账号</a> <span class="spliter">|</span>
					<a id='forgetPassword'>忘记密码</a>
				</div>
			</div>
			
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/mui.enterfocus.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				MathRand();
				var regButton = doc.getElementById('regButton');
				var forgetButton = doc.getElementById('forgetPassword');
				var loginButton = doc.getElementById('loginButton');
				var code = doc.getElementById('code');
				code.addEventListener('tap', function(event) {
					MathRand();
				}, false);
				
				loginButton.addEventListener('tap', function(event) {
					var agac_name = doc.getElementById('agac_name').value;
					var agac_password=doc.getElementById('agac_password').value;
					var vcode=doc.getElementById('vcode').value;
					var backurl=doc.getElementById('backurl').value;
					if(agac_name==''){
						mui.alert("用户名不能为空");
						//mui.toast('用户名不能为空');
						return ;
					}
					if(agac_password==''){
						mui.alert("密码不能为空");
						return ;
					}
					if(vcode==''){
						mui.alert("验证码不能为空");
						return ;
					}
					if(code.innerText!=vcode){
						mui.alert("验证码不正确");
						return ;
					}
					
					var spinner= new Spinner();
					mui.ajax('<c:url value='/user/dologin'/>',{
							data:{
								agac_name:agac_name,
								agac_password:agac_password,
								backurl:backurl
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(document.getElementById('preview'));
								loginButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								loginButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									document.activeElement.blur();
									var backurl=doc.getElementById('backurl').value;
									if(backurl==''){
										alert(data.Message);
										
									}else{
										doc.getElementById("toform").submit()
									}
									
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				}, false);
				regButton.addEventListener('tap', function(event) {
					$.openWindow({
						url: '<c:url value='/user/toreg'/>',
						id: 'reg',
						preload: true,
						show: {
							aniShow: 'pop-in'
						},
						styles: {
							popGesture: 'hide'
						},
						waiting: {
							autoShow: false
						}
					});
				}, false);
				forgetButton.addEventListener('tap', function(event) {
					$.openWindow({
						url: '<c:url value='/user/getpwd'/>',
						id: 'forgetPassword',
						preload: true,
						show: {
							aniShow: 'pop-in'
						},
						styles: {
							popGesture: 'hide'
						},
						waiting: {
							autoShow: false
						}
					});
				}, false);
			}(mui, document));

			function MathRand() {
				var Num = "";
				for(var i = 0; i < 4; i++) {
					Num += Math.floor(Math.random() * 10);
				}
				document.getElementById("code").innerText = Num;
				//document.getElementById("code").innerHTML = Num;
			}
		</script>
	</body>

</html>