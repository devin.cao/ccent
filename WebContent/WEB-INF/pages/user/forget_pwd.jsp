<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>找回密码</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
@media ( min-width :320px) {
	     .mui-input-group .itemlabel {
				width: 40%;
			}
			.mui-input-row .iteminput {
				width: 60%;
			}
}
@media ( min-width :360px) {
	      .mui-input-group .itemlabel {
				width: 36%;
			}
			.mui-input-row .iteminput {
				width: 64%;
			}
}
@media ( min-width :768px) {
	      .mui-input-group .itemlabel {
				width: 17%;
			}
			.mui-input-row .iteminput {
				width: 83%;
			}
}
@media ( min-width :800px) {
	      .mui-input-group .itemlabel {
				width: 16%;
			}
			.mui-input-row .iteminput {
				width: 84%;
			}
}
@media ( min-width :980px) {
	      .mui-input-group .itemlabel {
				width: 13%;
			}
			.mui-input-row .iteminput {
				width: 87%;
			}
}
@media ( min-width :1080px) {
	       .mui-input-group .itemlabel {
				width: 12%;
			}
			.mui-input-row .iteminput{
				width: 88%;
			}
}
@media ( min-width :1280px) {
	     .mui-input-group .itemlabel {
				width: 10%;
			}
			.mui-input-row .iteminput{
				width: 90%;
			}
}
@media ( min-width :1920px) {
	      .mui-input-group .itemlabel {
				width: 7%;
			}
			.mui-input-row .iteminput{
				width: 93%;
			}
}				
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
			
			#cons_phonecode {
				width: calc(50% - 53px);
				float: left;
			}

			#codebtn{
				float: right;
				width: 82px;
				height: 30px;
				margin-top: 5px;
				margin-right: 2px;
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">找回密码</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		 <section id="preview"></section>
			<form class="mui-input-group" id="getpwdform" name="getpwdform" method="post">
				<div class="mui-input-row">
					<label class="itemlabel" style="text-align: left;">姓名:</label>
					<input class="iteminput" id='agac_name' name="agac_name" type="text" class="mui-input-clear mui-input" placeholder="请输入姓名">
				</div>             	
               
				<div style="display:none;" class="mui-input-row">
					<label class="itemlabel" style="text-align: left;">在亿田账户名:</label>
					<input class="iteminput" id='agac_accountname' name="agac_accountname" type="text" class="mui-input-clear mui-input" placeholder="请输入在亿田账户名">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel" style="text-align: left;">移动号码:</label>
					<input class="iteminput" id='agac_mobilephone1' name="agac_mobilephone1" type="text" class="mui-input-clear mui-input" placeholder="请输入移动号码">
				</div>
				 <div class="mui-input-row">
					<label class="itemlabel">验证码:</label> <input class="iteminput" id='cons_phonecode' type="text"
					name="cons_phonecode" class="mui-input"
					placeholder="请输入验证码"> <input type="button" id="codebtn"
					value="获取验证码" style="color: blue;"  />
			</div>
			</form>
			<div class="mui-content-padded">
				<button id='nextButton' class="mui-btn mui-btn-block mui-btn-primary">下一步</button>
			</div>
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				
				//add by mars 获取验证码
				//code validate
				var countdown=60; 
				var codebtn = doc.getElementById('codebtn');
				var spinner1= new Spinner()
				codebtn.addEventListener('tap', function(event) {
					 var cons_phonenumber=doc.getElementById('agac_mobilephone1').value;
					 var agac_name = doc.getElementById('agac_name').value;
					 if (agac_name=='') {
							mui.alert("用户名不能为空");
							return;
						}	
					 if(cons_phonenumber==''){
							//showTrip("用户手机不能为空");
							mui.alert("用户手机不能为空");
							return ;
						}
						if(cons_phonenumber.length!=11){
							mui.alert("用户手机长度为11位");
							return ;
						} 
						mui.ajax("<c:url value='/user/getvalcode'/>",{
							data:{
								phone:cons_phonenumber,
								agac_name:agac_name
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner1.spin(document.getElementById('preview'));
								codebtn.disabled=true;
							},
							complete: function() {
								spinner1.spin();
								codebtn.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									settime(codebtn);
								}else{
									alert(data.Message);
								} 
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
				});
				}, false);
				//----------------------------------------------
				var nextButton = doc.getElementById('nextButton');
			    nextButton.addEventListener('tap', function(event) {
				var agac_name = doc.getElementById('agac_name').value;
				var agac_accountname = doc.getElementById('agac_accountname').value;
				var agac_mobilephone1=doc.getElementById('agac_mobilephone1').value;
				//add by mars 增加验证码cons_phonecode
				var cons_phonecode=doc.getElementById('cons_phonecode').value;
				if (agac_name=='') {
					mui.alert("用户名不能为空");
					return;
				}
				/* if (agac_accountname=='') {
					mui.alert("账号名不能为空");
					return;
				} */
				if (agac_mobilephone1=='') {
					mui.alert("移动电话不能为空");
					return;
				}
				if(cons_phonecode==''){
					mui.alert("验证码不能为空");
					return;
				}
				
			    var spinner= new Spinner();
				mui.ajax("<c:url value='/user/dogetpwd'/>",{
					data:{
						agac_accountname:agac_accountname,
						agac_name:agac_name,
						agac_mobilephone1:agac_mobilephone1,
						cons_phonecode:cons_phonecode
					},
					dataType:'json',//服务器返回json格式数据
					type:'post',//HTTP请求类型
					headers:null,
					beforeSend: function() {
						spinner.spin(document.getElementById('preview'));
						nextButton.disabled=true;
					},
					complete: function() {
						spinner.spin();
						nextButton.disabled=false;
					},
					success:function(data){
						if(data.StatusCode==1){
							//成功
							//alert(agac_name);
							doc.getpwdform.action = "<c:url value='/user/tosetpwd'/>";
							doc.getpwdform.submit();
			                
						}else{
							alert(data.Message);
						}
						
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						console.log(type);
					}
			     	}); 
			    }, false);
			}(mui, document));
			
		</script> 
		
		
	<script type="text/javascript"> 
	var countdown=300; 
	function settime(obj) { 
    if (countdown == 0) { 
        obj.removeAttribute("disabled");    
        obj.value="获取验证码"; 
        countdown = 300; 
        return;
    } else { 
        obj.setAttribute("disabled", true); 
        obj.value="重新获取(" + countdown + ")"; 
        countdown--; 
    } 
	setTimeout(function() { settime(obj) },1000);
}
  
</script>
</body>
</html>