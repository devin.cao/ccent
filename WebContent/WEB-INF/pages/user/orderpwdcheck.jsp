<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>二级密码验证</title>
		<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
		<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
		<style>
		    .mui-content-padded {
                background-color: white;
            }
			.area {
				margin: 20px auto 0px auto;
			}
			
			.mui-input-group {
				margin-top: 10px;
			}
			
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-input-group label {
				width: 30%;
			}
			
			
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 70%;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			
			.mui-content-padded {
				margin-top: 25px;
			}
			
			.mui-btn {
				padding: 10px;
			}
			
			.spliter {
				color: #bbb;
				padding: 0px 8px;
			}
			
			
		</style>

	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">二级密码验证</h1>
		</header>
		 
		<div class="mui-content">
		    <section id="preview"></section>
		   
			<form id='toform' class="mui-input-group"  >
			<input id='utype' name="utype" type="hidden" value="${utype}" class="mui-input-clear mui-input" >
				<div class="mui-input-row">
					<label  class="itemlabel">二级密码:</label>
					<input class="iteminput"  id='pwd' name="pwd" type="password" class="mui-input-clear mui-input" placeholder="请输入二级密码">
				</div>
				
			</form>
			<div class="mui-content-padded">
				<button id='checkButton' class="mui-btn mui-btn-block mui-btn-primary">验证</button>
				
			</div>
			
		</div>
		<script src="<c:url value='/js/mui.min.js' />"></script>
		<script src="<c:url value='/js/mui.enterfocus.js' />"></script>
		<script src="<c:url value='/js/app.js' />"></script>
		<script src="<c:url value='/js/jquery.js' />"></script>
		<script src="<c:url value='/js/spin.js' />"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				
				var checkButton = doc.getElementById('checkButton');
				checkButton.addEventListener('tap', function(event) {
					var pwd = doc.getElementById('pwd').value;
					if(pwd==''){
						mui.alert("二级密码不能为空");
						return ;
					}
					
					var spinner= new Spinner();
					mui.ajax("<c:url value='/user/orderpwd'/>",{
							data:{
								pwd:pwd
							},
							dataType:'json',//服务器返回json格式数据
							type:'post',//HTTP请求类型
							headers:null,
							beforeSend: function() {
								spinner.spin(document.getElementById('preview'));
								checkButton.disabled=true;
							},
							complete: function() {
								spinner.spin();
								checkButton.disabled=false;
							},
							success:function(data){
								if(data.StatusCode==1){
									//成功
									document.activeElement.blur();
									var utype=doc.getElementById('utype').value;
									if(utype==1) {
										var backurl = "<c:url value='/partorder/list?flag=1' />";
										var id = "partorderlist";
									}
									if(utype==2){
										 id="goodsorderlist";
										 backurl="<c:url value='/goodsorder/list?flag=1' />";
									}
									if(utype==3){
										id="aa";
										backurl="<c:url value='/product/price?flag=1' />";
									}
									if(utype==4){
										id="bb";
										backurl="<c:url value='/goodsorder/getCredit?flag=1' />";
									}
									if(utype==6){
										id="cc";
										backurl="<c:url value='/bomOrder/list?flag=1' />";
									}
									if(utype==5){
										id="dd";
										backurl="<c:url value='/traditional/list?flag=1' />";
									}
                                    if(utype==7){
                                        id="ee";
                                        backurl="<c:url value='/product/scan?flag=1' />";
                                    }
                                    if(utype==8){
                                        id="ff";
                                        backurl="<c:url value='/reports/orderlist?flag=1' />";
                                    }
                                    if(utype==9){
                                        id="hh";
                                        backurl="<c:url value='/reports/receivelist?flag=1' />";
                                    }
                                    if(utype==10){
                                        id="ii";
                                        backurl="<c:url value='/target/targetList?flag=1' />";
                                    }
                                    if(utype==11){
                                        id="jj";
                                        backurl="<c:url value='/target/targetDetail?flag=1' />";
                                    }
									mui.openWindow({
									    id: id,
									    url: backurl
								    });
								}else{
									alert(data.Message);
								}
								
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								console.log(type);
							}
						});
				}, false);
				
			}(mui, document));

		</script>
	</body>

</html>