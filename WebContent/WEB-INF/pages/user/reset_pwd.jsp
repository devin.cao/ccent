<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html class="ui-page-login">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<title>重置密码</title>
	<link href="<c:url value='/css/mui.min.css' />" rel="stylesheet" />
	<link href="<c:url value='/css/style.css' />" rel="stylesheet" />
	<style>
		.area {
			margin: 20px auto 0px auto;
		}
		.mui-input-group:first-child {
			margin-top: 20px;
		}
		.mui-input-group label {
			width: 34%;
		}
		.mui-input-row label~input,
		.mui-input-row label~select,
		.mui-input-row label~textarea {
			width: 66%;
		}
		.mui-checkbox input[type=checkbox],
		.mui-radio input[type=radio] {
			top: 6px;
		}
		.mui-content-padded {
			margin-top: 25px;
		}
		.mui-btn {
			padding: 10px;
		}
		.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
			margin-top: 1px;
		}
	</style>
</head>

<body>
<header class="mui-bar mui-bar-nav">
	<h1 class="mui-title">重置密码</h1>
	<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		<span class="mui-icon mui-icon-left-nav"></span>返回</button>
</header>
<div class="mui-content">
	<section id="preview"></section>
	<form class="mui-input-group">
		<input  type="hidden" id="agac_name" name="agac_name" value="${agac_name}">
		<input  type="hidden" id="agac_accountname" name="agac_accountname" value="${agac_accountname}">
		<input  type="hidden" id="agac_mobilephone1" name="agac_mobilephone1" value="${agac_mobilephone1}">
		<!--
        <div class="mui-input-row">
            <label>账号</label>
            <input id='account' type="text" class="mui-input-clear mui-input" placeholder="请输入账号">
        </div>
        -->
		<div class="mui-input-row">
			<label  class="itemlabel">登录密码:</label>
			<input  class="iteminput" id='pwd1' name="pwd1" type="password" class="mui-input-clear mui-input" placeholder="输入登录密码">
		</div>
		<div class="mui-input-row">
			<label  class="itemlabel">登录密码:</label>
			<input  class="iteminput" id='pwd2' name="pwd2" type="password" class="mui-input-clear mui-input" placeholder="重复输入登录密码">
		</div>
		<div class="mui-input-row">
			<label  class="itemlabel">二级密码:</label>
			<input  class="iteminput" id='pwd3' name="pwd3" type="password" class="mui-input-clear mui-input" placeholder="输入二级密码">
		</div>
		<div class="mui-input-row">
			<label  class="itemlabel">二级密码:</label>
			<input  class="iteminput" id='pwd4' name="pwd4" type="password" class="mui-input-clear mui-input" placeholder="重复输入二级密码">
		</div>
	</form>
	<div class="mui-content-padded">
		<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">提交</button>
	</div>
</div>
<script src="<c:url value='/js/mui.min.js' />"></script>
<script src="<c:url value='/js/app.js' />"></script>
<script src="<c:url value='/js/spin.js' />"></script>
<script>
	(function($, doc) {
		mui.init({
			statusBarBackground: '#000000'
		});
		var saveButton = doc.getElementById('saveButton');
		saveButton.addEventListener('tap', function(event) {
			var agac_name = doc.getElementById('agac_name').value;
			var agac_accountname = doc.getElementById('agac_accountname').value;
			var agac_mobilephone1=doc.getElementById('agac_mobilephone1').value;
			var pwd1=doc.getElementById('pwd1').value;
			var pwd2=doc.getElementById('pwd2').value;
			var pwd3=doc.getElementById('pwd3').value;
			var pwd4=doc.getElementById('pwd4').value;
			if(pwd1==''){
				mui.alert("新登录密码不能为空");
				return ;
			}
			if(pwd2==''){
				mui.alert("新登录重置密码不能为空");
				return ;
			}
			if(pwd1!=pwd2){
				mui.alert("新登录密码和新登录重置密码不一致");
				return ;
			}

			if(pwd3!=pwd4){
				mui.alert("新二级密码和新二级重置密码不一致");
				return ;
			}

			if(pwd1==pwd3){
				mui.alert("新二级密码和新登录密码不能相同");
				return ;
			}

			var spinner= new Spinner();
			mui.ajax("<c:url value='/user/dosetpwd'/>",{
				data:{
					newpwd:pwd1,
					newpwd_order:pwd3,
					usr:agac_name
				},
				dataType:'json',//服务器返回json格式数据
				type:'post',//HTTP请求类型
				headers:null,
				beforeSend: function() {
					spinner.spin(document.getElementById('preview'));
					saveButton.disabled=true;
				},
				complete: function() {
					spinner.spin();
					saveButton.disabled=false;
				},
				success:function(data){
					if(data.StatusCode==1){
						//成功
						alert(data.Message);
						window.location.href="<c:url value='/user/login'/>";


					}else{
						alert(data.Message);
					}

				},
				error:function(xhr,type,errorThrown){
					//异常处理；
					console.log(type);
				}
			});
		}, false);


	}(mui, document));
</script>
</body>

</html>