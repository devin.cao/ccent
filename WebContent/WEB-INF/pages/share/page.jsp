<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
 <%
   String url = request.getParameter("url");
   if(url.contains("?")){
	   url=url+"&";
   }else{
	   if(!url.endsWith("&")){
		  url=url+"?";
	   }
   }
  %>
<!--  ${pageUtils.showLastPage }<br>
			${pageUtils.showNextPage } -->
			<c:choose>
            <c:when test="${(pageUtils.showLastPage eq 0)&&(pageUtils.showNextPage eq 0)}">
             </c:when>
           <c:otherwise>
            <div class="mui-content-padded">
                <ul class="mui-pager">
                  <c:if test="${pageUtils.showLastPage eq 0}">
                   <li  class="mui-disabled">
                   <span  >上一页 </span>
                   </li>
                   </c:if>
                   <c:if test="${pageUtils.showLastPage eq 1}">
                   <li >
                   <a  href="<c:url value='<%=url%>' />pageIndex=${pageUtils.page-1}">上一页 </a>
                   </li>
                  </c:if>
                  <span>${pageUtils.page}/${pageUtils.totalpage}</span>
                   <c:if test="${pageUtils.showNextPage eq 0}">
                   <li  class="mui-disabled">
                   <span  >下一页 </span>
                   </li>
                   </c:if>
                   <c:if test="${pageUtils.showNextPage eq 1}">
                   <li >
                   <a  href="<c:url value='<%=url%>' />pageIndex=${pageUtils.page+1}">下一页 </a>
                   </li>
                  </c:if>
                  
                  
                  
                </ul>
            </div>
           </c:otherwise>
           </c:choose>