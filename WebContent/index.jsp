<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="ui-page-login">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title></title>
		<link href="css/mui.min.css" rel="stylesheet" />
		<link href="css/style.css" rel="stylesheet" />
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			
			.mui-input-group {
				margin-top: 10px;
			}
			
			.mui-input-group:first-child {
				margin-top: 20px;
			}
			
			.mui-input-group label {
				width: 27%;
			}
			#vcode{
				width: 50%;
				float: left;
			}
			#code{
				width: 23%;
				float: right;
			}
			
			.mui-input-row label~input,
			.mui-input-row label~select,
			.mui-input-row label~textarea {
				width: 73%;
			}
			
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			
			.mui-content-padded {
				margin-top: 25px;
			}
			
			.mui-btn {
				padding: 10px;
			}
			
			.link-area {
				display: block;
				margin-top: 25px;
				text-align: center;
			}
			
			.spliter {
				color: #bbb;
				padding: 0px 8px;
			}
			
			.oauth-area {
				position: absolute;
				bottom: 20px;
				left: 0px;
				text-align: center;
				width: 100%;
				padding: 0px;
				margin: 0px;
			}
			
			.oauth-area .oauth-btn {
				display: inline-block;
				width: 50px;
				height: 50px;
				background-size: 30px 30px;
				background-position: center center;
				background-repeat: no-repeat;
				margin: 0px 20px;
				/*-webkit-filter: grayscale(100%); */
				border: solid 1px #ddd;
				border-radius: 25px;
			}
			
			.oauth-area .oauth-btn:active {
				border: solid 1px #aaa;
			}
			
			.oauth-area .oauth-btn.disabled {
				background-color: #ddd;
			}
		</style>

	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">主页</h1>
		</header>
		<div class="mui-content">
			<ul class="mui-table-view">
    <li class="mui-table-view-cell" id="login">
    	 <a class="mui-navigate-right" >登录</a>
    </li>
    <li class="mui-table-view-cell" id="productscan">
        <a class="mui-navigate-right" >产品扫码</a>
    </li>
    <li class="mui-table-view-cell" id="partodlist">
        <a class="mui-navigate-right" >配件订单</a>
    </li>
    <li class="mui-table-view-cell" id="productodlist"> 
        <a class="mui-navigate-right" >成品订单</a>
    </li>
	<li class="mui-table-view-cell" id="bomlist">
        <a class="mui-navigate-right" >物料订单</a>
    </li>
	<li class="mui-table-view-cell" id="traditionallist">
        <a class="mui-navigate-right" >传统机订单</a>
    </li>
    <li class="mui-table-view-cell" id="userinfo"> 
        <a class="mui-navigate-right" >安装上报</a>
    </li>
    <li class="mui-table-view-cell"  id="woinfo">
        <a class="mui-navigate-right">工单</a>
    </li>
</ul>
		</div>
		<script src="js/mui.minjquery-ui.min.js"></script>
		<script src="js/mui.enterfocusjquery-ui.min.js"></script>
		<script src="js/appjquery-ui.min.js"></script>
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
			
			mui(".mui-table-view").on('tap', '.mui-table-view-cell', function() {
				var celid = this.getAttribute("id");
				//alert(celid);
				var id,url;
				if(celid=='login'){
					id="login";
					url="user/login";
				}else if(celid=='productscan'){
					id="scaninfo";
					url="product/get?barcode=43566111";
				}else if(celid=='partodlist'){
					id="partodlist";
					url="partorder/list";
				}else if(celid=='productodlist'){
					id="productodlist";
					url="goodsorder/list";
				}else if(celid=='bomlist'){
					id="bomlist";
					url="bomOrder/list";
				}else if(celid=='traditionallist'){
					id="traditionallist";
					url="traditional/list";
				}else if(celid=='userinfo'){
					id="userinfo";
					url="consumer/list";
				}else if(celid=='woinfo'){
					id="userwo";
					url="wo/list";
				}
				mui.openWindow({
					id: id,
					url: url
				});
			})
			
			}(mui, document));
		</script>
	</body>

</html>