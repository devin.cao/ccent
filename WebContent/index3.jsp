<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layer-更懂你的web弹窗解决方案</title>
  <script src="http://lib.sinaapp.com/js/jquery/1.9.1/jquery-1.9.1.min.js"></script>
		<script src="<c:url value='/layer/layer.js' />"></script>

  <style>
  html{background-color:#E3E3E3; font-size:14px; color:#000; font-family:'微软雅黑'}
  a,a:hover{ text-decoration:none;}
  pre{font-family:'微软雅黑'}
  .box{padding:20px; background-color:#fff; margin:50px 100px; border-radius:5px;}
  .box a{padding-right:15px;}
  #about_hide{display:none}
  .layer_text{background-color:#fff; padding:20px;}
  .layer_text p{margin-bottom: 10px; text-indent: 2em; line-height: 23px;}
  .button{display:inline-block; *display:inline; *zoom:1; line-height:30px; padding:0 20px; background-color:#56B4DC; color:#fff; font-size:14px; border-radius:3px; cursor:pointer; font-weight:normal;}
  .photos-demo img{width:200px;}
  </style>
</head>
<body>
<div class="box">
<input id="productcode" name="productcode" type="text">
<a href="#" onclick="openproduct();">test</a>
</div>

<script>
function openproduct(){
 	layer.open({
   type: 2,
  area: ['700px', '530px'],
  fixed: false, //不固定
  maxmin: true,
  content: '<c:url value='/product/list' />'
});
 }
</script>
</body>
</html>