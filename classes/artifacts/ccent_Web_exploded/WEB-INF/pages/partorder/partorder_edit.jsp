<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html class="ui-page-login">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>新增配件订单</title>
		<jsp:include page="../share/commoncss.jsp"></jsp:include>
		<style>
			.area {
				margin: 20px auto 0px auto;
			}
			.mui-input-group:first-child {
				margin-top: 20px;
			}
@media ( min-width :320px) {
	     .mui-input-group .itemlabel {
				width: 40%;
			}
			.mui-input-row .iteminput{
				width: 60%;
			}
}
@media ( min-width :360px) {
	     .mui-input-group .itemlabel {
				width: 36%;
			}
			.mui-input-row .iteminput{
				width: 64%;
			}
}
@media ( min-width :768px) {
	      .mui-input-group .itemlabel {
				width: 17%;
			}
			.mui-input-row .iteminput{
				width: 83%;
			}
}
@media ( min-width :800px) {
	       .mui-input-group .itemlabel {
				width: 16%;
			}
			.mui-input-row .iteminput{
				width: 84%;
			}
}
@media ( min-width :980px) {
	      .mui-input-group .itemlabel {
				width: 13%;
			}
			.mui-input-row .iteminput{
				width: 87%;
			}
}
@media ( min-width :1080px) {
	       .mui-input-group .itemlabel {
				width: 12%;
			}
			.mui-input-row .iteminput{
				width: 88%;
			}
}
@media ( min-width :1280px) {
	     .mui-input-group .itemlabel {
				width: 10%;
			}
			.mui-input-row .iteminput{
				width: 90%;
			}
}
@media ( min-width :1920px) {
	      .mui-input-group .itemlabel {
				width: 7%;
			}
			.mui-input-row .iteminput{
				width: 93%;
			}
}	
			.mui-checkbox input[type=checkbox],
			.mui-radio input[type=radio] {
				top: 6px;
			}
			.mui-content-padded {
				margin-top: 25px;
			}
			.mui-btn {
				padding: 10px;
			}
			.mui-input-row label~input, .mui-input-row label~select, .mui-input-row label~textarea{
				margin-top: 1px;
			}
			#paor_address{
				margin-top: 5px;
				margin-bottom: 5px;
				padding-right: 8px;
				border: 1px solid rgba(0, 0, 0, .2);
			}
		</style>
	</head>

	<body>
		<header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">新增配件订单</h1>
			<button class="mui-action-back mui-btn mui-btn-blue mui-btn-link mui-btn-nav mui-pull-right">
		    <span class="mui-icon mui-icon-left-nav"></span>返回</button>
		</header>
		<div class="mui-content">
		  <section id="preview"></section>
			<form class="mui-input-group" id="partorderform"  method="post" action="<c:url value='/partorder/get'/>">
			<input id='paor_PartsOrderID' name="paor_PartsOrderID" type="hidden" class="mui-input-clear mui-input" >
			   <div class="mui-input-row">
					<label class="itemlabel">可用余额:</label>
					<input class="iteminput" id='credit' name="credit" readonly="readonly" value="${item.credit}元" type="text" class="mui-input" >
				</div>
                <div class="mui-input-row">
                    <label class="itemlabel">押金余额:</label>
                    <input class="iteminput" id='deposit' name="deposit" readonly="readonly" value="${item.deposit}元" type="text" class="mui-input" >
                </div>
				<div class="mui-input-row">
					<label class="itemlabel">客户名称:</label>
					<input  class="iteminput" id='paor_company' readonly="readonly" name="paor_company"  readonly="readonly" value="${item.comp_name}" type="text" class="mui-input" placeholder="请输入客户名称">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">联系电话:</label>
					<input class="iteminput" id='paor_tel' name="paor_tel" type="text" value="${item.comp_phone}" class="mui-input-clear mui-input" placeholder="请输入联系电话">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">配件发放方式:</label>
					<select class="iteminput" id="paor_deliveringway" name="paor_deliveringway" onchange="deliveringwayChange(this.value)">
					
					  <option value="">请选择配件发放方式</option>
					  <option value="1">物流</option>
					  <option value="3">快递</option>
					   <option value="4">自提</option>
					</select>
					
				</div>
			<div id="procityinfo" style="display: none;">
				<div data-toggle="distpicker">
				<div class="mui-input-row">
					<label class="itemlabel">省:</label>
					 <select class="iteminput" class="form-control" id="paor_province" data-province=""  onchange="changeAdd(this.value,1)"></select>
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">市:</label>
					 <select class="iteminput" class="form-control" id="paor_city"  data-city=""  onchange="changeAdd(this.value,2)"></select>
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">区/县:</label>
					 <select class="iteminput" class="form-control" id="paor_county"  data-district=""   onchange="changeAdd(this.value,3)"></select>
				</div>
				</div>
			</div>
				<div class="mui-input-row" id="addressdiv" style="height: 113px;display: none;">
					<label class="itemlabel">详细地址:</label>
                   <textarea class="iteminput"  id='paor_address' name="paor_address" rows="3"  cols="2" placeholder="请输入详细地址">${item.comp_address}</textarea>  
				</div>
				<div id="kuaidiinfo" style="display: none;">
				<div class="mui-input-row">
					<label class="itemlabel">收件人手机:</label>
					<input class="iteminput" id='paor_recipientsphone' name="paor_recipientsphone" value="${item.comp_recipientsphone}" type="number" class="mui-input-clear mui-input" placeholder="请输入收件人手机">
				</div>
				<div class="mui-input-row">
					<label class="itemlabel">收件人姓名:</label>
					<input class="iteminput" id='paor_recipients' name="paor_recipients" type="text" value="${item.comp_recipients}"  class="mui-input-clear mui-input" placeholder="请输入收件人姓名" >
				</div>
					<div class="mui-input-row">
						<input class="iteminput" id='showJson' name="showJson" type="text"  class="mui-input-clear mui-input"  >
					</div>
				</div>
			</form>
			<div class="mui-content-padded">
				<button id='saveButton' class="mui-btn mui-btn-block mui-btn-primary">保存</button>
			</div>
		
		</div>
		<jsp:include page="../share/commonjs.jsp"/>
         <%-- <script src="<c:url value='/js/distpicker.datajquery-ui.min.js' />"></script> --%>
	  <script src="<c:url value='/js/distpicker.js' />"></script>
       
		<script>
			(function($, doc) {
				mui.init({
					statusBarBackground: '#000000'
				});
				var old_back = mui.back;
				mui.back = function(){
					window.location.href="<c:url value='/partorder/list?flag=1'/>";
				}
				 var $distpicker = $('#distpicker');
			   var saveButton = doc.getElementById('saveButton');
			    saveButton.addEventListener('tap', function(event) {
			    	//var paor_name=doc.getElementById('paor_name').value;
					var paor_company=doc.getElementById('paor_company').value;
					var paor_tel=doc.getElementById('paor_tel').value;
					var paor_deliveringway=doc.getElementById('paor_deliveringway').value;
					//var paor_express=doc.getElementById('paor_express').value;
				    var paor_province="";
					var paor_city="";
					var paor_county="";
					var paor_address="";
					var paor_recipientsphone="";
					var paor_recipients="";
					var sngp = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
					if(paor_company==''){
						showTrip("客户名称不能为空");
						return ;
					}
					if(paor_tel==''){
						showTrip("联系电话不能为空");
						return ;
					}
					if(sngp.test(paor_tel) ){
						showTrip("联系电话不能含有中文");
							return ;
					}
					if(paor_deliveringway==''){
						showTrip("请选择配件发放方式");
						return ;
					}
					if(paor_deliveringway==3){
						 paor_province=doc.getElementById('paor_province').value;
						 paor_city=doc.getElementById('paor_city').value;
						 paor_county=doc.getElementById('paor_county').value;
						 paor_address=doc.getElementById('paor_address').value;
						 paor_recipientsphone=doc.getElementById('paor_recipientsphone').value;
						 
						 paor_recipients=doc.getElementById('paor_recipients').value;
						 if(paor_province==''||paor_province ==0){
								showTrip("请选择省");
								return;
						 }
						 if(paor_city==''||paor_city==0){
								showTrip("请选择市");
								return ;
						  }
						 if(paor_county==''||paor_county==0){
								showTrip("请选择区/县");
								return ;
						}
						if(paor_address==''){
							showTrip("详细地址不能为空");
							return ;
						}
						if(paor_recipientsphone==''){
							showTrip("收件人手机不能为空");
							return ;
						}
						
						if(paor_recipientsphone.length!=11){
							showTrip("收件人手机长度为11位");
							return ;
						}
						
						if(paor_recipients==''){
							showTrip("收件人姓名不能为空");
							return ;
						}
						var sngp =/^[\u0391-\uFFE5-A-Za-z]+$/; 
						 if(!sngp.test(paor_recipients) ){
							 showTrip("收件人姓名只能是中英文");
								return ;
						}
						
					}
					
					var spinner= new Spinner();
					mui.ajax("<c:url value='/partorder/doEdit'/>",{
						data:{
							paor_company:paor_company,
							paor_tel:paor_tel,
							paor_deliveringway:paor_deliveringway,
							paor_province:paor_province,
							paor_city:paor_city,
							paor_county:paor_county,
							paor_address:paor_address,
							paor_recipientsphone:paor_recipientsphone,
							paor_recipients:paor_recipients
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						beforeSend: function() {
							spinner.spin(document.getElementById('preview'));
							saveButton.disabled=true;
						},
						complete: function() {
							spinner.spin();
							saveButton.disabled=false;
						},
						success:function(data){
							if(data.StatusCode==1){
								//成功
								//
								alert(data.Message);
								window.location.href="<c:url value='/partorder/get?paor_status=1&id='/>"+data.ID;
							}else{
								alert(data.Message);
							}
							
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
							console.log(type);
						}
					});
					
				}, false);
			}(mui, document));
			function changeAdd(value,type){
				if(type==0){
					mui.ajax("<c:url value='/consumer/province'/>",{
						data:{
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						contentType:"application/x-www-form-urlencoded;charset=utf-8",
						beforeSend: function(XMLHttpRequest){
							XMLHttpRequest.setRequestHeader("Accept", "text/plain");
						},
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("paor_province");
							//obj.options.add(new Option("选择",""));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].prov_ProvinceID;
								var name=json[i].prov_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
				if(type==1){//通过省份获取城市
					mui.ajax("<c:url value='/consumer/city'/>",{
						data:{
							id:value
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("paor_city");
							obj.options.length=0;//清除
							obj.options.add(new Option("请选择市","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].city_CityID;
								var name=json[i].city_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}
				if(type==2){//通过城市获取区域
					mui.ajax("<c:url value='/consumer/county'/>",{
						data:{
							id:value
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						headers:null,
						success:function(data){
							var json = eval(data);
							var obj=document.getElementById("paor_county");
							obj.options.length=0;//清除
							obj.options.add(new Option("请选择区/县","0"));
							for(var i=0; i<json.length; i++)
							{
								var id=json[i].coun_CountyID;
								var name=json[i].coun_Name;
								obj.options.add(new Option(name,id));
							}
						}
					});
				}

			}
			function deliveringwayChange(value){
				
				var procityinfo=document.getElementById("procityinfo");
				var addressdiv=document.getElementById("addressdiv");
				var kuaidiinfo=document.getElementById("kuaidiinfo");
				if(value==3){
					//kuaidi
					mui.ajax("<c:url value='/consumer/province'/>",{
						data:{
						},
						dataType:'json',//服务器返回json格式数据
						type:'post',//HTTP请求类型
						contentType:"application/x-www-form-urlencoded;charset=utf-8",
						beforeSend: function(XMLHttpRequest){  
							   XMLHttpRequest.setRequestHeader("Accept", "text/plain");  
							  },
						headers:null, 
						success:function(data){
							 var json = eval(data);
							document.getElementById("paor_province").value="";
							 var obj=document.getElementById("paor_province");
							 var obj2=document.getElementById("paor_county");
							 var obj1=document.getElementById("paor_city");
							 obj.options.add(new Option("请选择省","0"));
						        for(var i=0; i<json.length; i++)  
						        {
						           var id=json[i].prov_ProvinceID;
								   var name=json[i].prov_Name;
						           obj.options.add(new Option(name,id));
						        }
							obj1.options.add(new Option("请选择市","0"));
							obj2.options.add(new Option("请选择区/县","0"));

							//doc.getElementById("showJson").value=data;
							/* var obj=doc.getElementById("paor_province");
							$.each(data, function(i, n){ 
								var id=n.prov_ProvinceID;
								 
								var name=n.prov_Name;
								if(i==1)
									alert(id);
								//obj.options.add(new Option(name,id));  
							}); */
						}
					});
					
					procityinfo.style.display ="block";
					addressdiv.style.display ="block";
					kuaidiinfo.style.display ="block";
				}else{
					procityinfo.style.display ="none";
					addressdiv.style.display ="none";
					kuaidiinfo.style.display ="none";
				}
			}
		</script>
	</body>

</html>